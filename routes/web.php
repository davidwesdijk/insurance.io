<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::get('lang/{language}', 'Settings\LanguageController')->name('language');

Route::middleware('guest')->group(function () {
    Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
    Route::post('login', 'Auth\LoginController@login');
});

Route::middleware('auth')->group(function () {
    Route::post('logout', 'Auth\LoginController@logout')->name('logout');
    Route::get('verify', 'Auth\MultiFactorController@show')->name('verify-mfa');
    Route::post('verify', 'Auth\MultiFactorController@verify')->name('verify-mfa');

    Route::get('/', 'DashboardController@show')->name('dashboard');

    Route::prefix('dashboard')->group(function () {
        Route::get('atms', 'DashboardController@show')->name('dashboard.atms');
        Route::get('claims', 'DashboardController@show')->name('dashboard.claims');
    });

    Route::prefix('atms')->group(function () {
        Route::get('/', 'AtmController@index')->name('atms.index');
        Route::get('/new', 'AtmController@create')->name('atms.create');
        Route::post('/new', 'AtmController@store')->name('atms.store');
        Route::get('/{atm}', 'AtmController@show')->name('atms.show');
        Route::put('/{atm}', 'AtmController@setLocation')->name('atms.setLocation');
        Route::get('/{atm}/Edit', 'AtmController@edit')->name('atms.edit');
        Route::put('/{atm}/Edit', 'AtmController@update')->name('atms.update');
    });

    Route::prefix('claims')->group(function () {
        Route::get('/', 'ClaimController@index')->name('claims.index');
        Route::get('/new', 'ClaimController@create')->name('claims.new');

        Route::get('/new/atm/{atm}', 'ClaimController@createForAtm')->name('claims.new.atm');
        Route::post('/new/atm/{atm}', 'ClaimController@store')->name('claims.store');

        Route::get('/{claim}', 'ClaimController@show')->name('claims.show');
        Route::get('/{claim}/edit/{step?}', 'ClaimController@edit')->name('claims.edit');
        Route::get('/{claim}/preview', 'ClaimController@preview')->name('claims.preview');
        Route::post('/{claim}', 'ClaimController@storeDetail')->name('claims.store-detail');
    });

    Route::prefix('manage/users')->group(function () {
        Route::get('/', 'UserController@index')->name('manage.users.index');
        Route::get('/new', 'UserController@create')->name('manage.users.create');
        Route::post('/new', 'UserController@store')->name('manage.users.store');
        Route::post('/set-password', 'UserController@setPassword')->name('manage.users.set-password');
        Route::get('/{user}', 'UserController@show')->name('manage.users.show');
        Route::put('/{user}', 'UserController@update')->name('manage.users.update');
    });

    Route::prefix('manage/roles')->group(function () {
        Route::get('/', 'RoleController@index')->name('manage.roles.index');
    });

    Route::get('api/search', 'SearchController')->name('api.search');
    Route::post('api/atms', 'Api\ApiAtmController@index')->name('api.atms.index');
    Route::post('api/atms/locations', 'Api\ApiAtmController@getLocations')->name('api.atms.locations');
    Route::post('api/claims', 'Api\ApiClaimController@index')->name('api.claims.index');
    Route::post('api/claims/locations', 'Api\ApiClaimController@getLocations')->name('api.claims.locations');
    Route::post('api/claims/store', 'Api\ApiClaimController@storeClaim')->name('api.claims.store');
    Route::post('api/claims/{claim}', 'Api\ApiClaimController@updateClaim')->name('api.claims.update');
    Route::post('api/manage/users', 'Api\ApiUserController@index')->name('api.manage.users.index');
});