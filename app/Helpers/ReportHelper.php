<?php

namespace App\Helpers;

use App\Models\ATM;
use App\Models\Claim;
use Illuminate\Support\Facades\Cache;
use PDF;

class ReportHelper
{
    /**
     * @param Claim $claim
     *
     * @return mixed
     */
    public static function getReportPreviewPdf(Claim $claim)
    {
        $filename = 'Preview_report_' . $claim->identifier;

        $pdf = PDF::loadView('pdf.report', compact('claim', 'filename'));

        return $pdf->stream();
    }

    /**
     * @param ATM $atm
     *
     * @return string
     */
    public static function getImageForMap(ATM $atm)
    {
        $url = 'https://maps.googleapis.com/maps/api/staticmap?center='.$atm->lat.','.$atm->lng.'&zoom=19&size=640x480&scale=2&maptype=roadmap&markers=color:red%7C'.$atm->lat.','.$atm->lng.'&key='.config('geocoder.key');

        return Cache::remember($url, 60, function () use ($url) {
            return 'data:image/png;base64,' . base64_encode(file_get_contents($url));
        });
    }

    /**
     * @param ATM $atm
     *
     * @return string
     */
    public static function getImageForPanorama(ATM $atm)
    {
        $url = 'https://maps.googleapis.com/maps/api/streetview?size=640x480&location='.$atm->lat.','.$atm->lng.'&heading='.$atm->heading.'&pitch='.$atm->pitch.'&key='.config('geocoder.key');

        return Cache::remember($url, 60, function () use ($url) {
            return 'data:image/png;base64,' . base64_encode(file_get_contents($url));
        });
    }
}