<?php

namespace App\Helpers;

use App\Models\ATM;
use App\Models\Claim;
use Illuminate\Http\Request;

class ClaimHelper
{
    /**
     * @param ATM $atm
     *
     * @return string
     */
    public static function generateIdentifier(ATM $atm): string
    {
        $elements = [
            'SIOC',
            $atm->identifier,
            now()->format('dmY'),
            now()->format('Hi')
        ];

        return strtoupper(implode('', $elements));
    }

    /**
     * @param Claim $claim
     *
     * @return string
     */
    public static function getLootEstimate(Claim $claim)
    {
        if ($claim->loot) {
            $estimate = $claim->loot;

        } elseif ($claim->last_balance && !$claim->valuables_found) {
            $estimate = $claim->last_balance;

        } elseif ($claim->last_balance && $claim->valuables_found) {
            $estimate = ($claim->last_balance - $claim->valuables_found);

        }

        return number_format($estimate, 0);
    }
}