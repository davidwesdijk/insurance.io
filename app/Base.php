<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

/**
 * Class Base
 * @package App
 */
class Base extends Model
{
    /** @var bool */
    public $timestamps = false;

    /** @var array */
    protected $fillable = [
        'type',
        'key',
        'value',
    ];

    /**
     * @param Builder $query
     * @param string $type
     *
     * @return \Illuminate\Support\Collection
     */
    public function scopeType(Builder $query, string $type)
    {
        return $query->where('type', Str::snake($type))->pluck('value', 'key');
    }

    /**
     * @return mixed
     */
    public function getValueAttribute()
    {
        return $this->value ?? Str::title($this->key);
    }
}
