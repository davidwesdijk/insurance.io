<?php

namespace App;

use App\Models\ATM;
use App\Models\Claim;
use App\Models\ClaimDetails;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'description',
        'type',
    ];

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @param Builder $query
     *
     * @return Builder
     */
    public function scopeAtm(Builder $query)
    {
        return $query->where('type', ATM::class);
    }

    /**
     * @param Builder $query
     *
     * @return Builder
     */
    public function scopeUser(Builder $query)
    {
        return $query->where('type', User::class);
    }

    /**
     * @param Builder $query
     *
     * @return Builder
     */
    public function scopeClaim(Builder $query)
    {
        return $query->where('type', Claim::class);
    }

    /**
     * @param Builder $query
     *
     * @return Builder
     */
    public function scopeDetail(Builder $query)
    {
        return $query->where('type', ClaimDetails::class);
    }
}
