<?php

namespace App\Response;

use Illuminate\Contracts\Support\Arrayable;

class SuccessResponse implements Arrayable
{
    /**
     * @var string
     */
    private $message;

    /**
     * @var string
     */
    private $title;

    /**
     * SuccessResponse constructor.
     *
     * @param string|null $message
     * @param string|null $title
     */
    public function __construct(string $message = null, string $title = null)
    {
        $this->title = __($title) ?: __('Successful');
        $this->message = __($message) ?: __('Action has been processed successfully.');
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return [
            'message' => [
                'type' => 'success',
                'title' => $this->title,
                'message' => $this->message,
            ]
        ];
    }
}