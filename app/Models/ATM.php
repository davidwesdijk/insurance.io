<?php

namespace App\Models;

use App\Builders\HistoryBuilder;
use App\Status;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use \OwenIt\Auditing\Auditable as AuditableTrait;

class ATM extends Model implements Auditable
{
    use Uuids, AuditableTrait;

    public static $initialStatus = 1;

    protected $table = 'atms';

    public $incrementing = false;

    protected $fillable = [
        'id',
        'address',
        'zipcode',
        'city',
        'country',
        'lat',
        'lng',
        'heading',
        'pitch',
        'manufacturer',
        'identifier',
        'serial_number',
        'year',
        'owner',
        'location',
        'capacity',
        'commissioned',
        'decommissioned',
        'status_id',
    ];

    protected $casts = [
        'lat' => 'decimal:14',
        'lng' => 'decimal:14',
        'heading' => 'decimal:14',
        'pitch' => 'decimal:16',
        'commissioned' => 'date',
        'decommissioned' => 'date',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function functionalities()
    {
        return $this->belongsToMany(Functionality::class, 'atm_functionality', 'atm_id');
    }

    /**
     * @return bool
     */
    public function getActiveAttribute()
    {
        $active = $this->commissioned ? $this->commissioned->isPast() : false;

        if ($active && $this->decommissioned) {
            $active = $this->decommissioned->isFuture();
        }

        return $active;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function status()
    {
        return $this->belongsTo(Status::class);
    }

    /**
     * Generate tags for the audit
     *
     * @return array
     */
    public function generateTags(): array
    {
        return [
            $this->status->description,
        ];
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function claims()
    {
        return $this->hasMany(Claim::class, 'atm_id');
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function history()
    {
        return (new HistoryBuilder)->getByATM($this);
    }
}
