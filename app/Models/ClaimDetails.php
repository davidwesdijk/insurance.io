<?php

namespace App\Models;

use App\Status;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable as AuditableTrait;
use OwenIt\Auditing\Contracts\Auditable;

class ClaimDetails extends Model implements Auditable
{
    use Uuids, AuditableTrait;

    public static $initialStatus = 300;

    public $incrementing = false;

    protected $fillable = [
        'type',
        'amount',
        'data',
        'status_id',
        'order',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function claim()
    {
        return $this->belongsTo(Claim::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function status()
    {
        return $this->belongsTo(Status::class);
    }

    /**
     * Generate tags for the audit
     *
     * @return array
     */
    public function generateTags(): array
    {
        return [
            $this->status->description,
        ];
    }
}
