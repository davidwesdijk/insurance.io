<?php

namespace App\Models\Navigation;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Route;

class Menu extends Model
{
    protected $fillable = [
        'label',
        'route',
        'icon',
        'permission',
        'parent_id',
        'order',
    ];

    protected $with = ['submenu'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function submenu()
    {
        return $this->hasMany(Menu::class, 'parent_id', 'id')->orderBy('order');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parent()
    {
        return $this->belongsTo(Menu::class, 'parent_id', 'id');
    }

    /**
     * @param Builder $query
     *
     * @return Builder
     */
    public function scopeMain(Builder $query)
    {
        return $query->whereNull('parent_id');
    }

    /**
     * @return bool
     */
    public function hasActiveSubmenu()
    {
        $active = false;

        $this->submenu->each(function (Menu $menu) use (&$active) {
            if (Route::currentRouteName() == $menu->route) {
                $active = true;
            }
        });

        return $active;
    }
}
