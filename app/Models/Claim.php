<?php

namespace App\Models;

use App\Builders\HistoryBuilder;
use App\Status;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable as AuditableTrait;
use OwenIt\Auditing\Contracts\Auditable;

class Claim extends Model implements Auditable
{
    use Uuids, AuditableTrait;

    public static $initialStatus = 100;

    public $incrementing = false;

    protected $fillable = [
        'atm_id',
        'identifier',
        'incident_type',
        'attempt',
        'incident_datetime',
        'reported_emergency_services',
        'reported_sioc',
        'reported_police',
        'arrival_picket',
        'departure_picket',
        'other_atms_on_location',
        'other_atms_on_location_data',
        'securitybox_present',
        'securitybox_present_data',
        'surveillance_present',
        'surveillance_present_service',
        'surveillance_present_employee',
        'surveillance_present_number',
        'access_door_forced',
        'access_door_type',
        'access_door_data',
        'cilinders_secured',
        'cilinders_secured_amount',
        'vault_state',
        'valuables_present',
        'valuables_present_amount',
        'amount_missing',
        'confiscated_police',
        'weather_circumstances',
        'recorder_present',
        'recordings_present',
        'nearby_recorders',
        'nearby_recorders_description',
        'escape_vehicle',
        'suspects_amount',
        'suspects_signs',
        'abandoned_objects',
        'abandoned_objects_secured',
        'additional_information',
        'situation',
        'securitybox_number',
        'securitybox_location',
        'sim_number',
        'sim_location',
        'recorder_location',
        'cilinder_location',
        'rco_name',
        'rco_phone',
        'owner_company',
        'contacts',
        'witnesses',
        'last_servicing',
        'last_balance',
        'loot',
        'valuables_found',
        'transporter',
        'transporter_arrival',
        'transporter_code',
        'countcenter_code',
        'sealbags',
        'transporter_employees',
        'status_id',
    ];

    protected $casts = [
        'incident_datetime' => 'datetime',
        'reported_emergency_services' => 'datetime',
        'reported_sioc' => 'datetime',
        'reported_police' => 'datetime',
        'arrival_picket' => 'datetime',
        'departure_picket' => 'datetime',
        'last_servicing' => 'date',
        'contacts' => 'object',
        'witnesses' => 'object',
        'sealbags' => 'object',
        'transporter_employees' => 'object',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function atm()
    {
        return $this->belongsTo(ATM::class, 'atm_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function status()
    {
        return $this->belongsTo(Status::class);
    }

    /**
     * Generate tags for the audit
     *
     * @return array
     */
    public function generateTags(): array
    {
        return [
            $this->status->description,
        ];
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function history()
    {
        return (new HistoryBuilder)->getByClaim($this);
    }
}
