<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Functionality extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'description',
    ];
}
