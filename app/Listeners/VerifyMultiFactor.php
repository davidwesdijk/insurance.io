<?php

namespace App\Listeners;

use Illuminate\Auth\Events\Login;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class VerifyMultiFactor
{
    /**
     * Handle the event.
     *
     * @param  Login  $event
     * @return void
     */
    public function handle(Login $event)
    {
        if ($event->user->token_expiry > now()) {
            return;
        }

        $event->user->token_expiry->token = 123456;
        $event->user->save();

        return redirect()->to(route('verify-mfa'));
    }
}
