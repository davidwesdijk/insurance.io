<?php

namespace App\Repositories;


use App\Http\Requests\Atm\SaveAtmRequest;
use App\Models\ATM;
use Illuminate\Http\Request;
use Spatie\Geocoder\Facades\Geocoder;

class AtmRepository
{
    /**
     * @param Request $request
     *
     * @return ATM
     */
    public function createAtm(Request $request)
    {
        $location = Geocoder::getCoordinatesForAddress($request->address . ', ' . $request->zipcode . ', ' . $request->city . ', ' . $request->country);

        /** @var ATM $atm */
        $atm = ATM::create([
            'lat' => array_get($location, 'lat'),
            'lng' => array_get($location, 'lng'),
            'address' => $request->address,
            'zipcode' => $request->zipcode,
            'city' => $request->city,
            'country' => $request->country,
            'identifier' => $request->identifier,
            'serial_number' => $request->serial_number,
            'manufacturer' => $request->manufacturer,
            'year' => $request->year,
            'owner' => $request->owner,
            'location' => $request->location,
            'capacity' => $request->capacity,
            'commissioned' => $request->commissioned,
            'decommissioned' => $request->decommissioned,
            'status_id' => ATM::$initialStatus,
        ]);

        if ($request->functionalities) {
            $atm->functionalities()->sync($request->functionalities);
        }

        return $atm;
    }

    /**
     * @param ATM $atm
     * @param SaveAtmRequest $request
     *
     * @return ATM
     */
    public function updateAtm(ATM $atm, SaveAtmRequest $request)
    {
        $atm = $atm->fill($request->all());

        if ($request->functionalitiesHasChanged($atm)) {
            $atm->functionalities()->sync($request->functionalities);
        }

        if ($atm->isDirty()) {
            $atm->save();
        }

        return $atm;
    }

    /**
     * @param int $start
     * @param int $length
     *
     * @return mixed
     */
    public function getPaginatedResults($start = 0, $length = 10)
    {
        return ATM::skip($start)->take($length)->get()->transform(function (ATM $atm) {
            return [
                'id' => $atm->id,
                'identifier' => $atm->identifier,
                'owner' => $atm->owner,
                'address' => $atm->address,
                'zipcode' => $atm->zipcode,
                'city' => $atm->city,
                'country' => $atm->country,
                'active' => $atm->active,
                'url' => route('atms.show', $atm),
            ];
        });
    }

    /**
     * @return ATM[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getLocations()
    {
        return ATM::all()->transform(function (ATM $atm) {
            return [
                'id' => $atm->id,
                'lat' => $atm->lat,
                'lng' => $atm->lng,
                'identifier' => $atm->identifier,
                'owner' => $atm->owner,
                'address' => $atm->address,
                'zipcode' => $atm->zipcode,
                'city' => $atm->city,
                'country' => $atm->country,
                'active' => true,
                'url' => route('atms.show', $atm),
                'popup' => view('api.atm.popup')->with('atm', $atm)->render(),
            ];
        });
    }

    /**
     * @param ATM $atm
     * @param Request $request
     *
     * @return ATM
     */
    public function setLocation(ATM $atm, Request $request)
    {
        $atm->lat = $request->lat;
        $atm->lng = $request->lng;
        $atm->heading = $request->heading;
        $atm->pitch = $request->pitch;
        $atm->status_id = 6;
        $atm->save();

        return $atm;
    }
}