<?php

namespace App\Repositories;

use App\User;
use Illuminate\Http\Request;

class UserRepository
{
    /**
     * @param int $start
     * @param int $length
     *
     * @return mixed
     */
    public function getPaginatedResults($start = 0, $length = 10)
    {
        return User::skip($start)->take($length)->get()->transform(function (User $user) {
            return [
                'id' => $user->id,
                'full_name' => $user->full_name,
                'email' => $user->email,
                'role' => $user->getRoleNames()->implode(', '),
                'active' => $user->active,
                'url' => route('manage.users.show', $user),
            ];
        });
    }

    /**
     * @param Request $request
     *
     * @return mixed
     */
    public function createUser(Request $request)
    {
        $user = User::create([
            'first_name' => $request->first_name,
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'region' => $request->region,
            'password' => bcrypt($request->password),
            'active' => $request->active,
            'status_id' => User::$initialStatus,
        ]);

        $user->syncRoles([$request->role]);

        return $user;
    }

    /**
     * @param User $user
     * @param Request $request
     *
     * @return User
     */
    public function updateUser(User $user, Request $request)
    {
        $user = $user->fill($request->all());
        $user->syncRoles([$request->role]);

        if ($user->isDirty()) {
            $user->save();
        }

        return $user;
    }
}