<?php

namespace App\Repositories;

use App\Helpers\ClaimHelper;
use App\Http\Requests\Claims\SaveClaimRequest;
use App\Models\ATM;
use App\Models\Claim;
use App\Models\ClaimDetails;
use Illuminate\Http\Request;

class ClaimRepository
{
    /**
     * @param SaveClaimRequest $request
     *
     * @return mixed
     */
    public function createClaim(SaveClaimRequest $request)
    {
        $claim = Claim::create($request->all() + ['status_id' => Claim::$initialStatus]);

        return $claim;
    }

    /**
     * @param SaveClaimRequest $request
     * @param Claim $claim
     *
     * @return Claim
     */
    public function updateClaim(SaveClaimRequest $request, Claim $claim)
    {
        $claim->fill($request->all());
        $claim->save();

        return $claim;
    }

    /**
     * @param int $start
     * @param int $length
     *
     * @return mixed
     */
    public function getPaginatedResults($start = 0, $length = 10)
    {
        return Claim::skip($start)->take($length)->get()->transform(function (Claim $claim) {
            return [
                'id' => $claim->id,
                'identifier' => $claim->identifier,
                'date' => now()->toFormattedDateString(),
                'address' => $claim->atm->address,
                'city' => $claim->atm->city,
                'active' => $claim->active ?? true,
                'url' => route('claims.show', $claim),
            ];
        });
    }

    /**
     * @return ATM[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getLocations()
    {
        return ATM::all()->transform(function (ATM $atm) {
            return [
                'id' => $atm->id,
                'lat' => $atm->lat,
                'lng' => $atm->lng,
                'identifier' => $atm->identifier,
                'owner' => $atm->owner,
                'address' => $atm->address,
                'zipcode' => $atm->zipcode,
                'city' => $atm->city,
                'country' => $atm->country,
                'active' => true,
                'url' => route('atms.show', $atm),
                'popup' => view('api.atm.popup')->with('atm', $atm)->render(),
            ];
        });
    }

    /**
     * @param Claim $claim
     * @param Request $request
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function storeClaimDetail(Claim $claim, Request $request)
    {
        if ($request->has('detail')) {
            $detail = ClaimDetails::find($request->detail);

            $detail->fill([
                'type' => $request->type,
                'amount' => str_replace(',', '.', str_replace('.', '', $request->amount)),
                'data' => $request->data,
                'status_id' => $request->status_id,
                'order' => $request->index ?? 0,
            ]);

            return tap($detail, function ($detail) {
                $detail->save();
            });
        }

        return $claim->details()->create([
            'type' => $request->type,
            'amount' => str_replace(',', '.', str_replace('.', '', $request->amount)),
            'data' => $request->data,
            'status_id' => ClaimDetails::$initialStatus,
            'order' => $request->index ?? 0,
        ]);
    }
}