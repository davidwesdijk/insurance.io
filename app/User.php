<?php

namespace App;

use App\Models\Uuids;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;
use OwenIt\Auditing\Contracts\Auditable;
use \OwenIt\Auditing\Auditable as AuditableTrait;

class User extends Authenticatable implements Auditable
{
    use Notifiable, Uuids, HasRoles, AuditableTrait;

    public static $initialStatus = 500;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'first_name',
        'name',
        'email',
        'region',
        'phone',
        'password',
        'token',
        'token_expiry',
        'active',
        'status_id',
    ];

    public $incrementing = false;

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * @return string
     */
    public function getFullNameAttribute()
    {
        return trim($this->first_name . ' ' . $this->name);
    }

    /**
     * Encrypt the user's token.
     *
     * @param  string  $value
     * @return string
     */
    public function setTokenAttribute($value)
    {
        $this->attributes['token'] = encrypt($value);
    }

    /**
     * Decrypt the user's token.
     *
     * @param  string  $value
     * @return string
     */
    public function getTokenAttribute($value)
    {
        return decrypt($value);
    }

    /**
     * @param string $token
     *
     * @return bool
     */
    public function tokenIsValid(string $token)
    {
        return $this->token == $token;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function status()
    {
        return $this->belongsTo(Status::class);
    }

    /**
     * Generate tags for the audit
     *
     * @return array
     */
    public function generateTags(): array
    {
        return [
            $this->status->description,
        ];
    }
}
