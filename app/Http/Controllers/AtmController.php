<?php

namespace App\Http\Controllers;

use App\Http\Requests\Atm\SaveAtmRequest;
use App\Models\ATM;
use App\Repositories\AtmRepository;
use App\Response\SuccessResponse;
use Illuminate\Http\Request;

class AtmController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('pages.atms.index');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $this->setActive('atms.index');

        return view('pages.atms.create');
    }

    /**
     * @param SaveAtmRequest $request
     * @param AtmRepository $repository
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(SaveAtmRequest $request, AtmRepository $repository)
    {
        $atm = $repository->createAtm($request);

        return redirect()->to(route('atms.show', $atm))->with((new SuccessResponse)->toArray());
    }

    /**
     * @param ATM $atm
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function show(ATM $atm)
    {
        $this->setActive('atms.index');

        return view('pages.atms.show')->with('atm', $atm);
    }

    /**
     * @param ATM $atm
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function edit(ATM $atm)
    {
        $this->setActive('atms.index');

        return view('pages.atms.create')->with('atm', $atm);
    }

    /**
     * @param SaveAtmRequest $request
     * @param AtmRepository $repository
     * @param ATM $atm
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(SaveAtmRequest $request, AtmRepository $repository, ATM $atm)
    {
        $atm = $repository->updateAtm($atm, $request);

        return redirect()->to(route('atms.show', $atm))->with((new SuccessResponse)->toArray());
    }

    /**
     * @param Request $request
     * @param AtmRepository $repository
     * @param ATM $atm
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function setLocation(Request $request, AtmRepository $repository, ATM $atm)
    {
        $atm = $repository->setLocation($atm, $request);

        return redirect()->to(route('atms.show', $atm))->with((new SuccessResponse)->toArray());
    }
}
