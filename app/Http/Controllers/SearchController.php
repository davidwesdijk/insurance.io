<?php

namespace App\Http\Controllers;

use App\Builders\SearchBuilder;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Arr;

class SearchController extends Controller
{
    /**
     * TODO: Implement
     *
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function __invoke(Request $request, SearchBuilder $builder)
    {
        abort_if(!$request->has('query'), Response::HTTP_UNPROCESSABLE_ENTITY);

        $results = $builder->find(Arr::get($request->all(), 'query'));

        return view('template.partials.search-results')->with('results', $results);
    }
}
