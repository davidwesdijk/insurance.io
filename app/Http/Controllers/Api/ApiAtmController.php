<?php

namespace App\Http\Controllers\Api;

use App\Models\ATM;
use App\Repositories\AtmRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ApiAtmController extends Controller
{
    /**
     * @param Request $request
     * @param AtmRepository $repository
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request, AtmRepository $repository)
    {
        $total = ATM::count();

        $results = $repository->getPaginatedResults(
            ($request->pagination['page'] - 1) * $request->pagination['perpage'],
            $request->pagination['perpage']
        );

        return response()->json([
            'data' => $results,
            'meta' => [
                'page' => $request->pagination['page'] ?? 1,
                'pages' => ceil($total / $request->pagination['perpage']),
                'perpage' => $request->pagination['perpage'] ?? 1,
                'total' => $total,
            ],
        ]);
    }

    /**
     * @param AtmRepository $repository
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getLocations(AtmRepository $repository)
    {
        $results = $repository->getLocations();

        return response()->json([
            'data' => $results,
        ]);
    }
}
