<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\Claims\SaveClaimRequest;
use App\Models\ATM;
use App\Models\Claim;
use App\Repositories\AtmRepository;
use App\Repositories\ClaimRepository;
use App\Response\SuccessResponse;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ApiClaimController extends Controller
{
    /**
     * @param Request $request
     * @param ClaimRepository $repository
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request, ClaimRepository $repository)
    {
        $total = Claim::count();

        $results = $repository->getPaginatedResults(
            ($request->pagination['page'] - 1) * $request->pagination['perpage'],
            $request->pagination['perpage']
        );

        return response()->json([
            'data' => $results,
            'meta' => [
                'page' => $request->pagination['page'] ?? 1,
                'pages' => ceil($total / $request->pagination['perpage']),
                'perpage' => $request->pagination['perpage'] ?? 1,
                'total' => $total,
            ],
        ]);
    }

    /**
     * @param AtmRepository $repository
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getLocations(AtmRepository $repository)
    {
        $results = $repository->getLocations();

        return response()->json([
            'data' => $results,
        ]);
    }

    /**
     * @param SaveClaimRequest $request
     * @param ClaimRepository $repository
     *
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     */
    public function storeClaim(SaveClaimRequest $request, ClaimRepository $repository)
    {
        $claim = $repository->createClaim($request);

        return response()->json([
            'status' => 200,
            'claim' => $claim,
        ]);
    }

    /**
     * @param SaveClaimRequest $request
     * @param ClaimRepository $repository
     *
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     */
    public function updateClaim(SaveClaimRequest $request, ClaimRepository $repository, Claim $claim)
    {
        $claim = $repository->updateClaim($request, $claim);

        return response()->json([
            'status' => 200,
            'claim' => $claim,
        ]);
    }
}
