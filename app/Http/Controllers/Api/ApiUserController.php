<?php

namespace App\Http\Controllers\Api;

use App\Repositories\UserRepository;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ApiUserController extends Controller
{
    /**
     * @param Request $request
     * @param UserRepository $repository
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request, UserRepository $repository)
    {
        $total = User::count();

        $results = $repository->getPaginatedResults(
            ($request->pagination['page'] - 1) * $request->pagination['perpage'],
            $request->pagination['perpage']
        );

        return response()->json([
            'data' => $results,
            'meta' => [
                'page' => $request->pagination['page'] ?? 1,
                'pages' => ceil($total / $request->pagination['perpage']),
                'perpage' => $request->pagination['perpage'] ?? 1,
                'total' => $total,
            ],
        ]);
    }
}
