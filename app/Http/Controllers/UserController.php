<?php

namespace App\Http\Controllers;

use App\Repositories\UserRepository;
use App\Response\SuccessResponse;
use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('pages.manage.users.index');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('pages.manage.users.show');
    }

    /**
     * @param Request $request
     * @param UserRepository $repository
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request, UserRepository $repository)
    {
        $user = $repository->createUser($request);

        return redirect()->to(route('manage.users.index', $user))->with((new SuccessResponse)->toArray());
    }

    /**
     * @param User $user
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(User $user)
    {
        $this->setActive('manage.users.index');

        return view('pages.manage.users.show')->with('user', $user);
    }

    /**
     * @param Request $request
     * @param UserRepository $repository
     * @param User $user
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, UserRepository $repository, User $user)
    {
        $repository->updateUser($user, $request);

        return redirect()->to(route('manage.users.index'))->with((new SuccessResponse)->toArray());
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function setPassword(Request $request)
    {
        $user = User::find($request->id);

        $user->password = bcrypt($request->password);
        $user->save();

        return redirect()->to(route('manage.users.index'))->with((new SuccessResponse)->toArray());
    }
}
