<?php

namespace App\Http\Controllers\Settings;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LanguageController extends Controller
{
    /**
     * @param string $language
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function __invoke(string $language)
    {
        abort_if(!array_key_exists($language, config('app.locales')), 404);

        $cookie = cookie()->forever('locale', $language);

        return redirect()->back()->cookie($cookie);
    }
}
