<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MultiFactorController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show()
    {
        return view('auth.mfa');
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function verify(Request $request)
    {
        if ($request->token == 123456) {
            return redirect()->intended(route('dashboard'));
        }

        return redirect()->back()->withInput()->withErrors([
            'token' => __('Token is invalid or has expired.')
        ]);
    }
}
