<?php

namespace App\Http\Controllers;

use App\Helpers\ClaimHelper;
use App\Helpers\ReportHelper;
use App\Http\Requests\Claims\SaveClaimRequest;
use App\Models\ATM;
use App\Models\Claim;
use App\Repositories\ClaimRepository;
use App\Response\SuccessResponse;
use Illuminate\Http\Request;

class ClaimController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('pages.claims.index');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $this->setActive('claims.index');

        return view('pages.claims.select-atm');
    }

    /**
     * @param ATM $atm
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function createForAtm(ATM $atm)
    {
        $this->setActive('claims.index');

        return view('pages.claims.create')
            ->with('identifier', ClaimHelper::generateIdentifier($atm))
            ->with('atm', $atm);
    }

    /**
     * @param Claim $claim
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Claim $claim)
    {
        $this->setActive('claims.index');

        return view('pages.claims.show')->with('claim', $claim)->with('atm', $claim->atm);
    }

    /**
     * @param Claim $claim
     * @param int|null $step
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Claim $claim, int $step = null)
    {
        $this->setActive('claims.index');


        return view('pages.claims.create')
            ->with('claim', $claim)
            ->with('step', $step)
            ->with('atm', $claim->atm);
    }

    /**
     * @param Request $request
     * @param ClaimRepository $repository
     * @param Claim $claim
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function storeDetail(Request $request, ClaimRepository $repository, Claim $claim)
    {
        $repository->storeClaimDetail($claim, $request);

        return redirect()->back()->with((new SuccessResponse)->toArray());
    }

    /**
     * @param Claim $claim
     *
     * @return mixed
     */
    public function preview(Claim $claim)
    {
        return ReportHelper::getReportPreviewPdf($claim);
    }
}
