<?php

namespace App\Http\Requests\Atm;

use App\Models\ATM;
use Illuminate\Foundation\Http\FormRequest;

class SaveAtmRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'address' => 'required',
            'zipcode' => 'required',
            'city' => 'required',
            'country' => 'required',
            'identifier' => 'required',
            'serial_number' => 'required',
            'manufacturer' => 'required',
            'year' => 'required',
            'state' => 'required',
            'owner' => 'required',
            'location' => 'required',
            'capacity' => 'numeric',
            'commissioned' => 'date|nullable',
            'decommissioned' => 'date|nullable',
            'status_id' => 'required_if:action,update',
        ];
    }

    /**
     * @param ATM $atm
     *
     * @return bool
     */
    public function functionalitiesHasChanged(ATM $atm)
    {
        $requestFunctionalities = implode('#', $this->get('functionalities', []));
        $atmFunctionalities = implode('#', $atm->functionalities->pluck('id')->toArray());

        return $requestFunctionalities != $atmFunctionalities;
    }

    /**
     * Format the capacity by unmasking the value
     */
    private function formatCapacity()
    {
        if ($this->request->has('capacity')){
            $this->merge([
                'capacity' => str_replace('.', '', $this->request->get('capacity')),
            ]);
        }
    }

    /**
     * @return \Illuminate\Contracts\Validation\Validator|void
     */
    public function prepareForValidation()
    {
        $this->formatCapacity();

        parent::prepareForValidation();
    }
}
