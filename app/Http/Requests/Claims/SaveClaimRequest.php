<?php

namespace App\Http\Requests\Claims;

use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Arr;

class SaveClaimRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'atm_id' => 'exists:atms,id',
        ];
    }

    /**
     * Combine date and time field
     */
    private function prepareRequest()
    {
        if (is_array($this->request->get('witnesses')) && count($this->request->get('witnesses')) == 1) {
            if (!Arr::get(Arr::first($this->request->get('witnesses')), 'name')) {
                $this->merge([
                    'witnesses' => []
                ]);
            }
        }

        if (is_array($this->request->get('contacts')) && count($this->request->get('contacts')) == 1) {
            if (!Arr::get(Arr::first($this->request->get('contacts')), 'name')) {
                $this->merge([
                    'contacts' => []
                ]);
            }
        }

        if (is_array($this->request->get('sealbags')) && count($this->request->get('sealbags')) == 1) {
            if (!Arr::get(Arr::first($this->request->get('sealbags')), 'sealbag')) {
                $this->merge([
                    'sealbags' => []
                ]);
            }
        }

        if (is_array($this->request->get('transporter_employees')) && count($this->request->get('transporter_employees')) == 1) {
            if (!Arr::get(Arr::first($this->request->get('transporter_employees')), 'name')) {
                $this->merge([
                    'transporter_employees' => []
                ]);
            }
        }

        $this->merge([
            'incident_datetime' => $this->request->get('incident_datetime') ? str_replace('T', ' ', $this->request->get('incident_datetime')) . ':00' : null,
            'reported_emergency_services' => $this->request->get('incident_datetime') ? str_replace('T', ' ', $this->request->get('incident_datetime')) . ':00' : null,
            'reported_sioc' => $this->request->get('incident_datetime') ? str_replace('T', ' ', $this->request->get('incident_datetime')) . ':00' : null,
            'reported_police' => $this->request->get('incident_datetime') ? str_replace('T', ' ', $this->request->get('incident_datetime')) . ':00' : null,
            'arrival_picket' => $this->request->get('incident_datetime') ? str_replace('T', ' ', $this->request->get('incident_datetime')) . ':00' : null,
            'departure_picket' => $this->request->get('incident_datetime') ? str_replace('T', ' ', $this->request->get('incident_datetime')) . ':00' : null,
        ]);
    }

    /**
     * @return \Illuminate\Contracts\Validation\Validator|void
     */
    public function prepareForValidation()
    {
        $this->prepareRequest();

        parent::prepareForValidation();
    }
}
