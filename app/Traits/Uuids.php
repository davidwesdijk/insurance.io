<?php

namespace App\Models;

use Illuminate\Support\Str;

trait Uuids
{
    /**
     * Hook in the boot method of Eloquent models
     */
    public static function bootUuids()
    {
        static::creating(function ($model) {
            $model->{$model->getKeyName()} = (string)Str::orderedUuid();
        });
    }
}
