<?php

namespace App\Builders;

use App\Models\Navigation\Menu;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;

class NavigationBuilder
{
    private $cache_time = 0;

    /**
     * @return Collection
     */
    public static function get(): Collection
    {
        return (new self)->build();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    private function build()
    {
//        return Cache::remember($this->getKey(), $this->cache_time, function () {
            return Menu::main()->orderBy('order')->get();
//        });
    }

    /**
     * @param string $key
     *
     * @return string
     */
    private function getKey($key = '')
    {
        if (auth()->user()) {
            $key = $key . '_';
        }

        return $key . 'navigation';
    }
}