<?php

namespace App\Builders;

use App\Models\ATM;
use App\Models\Claim;
use Illuminate\Support\Collection;
use OwenIt\Auditing\Models\Audit;

class HistoryBuilder
{
    /** @var Collection */
    private $history;

    /**
     * HistoryBuilder constructor.
     */
    public function __construct()
    {
        $this->history = new Collection();
    }

    /**
     * @param ATM $atm
     *
     * @return Collection
     */
    public function getByATM(ATM $atm)
    {
        $this->history = $this->history->merge($atm->claims->reverse());
        $this->history = $this->history->merge($atm->audits->reverse());

        return $this->history;
    }

    /**
     * @param Claim $claim
     *
     * @return Collection
     */
    public function getByClaim(Claim $claim)
    {
        $this->history = $this->history->merge($claim->audits->reverse());

        return $this->history;
    }
}