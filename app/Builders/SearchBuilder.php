<?php

namespace App\Builders;

use App\Models\ATM;
use Illuminate\Support\Collection;

class SearchBuilder
{
    private $results;
    private $user;
    private $query;

    /**
     * SearchBuilder constructor.
     */
    public function __construct()
    {
        $this->results = new Collection();
        $this->user = auth()->user();
    }

    /**
     * @param string $query
     *
     * @return \Illuminate\Support\Collection
     */
    public function find(string $query)
    {
        $this->query = $query;

        return $this->fetchAll();
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    private function fetchAll()
    {
        if ($this->user->can('read atms')) {
            $this->results->offsetSet('atms', $this->fetchAtms());
        }

        return $this->results;
    }

    /**
     * @return Collection
     */
    private function fetchAtms()
    {
        /** @var Collection $matches */
        $matches = ATM::query()
            ->whereLike('address', $this->query)
            ->whereLike('zipcode', $this->query)
            ->whereLike('city', $this->query)
            ->whereLike('country', $this->query)
            ->whereLike('manufacturer', $this->query)
            ->whereLike('identifier', $this->query)
            ->whereLike('serial_number', $this->query)
            ->get();

        return new Collection([
            'count' => $matches->count(),
            'results' => $matches->take(5),
        ]);
    }
}