<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class MakeScreenshots extends DuskTestCase
{
    /**
     * @throws \Throwable
     */
    public function testGetMap()
    {
        global $argv;

        $this->browse(function (Browser $browser) use ($argv) {
            Browser::$storeScreenshotsAt = storage_path('');

            $browser->ensurejQueryIsAvailable();

            $browser->visit(route('atms.maps.map', $argv[4]))
                ->waitFor('*', 3)
                ->screenshot($argv[4] . '-map.png');
        });
    }
}
