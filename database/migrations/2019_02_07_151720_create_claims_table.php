<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClaimsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('claims', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('identifier')->nullable();
            $table->uuid('atm_id');

            $table->string('incident_type')->nullable();
            $table->boolean('attempt')->default(false);
            $table->dateTime('incident_datetime')->nullable();
            $table->dateTime('reported_emergency_services')->nullable();
            $table->dateTime('reported_sioc')->nullable();
            $table->dateTime('reported_police')->nullable();
            $table->dateTime('arrival_picket')->nullable();
            $table->dateTime('departure_picket')->nullable();
            $table->boolean('other_atms_on_location')->default(false);
            $table->text('other_atms_on_location_data')->nullable();
            $table->boolean('securitybox_present')->default(false);
            $table->string('securitybox_present_data')->nullable();
            $table->boolean('surveillance_present')->default(false);
            $table->string('surveillance_present_service')->nullable();
            $table->string('surveillance_present_employee')->nullable();
            $table->string('surveillance_present_number')->nullable();
            $table->boolean('access_door_forced')->default(false);
            $table->string('access_door_type')->nullable();
            $table->string('access_door_data')->nullable();
            $table->boolean('cilinders_secured')->default(false);
            $table->string('cilinders_secured_amount')->nullable();
            $table->string('vault_state')->nullable();
            $table->boolean('valuables_present')->default(false);
            $table->string('valuables_present_amount')->nullable();
            $table->string('amount_missing')->nullable();
            $table->string('confiscated_police')->nullable();
            $table->string('weather_circumstances')->nullable();
            $table->boolean('recorder_present')->default(false);
            $table->boolean('recordings_present')->default(false);
            $table->boolean('nearby_recorders')->default(false);
            $table->string('nearby_recorders_description')->nullable();
            $table->string('escape_vehicle')->nullable();
            $table->string('suspects_amount')->nullable();
            $table->string('suspects_signs')->nullable();
            $table->string('abandoned_objects')->nullable();
            $table->string('abandoned_objects_secured')->nullable();
            $table->text('additional_information')->nullable();

            $table->longText('situation')->nullable();

            $table->string('securitybox_number')->nullable();
            $table->string('securitybox_location')->nullable();
            $table->string('sim_number')->nullable();
            $table->string('sim_location')->nullable();
            $table->string('recorder_location')->nullable();
            $table->string('cilinder_location')->nullable();
            $table->string('rco_name')->nullable();
            $table->string('rco_phone')->nullable();
            $table->string('owner_company')->nullable();

            $table->text('contacts')->nullable();
            $table->text('witnesses')->nullable();

            $table->date('last_servicing')->nullable();
            $table->decimal('last_balance', 11, 2)->nullable();
            $table->decimal('loot', 11, 2)->nullable();
            $table->decimal('valuables_found', 11, 2)->nullable();
            $table->string('transporter')->nullable();
            $table->time('transporter_arrival')->nullable();
            $table->string('transporter_code')->nullable();
            $table->string('countcenter_code')->nullable();

            $table->text('sealbags')->nullable();
            $table->text('transporter_employees')->nullable();

            $table->unsignedInteger('status_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('claims');
    }
}
