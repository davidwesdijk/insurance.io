<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToAtmsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('atms', function (Blueprint $table) {
            $table->decimal('pitch', 20, 16)->after('lng')->default(0);
            $table->decimal('heading', 20, 16)->after('lng')->default(0);
            $table->string('serial_number')->after('identifier')->nullable();

            $table->dropColumn('state');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('atms', function (Blueprint $table) {
            $table->dropColumn('pitch');
            $table->dropColumn('heading');
            $table->dropColumn('serial_number');

            $table->string('state')->after('year')->nullable();
        });
    }
}
