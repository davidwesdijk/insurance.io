<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('region')->after('email_verified_at')->nullable();
            $table->string('phone')->after('email_verified_at')->nullable();
            $table->unsignedInteger('status_id')->after('token_expiry')->nullable();
            $table->boolean('active')->after('token_expiry')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('region');
            $table->dropColumn('phone');
            $table->dropColumn('active');
            $table->dropColumn('status_id');
        });
    }
}
