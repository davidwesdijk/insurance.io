<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateATMsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('atms', function (Blueprint $table) {
            $table->uuid('id')->primary();

            $table->string('address')->nullable();
            $table->string('zipcode')->nullable();
            $table->string('city')->nullable();
            $table->string('country')->nullable();

            $table->decimal('lat', 11, 7);
            $table->decimal('lng', 11, 7);

            $table->string('manufacturer')->nullable();
            $table->string('identifier')->nullable();
            $table->year('year')->nullable();
            $table->string('state')->nullable();
            $table->string('owner')->nullable();
            $table->string('location')->nullable();
            $table->unsignedInteger('capacity')->nullable();

            $table->date('commissioned')->nullable();
            $table->date('decommissioned')->nullable();

            $table->unsignedInteger('status_id')->default(1);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('atms');
    }
}
