<?php

use App\Models\Functionality;
use Illuminate\Database\Seeder;

class FunctionalitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Functionality::truncate();

        Functionality::create([
            'description' => 'ATM',
        ]);

        Functionality::create([
            'description' => 'Sealbag',
        ]);

        Functionality::create([
            'description' => 'Deposit',
        ]);
    }
}
