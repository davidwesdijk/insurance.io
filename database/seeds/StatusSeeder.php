<?php

use App\Models\ATM;
use App\Models\Claim;
use App\Models\ClaimDetails;
use App\Status;
use App\User;
use Illuminate\Database\Seeder;

class StatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Status::truncate();

        Status::create([
            'id' => 1,
            'description' => 'Created',
            'type' => ATM::class,
        ]);
        Status::create([
            'id' => 2,
            'description' => 'General change',
            'type' => ATM::class,
        ]);
        Status::create([
            'id' => 3,
            'description' => 'Functionality change',
            'type' => ATM::class,
        ]);
        Status::create([
            'id' => 4,
            'description' => 'Availability change',
            'type' => ATM::class,
        ]);
        Status::create([
            'id' => 5,
            'description' => 'Removed',
            'type' => ATM::class,
        ]);
        Status::create([
            'id' => 6,
            'description' => 'Location set',
            'type' => ATM::class,
        ]);

        Status::create([
            'id' => 100,
            'description' => 'Created',
            'type' => Claim::class,
        ]);
        Status::create([
            'id' => 101,
            'description' => 'General update',
            'type' => Claim::class,
        ]);
        Status::create([
            'id' => 102,
            'description' => 'Report by expert',
            'type' => Claim::class,
        ]);
        Status::create([
            'id' => 103,
            'description' => 'Closed',
            'type' => Claim::class,
        ]);

        Status::create([
            'id' => 300,
            'description' => 'Created',
            'type' => ClaimDetails::class,
        ]);
        Status::create([
            'id' => 301,
            'description' => 'General update',
            'type' => ClaimDetails::class,
        ]);

        Status::create([
            'id' => 500,
            'description' => 'Created',
            'type' => User::class,
        ]);
        Status::create([
            'id' => 501,
            'description' => 'Authorisation changed',
            'type' => User::class,
        ]);
        Status::create([
            'id' => 502,
            'description' => 'Password reset',
            'type' => User::class,
        ]);
        Status::create([
            'id' => 503,
            'description' => 'General update',
            'type' => User::class,
        ]);
        Status::create([
            'id' => 504,
            'description' => 'Removed',
            'type' => User::class,
        ]);
    }
}
