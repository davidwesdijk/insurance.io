<?php

use App\Models\ATM;
use Illuminate\Database\Seeder;

class AtmSeeder extends Seeder
{
    /** @var \Illuminate\Support\Collection */
    private $import;

    private $functionalities = [
        'Geldautomaat' => 1,
        'Geld storten en opnemen' => 3,
        'Sealbag-automaat' => 2,
        'Geldstortautomaat' => 3,
        'Muntrol-automaat' => 1,
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->import = $this->getData();

        foreach ($this->import as $import) {

            try {
                $result = [];
                $location = \Spatie\Geocoder\Facades\Geocoder::getAddressForCoordinates($import->latitude, $import->longitude);
                preg_match('/[1-9][0-9]{3} ?[A-Z]{2}/', $location['formatted_address'], $result);
                $address = $location['address_components'][1]->long_name . ' ' . $location['address_components'][0]->long_name;
                $zipcode = $result[0];
                $city = $location['address_components'][2]->long_name;
                /** @var ATM $atm */
                $atm = ATM::create([
                    'identifier' => $import->id,
                    'owner' => $import->owner,
                    'lat' => $import->latitude,
                    'lng' => $import->longitude,
                    'address' => $address,
                    'zipcode' => $zipcode,
                    'city' => $city,
                    'country' => 'NL',
                    'status_id' => 1,
                ]);
                foreach ($import->functionality ?? [] as $functionality) {
                    if (isset($this->functionalities[$functionality])) {
                        $atm->functionalities()->detach($this->functionalities[$functionality]);
                        $atm->functionalities()->attach($this->functionalities[$functionality]);
                    }
                }
            } catch (Exception $e) {
                dump($e->getMessage(), $import->id);
            }

            sleep(2);
        }
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    private function getData()
    {
        $content = json_decode(file_get_contents(database_path('import/atm.json')));

        return collect($content);
    }
}
