<?php

use App\Models\Navigation\Menu;
use Illuminate\Database\Seeder;

class MenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Menu::truncate();

        foreach ($this->getStructure() as $menu) {
            $parent = Menu::create([
                'label' => $menu['label'],
                'route' => $menu['route'],
                'icon' => $menu['icon'] ?? null,
                'permission' => $menu['permission'],
                'order' => $menu['order'],
            ]);

            if ($menu['children']->isNotEmpty()) {
                foreach ($menu['children'] as $child) {
                    Menu::create([
                        'label' => $child['label'],
                        'route' => $child['route'],
                        'icon' => $child['icon'],
                        'permission' => $child['permission'],
                        'order' => $child['order'],
                        'parent_id' => $parent->id,
                    ]);
                }
            }
        }
    }

    private function getStructure()
    {
        return collect([
            [
                'label' => 'Dashboard',
                'route' => null,
                'permission' => 'view general reports',
                'order' => 1,
                'children' => collect([
                    [
                        'label' => 'General',
                        'route' => 'dashboard',
                        'icon' => 'flaticon-analytics',
                        'permission' => 'view general reports',
                        'order' => 1,
                    ],
                    [
                        'label' => 'ATMs',
                        'route' => 'dashboard.atms',
                        'icon' => 'flaticon-analytics',
                        'permission' => 'view extended reports',
                        'order' => 2,
                    ],
                    [
                        'label' => 'Claims',
                        'route' => 'dashboard.claims',
                        'icon' => 'flaticon-analytics',
                        'permission' => 'view extended reports',
                        'order' => 2,
                    ],
                ]),
            ],
            [
                'label' => 'ATMs',
                'route' => 'atms.index',
                'permission' => 'read atms',
                'order' => 2,
                'children' => collect([]),
            ],
            [
                'label' => 'Claims',
                'route' => 'claims.index',
                'permission' => 'read claims',
                'order' => 3,
                'children' => collect([]),
            ],
            [
                'label' => 'Manage',
                'route' => null,
                'permission' => 'manage users',
                'order' => 4,
                'children' => collect([
                    [
                        'label' => 'Users',
                        'route' => 'manage.users.index',
                        'icon' => 'flaticon-users',
                        'permission' => 'manage users',
                        'order' => 1,
                    ],
                    [
                        'label' => 'Roles & permissions',
                        'route' => 'manage.roles.index',
                        'icon' => 'flaticon-safe-shield-protection',
                        'permission' => 'manage users',
                        'order' => 2,
                    ],
                ])
            ]
        ]);
    }
}
