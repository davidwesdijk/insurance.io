<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Spatie\Permission\PermissionRegistrar;

class RolesAndPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     * @return void
     */
    public function run()
    {
        app()[PermissionRegistrar::class]->forgetCachedPermissions();

        Permission::updateOrCreate(['name' => 'view general reports']);
        Permission::updateOrCreate(['name' => 'view extended reports']);

        Permission::updateOrCreate(['name' => 'create atms']);
        Permission::updateOrCreate(['name' => 'read atms']);
        Permission::updateOrCreate(['name' => 'update atms']);
        Permission::updateOrCreate(['name' => 'delete atms']);

        Permission::updateOrCreate(['name' => 'create claims']);
        Permission::updateOrCreate(['name' => 'read claims']);
        Permission::updateOrCreate(['name' => 'update claims']);
        Permission::updateOrCreate(['name' => 'delete claims']);

        Permission::updateOrCreate(['name' => 'manage users']);

        Role::updateOrCreate(['name' => 'administrator'])->givePermissionTo(Permission::all());

        Role::updateOrCreate(['name' => 'manager'])->givePermissionTo([
            'view general reports',
            'view extended reports',
            'create atms',
            'read atms',
            'update atms',
            'delete atms',
            'create claims',
            'read claims',
            'update claims',
            'delete claims',
        ]);

        Role::updateOrCreate(['name' => 'employee'])->givePermissionTo([
            'view general reports',
            'read atms',
            'create claims',
            'read claims',
            'update claims',
        ]);

        Role::updateOrCreate(['name' => 'expert'])->givePermissionTo([
            'view general reports',
            'read atms',
            'create claims',
            'read claims',
            'update claims',
        ]);

        Role::updateOrCreate(['name' => 'controller'])->givePermissionTo([
            'view general reports',
            'read atms',
            'read claims',
        ]);

        Role::updateOrCreate(['name' => 'insurer'])->givePermissionTo([
            'view general reports',
            'read claims',
        ]);
    }
}
