<?php

use App\Base;
use Illuminate\Database\Seeder;

class BaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Base::create([
            'type' => 'incident_type',
            'key' => 'actual demolition',
        ]);
        Base::create([
            'type' => 'incident_type',
            'key' => 'failed demolition',
        ]);
        Base::create([
            'type' => 'incident_type',
            'key' => 'accident',
        ]);

        Base::create([
            'type' => 'vault_state',
            'key' => 'open',
        ]);
        Base::create([
            'type' => 'vault_state',
            'key' => 'closed',
        ]);

        Base::create([
            'type' => 'weather_circumstances',
            'key' => 'dry',
        ]);
        Base::create([
            'type' => 'weather_circumstances',
            'key' => 'rainy',
        ]);
        Base::create([
            'type' => 'weather_circumstances',
            'key' => 'snow',
        ]);

        Base::create([
            'type' => 'value_transporters',
            'key' => 'G4S',
        ]);
        Base::create([
            'type' => 'value_transporters',
            'key' => 'SecurCash',
        ]);
    }
}
