@php
    /** @var \App\Models\ClaimDetails $details */
    $detail = $detail ?? null;
    $index = $index ?? null;
@endphp

<div class="m-portlet m-portlet--mobile m-portlet--focus m-portlet--head-solid-bg detail_container" data-index="{{ $index ?? '{index}' }}">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    <span class="m-portlet__head-icon">
                        <i class="la la-exclamation-triangle"></i>
                    </span>
                    <span class="detail_title">{{ isset($detail) ? __('Subject') : __('New subject') }}</span> <small class="detail_type">{{ $detail->type ?? null ? __($detail->type) : '' }}</small>
                </h3>
            </div>
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
                <li class="m-portlet__nav-item">
                    <a href="#" class="m-portlet__nav-link m-portlet__nav-link--icon toggle">
                        <i class="la {{ $detail ? 'la-angle-up' : 'la-angle-down' }}"></i>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <form action="{{ route('claims.store-detail', $claim) }}" class="detail_content" method="post" {!! $detail ? 'style="display: none;"' : '' !!}>
        <input type="hidden" name="index" value="{{ $index ?? '{index}' }}">
        @csrf

        @if ($detail)
            <input type="hidden" name="detail" value="{{ $detail->id }}">
        @endif

        <div class="m-portlet__body">
            <div class="m-form__section m-form__section--first">
                <div class="form-group m-form__group m-form__group--md row">
                    <label class="col-lg-3 col-form-label">{{ __('Type') }}:</label>
                    <div class="col-lg-6">
                        <select name="type" class="form-control m-input">
                            <option value="" disabled {{ ($detail->type ?? null) ? '' : 'selected' }}>{{ __('Select a type of claim to continue...') }}</option>
                            <option value="loot" {{ ($detail->type ?? '') == 'loot' ? 'selected' : '' }}>{{ __('Loot') }}</option>
                            <option value="material" {{ ($detail->type ?? '') == 'material' ? 'selected' : '' }}>{{ __('Material') }}</option>
                            <option value="periphery" {{ ($detail->type ?? '') == 'periphery' ? 'selected' : '' }}>{{ __('Periphery') }}</option>
                            <option value="victim" {{ ($detail->type ?? '') == 'victim' ? 'selected' : '' }}>{{ __('Victim') }}</option>
                            <option value="other" {{ ($detail->type ?? '') == 'other' ? 'selected' : '' }}>{{ __('Other') }}</option>
                        </select>
                        @if ($errors->has('type'))
                            <div class="form-control-feedback">{{ $errors->first('type') }}</div>
                        @endif
                    </div>
                </div>
            </div>

            <div class="detail_body_content" style="{{ $detail ? '' : 'display: none;' }}">
                <div class="m-form__section m-form__section">
                    <div class="form-group m-form__group m-form__group--md row">
                        <label class="col-lg-3 col-form-label required">{{ __('Estimated amount') }}:</label>
                        <div class="col-lg-4">
                            <div class="input-group m-input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">&euro;</span>
                                </div>
                                <input type="text" class="form-control m-input" name="amount" aria-describedby="basic-addon1"
                                       value="{{ $detail->amount ?? '' }}"
                                       data-mask="#.##0,00" data-mask-reverse="true">
                            </div>
                        </div>
                    </div>

                    <div class="form-group m-form__group m-form__group--md row">
                        <label class="col-lg-3 col-form-label required">{{ __('Additional information') }}:</label>
                        <div class="col-lg-9">
                            <textarea class="form-control m-input" name="data" rows="5">{{ $detail->data ?? '' }}</textarea>
                        </div>
                    </div>
                </div>

                @if ($detail)
                    <div class="m-separator m-separator--dashed m-separator--lg"></div>

                    <div class="m-alert m-alert--icon m-alert--icon-solid m-alert--outline alert alert-{{ $errors->has('status_id') ? 'danger' : 'info' }} fade show" role="alert">
                        <div class="m-alert__icon">
                            <i class="flaticon-exclamation-1"></i>
                            <span></span>
                        </div>
                        <div class="m-alert__text">
                            {{ __('While editing an existing claim, please select the proper reason for editing this entity:') }}<br>
                            <div class="row mt-3">
                                <div class="col-lg-6">
                                    <input type="hidden" name="action" value="update">
                                    <select name="status_id" class="form-control m-input">
                                        <option value="" disabled selected>{{ __('Select a status') }}</option>
                                        @foreach (\App\Status::detail()->get() as $status)
                                            <option {{ old('status_id') == $status->id ? 'selected' : '' }} value="{{ $status->id }}">{{ __($status->description) }}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('status_id'))
                                        <div class="form-control-feedback mt-2">{{ $errors->first('status_id') }}</div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
        <div class="m-portlet__foot m--align-right">
            <button type="submit" class="btn btn-focus m-btn m-btn--icon m-btn--wide">
                <i class="la la-check"></i>
                {{ __('Save') }}
            </button>
            <span class="m--margin-left-10">{{ __('or') }} <a href="javascript:;" class="m-link m--font-bold detail_close">{{ __('cancel') }}</a></span>
        </div>
    </form>
</div>
@unset($detail, $index)