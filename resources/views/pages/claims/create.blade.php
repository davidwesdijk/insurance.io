@extends ('template.default-2019')

@isset ($claim)
    @section ('title', __('Edit report'))
@else
    @section ('title', __('New report'))
@endisset

@section ('content')
    <div class="m-portlet m-portlet--full-height">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon">
                        <i class="la la-exclamation-triangle"></i>
                    </span>
                    <h3 class="m-portlet__head-text m--font-focus">{{ isset($claim) ? __('Edit claim report') : __('Report new claim') }}</h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                <ul class="m-portlet__nav">
                    <li class="m-portlet__nav-item">
                        <a href="#" data-toggle="m-tooltip" class="m-portlet__nav-link m-portlet__nav-link--icon" data-direction="left" data-width="auto" title="Get help with filling up this form">
                            <i class="flaticon-info m--icon-font-size-lg3"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>

        <div class="m-portlet__body m-portlet__body--no-padding">
            <div class="m-wizard m-wizard--3 m-wizard--success" id="m_wizard">
                <div class="m-portlet__padding-x">
                    <!-- Here you can put a message or alert -->
                </div>

                <div class="row m-row--no-padding">
                    <div class="col-xl-3 col-lg-12">
                        <div class="m-wizard__head">
                            <div class="m-wizard__progress">
                                <div class="progress">
                                    <div class="progress-bar"  role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>

                            <div class="m-wizard__nav">
                                <div class="m-wizard__steps">
                                    <div class="m-wizard__step m-wizard__step--current" m-wizard-target="m_wizard_form_step_1">
                                        <div class="m-wizard__step-info">
                                            <a href="#" class="m-wizard__step-number">
                                                <span><span>1</span></span>
                                            </a>
                                            <div class="m-wizard__step-line"><span></span></div>
                                            <div class="m-wizard__step-label">{{ __('General') }}</div>
                                        </div>
                                    </div>
                                    <div class="m-wizard__step" m-wizard-target="m_wizard_form_step_2">
                                        <div class="m-wizard__step-info">
                                            <a href="#" class="m-wizard__step-number">
                                                <span><span>2</span></span>
                                            </a>
                                            <div class="m-wizard__step-line"><span></span></div>
                                            <div class="m-wizard__step-label">{{ __('Situation') }}</div>
                                        </div>
                                    </div>
                                    <div class="m-wizard__step" m-wizard-target="m_wizard_form_step_3">
                                        <div class="m-wizard__step-info">
                                            <a href="#" class="m-wizard__step-number">
                                                <span><span>3</span></span>
                                            </a>
                                            <div class="m-wizard__step-line"><span></span></div>
                                            <div class="m-wizard__step-label">{{ __('Components') }}</div>
                                        </div>
                                    </div>
                                    <div class="m-wizard__step" m-wizard-target="m_wizard_form_step_4">
                                        <div class="m-wizard__step-info">
                                            <a href="#" class="m-wizard__step-number">
                                                <span><span>4</span></span>
                                            </a>
                                            <div class="m-wizard__step-line"><span></span></div>
                                            <div class="m-wizard__step-label">{{ __('Contacts') }}</div>
                                        </div>
                                    </div>
                                    <div class="m-wizard__step" m-wizard-target="m_wizard_form_step_5">
                                        <div class="m-wizard__step-info">
                                            <a href="#" class="m-wizard__step-number">
                                                <span><span>5</span></span>
                                            </a>
                                            <div class="m-wizard__step-line"><span></span></div>
                                            <div class="m-wizard__step-label">{{ __('Witnesses') }}</div>
                                        </div>
                                    </div>
                                    <div class="m-wizard__step" m-wizard-target="m_wizard_form_step_6">
                                        <div class="m-wizard__step-info">
                                            <a href="#" class="m-wizard__step-number">
                                                <span><span>6</span></span>
                                            </a>
                                            <div class="m-wizard__step-line"><span></span></div>
                                            <div class="m-wizard__step-label">{{ __('Values') }}</div>
                                        </div>
                                    </div>
                                    <div class="m-wizard__step" m-wizard-target="m_wizard_form_step_7">
                                        <div class="m-wizard__step-info">
                                            <a href="#" class="m-wizard__step-number">
                                                <span><span>7</span></span>
                                            </a>
                                            <div class="m-wizard__step-line"><span></span></div>
                                            <div class="m-wizard__step-label">{{ __('Confirmation') }}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-9 col-lg-12">
                        <div class="m-wizard__form pt-5">
                            <form class="m-form m-form--label-align-left- m-form--state-" id="m_form" action="{{ isset($claim) ? route('api.claims.update', $claim) : route('api.claims.store') }}" method="post">
                                @csrf
                                <input type="hidden" name="atm_id" value="{{ $atm->id }}">
                                <div class="m-portlet__body m-portlet__body--no-padding">

                                    {{--1--}}
                                    @include('pages.atms.report.general')

                                    {{--2--}}
                                    @include('pages.atms.report.situation')

                                    {{--3--}}
                                    @include('pages.atms.report.components')

                                    {{--4--}}
                                    @include('pages.atms.report.contacts')

                                    {{--5--}}
                                    @include('pages.atms.report.witnesses')

                                    {{--6--}}
                                    @include('pages.atms.report.values')

                                    {{--7--}}
                                    @include('pages.atms.report.confirmation')

                                </div>

                                <div class="m-portlet__foot m-portlet__foot--fit m--margin-top-40">
                                    <div class="m-form__actions">
                                        <div class="row">
                                            <div class="col-lg-6 m--align-left">
                                                <a href="#" class="btn btn-secondary m-btn m-btn--custom m-btn--icon" data-wizard-action="prev">
                                                    <span>
                                                        <i class="la la-arrow-left"></i>&nbsp;&nbsp;
                                                        <span>{{ __('Back') }}</span>
                                                    </span>
                                                </a>
                                            </div>
                                            <div class="col-lg-6 m--align-right">
                                                <a href="#" class="btn btn-success m-btn m-btn--custom m-btn--icon" data-wizard-action="submit">
                                                    <span>
                                                        <i class="la la-check"></i>&nbsp;&nbsp;
                                                        <span>{{ __('Save & close') }}</span>
                                                    </span>
                                                </a>
                                                <a href="#" class="btn btn-focus m-btn m-btn--custom m-btn--icon" data-wizard-action="next">
                                                    <span>
                                                        <span>{{ __('Next step') }}</span>&nbsp;&nbsp;
                                                        <i class="la la-arrow-right"></i>
                                                    </span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push ('styles')
    <style>
        p.form-control-static {
            margin-top: 10px;
        }
    </style>
@endpush

@push ('scripts')
    <script>
        var step = {{ isset($claim) ? ($step ?? 1) : 1 }};

        $(function () {
            var e, r, form = $("#m_form");

            $('select').selectpicker();

            wizard.init();

            form.find('[data-show-if-toggled]').trigger('change');
            form.on('change', '[data-show-if-toggled]', function() {
                var el = $(this);
                if (el.is(':checked')) {
                    $('.' + el.data('show-if-toggled')).show();
                } else {
                    $('.' + el.data('show-if-toggled')).hide();
                    $('.' + el.data('show-if-toggled')).find('input').val('');
                }
            });

            form.find('[data-show-if-toggled]').trigger('change');
        });

        var wizard = function () {
            return {
                init: function () {
                    var n;
                    $("#m_wizard"), form = $("#m_form"), (r = new mWizard("m_wizard", {startStep: step})).on("beforeNext", function (r) {
                        !0 !== e.form() && r.stop()
                    }), r.on("change", function (e) {
                        @isset ($claim)
                            history.replaceState(null, null, '{{ route('claims.edit', $claim) }}/' + e.currentStep);
                        @endisset
                        mUtil.scrollTop()
                    }), e = form.validate({
                        ignore: ":hidden",
                        rules: {
                            incident_type: {required: true},
                            incident_datetime: {required: true},
                            reported_emergency_services: {required: true},
                            reported_sioc: {required: true},
                            reported_police: {required: true},
                            arrival_picket: {required: true},
                            departure_picket: {required: true},
                            surveillance_present_service: {required: true},
                            surveillance_present_employee: {required: true},
                            surveillance_present_number: {required: true},
                            access_door_type: {required: true},

                            situation: {required: true},

                            securitybox_number: {required: true},
                            securitybox_location: {required: true},
                            sim_number: {required: true},
                            sim_location: {required: true},
                            recorder_location: {required: true},
                            cilinder_location: {required: true},

                            rco_name: {required: true},
                            rco_phone: {required: true},
                            owner_company: {required: true},

                            last_servicing: {required: true},
                            last_balance: {required: true},
                            transporter_code: {required: true},
                            countcenter_code: {required: true},

                            status_id: {required: {{ isset($claim) ? 'true' : 'false' }}}
                        },
                        messages: {
                            "account_communication[]": {required: "You must select at least one communication option"},
                            accept: {required: "You must accept the Terms and Conditions agreement!"}
                        },
                        invalidHandler: function (e, r) {
                            swal({
                                title: "",
                                text: "There are some missing values in your submission. Please correct them.",
                                type: "error",
                                confirmButtonClass: "btn btn-secondary m-btn m-btn--wide"
                            });
                        },
                        submitHandler: function (e) {
                            console.log()
                        }
                    }), (n = form.find('[data-wizard-action="submit"]')).on("click", function (r) {
                        r.preventDefault(), e.form() && (mApp.progress(n), form.ajaxSubmit({
                            success: function (data) {
                                console.log(data);
                                mApp.unprogress(n), swal({
                                    title: "",
                                    text: "The application has been successfully submitted!",
                                    type: "success",
                                    confirmButtonClass: "btn btn-secondary m-btn m-btn--wide"
                                })
                            }
                        }))
                    })
                }
            }
        }();
    </script>
@endpush