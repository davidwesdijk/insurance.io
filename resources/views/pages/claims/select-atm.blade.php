@extends ('template.default-2019')

@section ('title', __('Select an ATM'))

@section ('content')
    <div class="m-portlet m-portlet--tabs">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon">
                        <i class="la la-exclamation-triangle"></i>
                    </span>
                    <h3 class="m-portlet__head-text m--font-focus">{{ __('Select an ATM to continue') }}</h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                <ul class="nav nav-tabs m-tabs-line m-tabs-line--focus" role="tablist">
                    <li class="nav-item m-tabs__item">
                        <a class="nav-link m-tabs__link active" data-toggle="tab" href="#overview" role="tab">
                            <i class="la la-list"></i> {{ __('Overview') }}
                        </a>
                    </li>
                    <li class="nav-item m-tabs__item">
                        <a class="nav-link m-tabs__link" data-toggle="tab" href="#map" role="tab" onclick="toggleMap();">
                            <i class="la la-map"></i> {{ __('Map') }}
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="m-portlet__body">
            <div class="tab-content">
                <div class="tab-pane active" id="overview" role="tabpanel">
                    <div class="m_datatable" id="ajax_data"></div>
                </div>
                <div class="tab-pane" id="map" role="tabpanel">
                    <div id="map-container" style="height: 60vh;"></div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push ('scripts')
    <script type="application/javascript">
        var Datatable = {
            init: function() {
                var t;
                t = $(".m_datatable").mDatatable({
                    data: {
                        type: "remote",
                        source: {
                            read: {
                                url: '{{ route('api.atms.index') }}',
                                map: function(t) {
                                    var e = t;
                                    return void 0 !== t.data && (e = t.data),
                                        e
                                }
                            }
                        },
                        pageSize: 10,
                        serverPaging: !0,
                        serverFiltering: !0,
                        serverSorting: !0
                    },
                    layout: {
                        scroll: !1,
                        footer: !1
                    },
                    sortable: !0,
                    pagination: !0,
                    toolbar: {
                        items: {
                            pagination: {
                                pageSizeSelect: [10, 20, 30, 50, 100]
                            }
                        }
                    },
                    search: {
                        input: $("#generalSearch")
                    },
                    columns: [{
                        field: "identifier",
                        title: "#",
                        sortable: !1,
                        width: 100,
                        selector: !1,
                    }, {
                        field: "owner",
                        title: "{{ __('Owner') }}",
                        filterable: true,
                        width: 100,
                    }, {
                        field: "address",
                        title: "{{ __('Location') }}",
                        attr: {
                            nowrap: "nowrap"
                        },
                        template: function(t) {
                            return t.address + ", " + t.city
                        }
                    }, {
                        field: "active",
                        title: "{{ __('Status') }}",
                        width: 100,
                        template: function(t) {
                            if (t.active == true) {
                                return '<span class="m-badge m-badge--success m-badge--wide">{{ __('Active') }}</span>';
                            }

                            return '<span class="m-badge m-badge--metal m-badge--wide">{{ __('Inactive') }}</span>';
                        }
                    }, {
                        field: "url",
                        width: 100,
                        attr: {
                            nowrap: "nowrap"
                        },
                        title: "{{ __('Actions') }}",
                        sortable: !1,
                        overflow: "visible",
                        template: function(t, e, a) {
                            var url = ('{{ route('claims.new.atm', 'ATM') }}').replace(/ATM/g, t.id);
                            return '<a href="' + url + '" class="m-portlet__nav-link btn btn-focus m-btn m-btn--icon m-btn--sm" title="{{ __('Create claim') }}">{{ __('Create claim') }} <i class="la la-exclamation-triangle"></i></a>';
                        }
                    }]
                }),
                    $("#m_form_status").on("change", function() {
                        t.search($(this).val(), "Status")
                    }),
                    $("#m_form_type").on("change", function() {
                        t.search($(this).val(), "Type")
                    }),
                    $("#m_form_status, #m_form_type").selectpicker()
            }
        };
        jQuery(document).ready(function() {
            Datatable.init();
        });
    </script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key={{ config('geocoder.key') }}&callback=initMap"></script>
    <script>
        var map, origin, bounds, infowindow;
        var markers = [];

        function initMap() {
            origin = new google.maps.LatLng(51.9225, 4.47917);
            map = new google.maps.Map(document.getElementById('map-container'), {
                zoom: 4,
                center: origin
            });

            infowindow = new google.maps.InfoWindow();
            bounds = new google.maps.LatLngBounds();

            $.ajax({
                url: '{{ route('api.atms.locations') }}',
                method: 'POST',
                data: {},
                success: function (response) {
                    for (var i = 0; i < response.data.length; i++) {
                        var position = new google.maps.LatLng(parseFloat(response.data[i].lat), parseFloat(response.data[i].lng));
                        var marker = new google.maps.Marker({
                            position: position,
                            map: map,
                        });
                        google.maps.event.addListener(marker, 'click', (function (marker, i) {
                            return function () {
                                infowindow.setContent(response.data[i].popup);
                                infowindow.setOptions({maxWidth: 400});
                                infowindow.open(map, marker);
                            }
                        }) (marker, i));

                        markers.push(marker);
                        bounds.extend(marker.position);
                    }
                }
            }).done(function () {
                new MarkerClusterer(map, markers, {imagePath: '/assets/vendors/custom/markerclusterer/images/m'});
            });
        }

        function toggleMap() {
            setTimeout(
                function() {
                    map.fitBounds(bounds);
                },
                500);
        }
    </script>
@endpush