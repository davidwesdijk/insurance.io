@extends ('template.default-2019')

@section ('title', __('Claim') . ' ' . $claim->identifier)

@section ('content')
    <div class="row">
        <div class="col-xl-8">
            <div class="m-portlet m-portlet--responsive-mobile" id="main_portlet">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-wrapper">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon">
                                    <i class="la la-file-text"></i>
                                </span>
                                <h3 class="m-portlet__head-text m--font-focus">{{ __('Claim information') }}</h3>
                            </div>
                        </div>
                        <div class="m-portlet__head-tools">
                            <ul class="m-portlet__nav">
                                @can ('update claims')
                                    <li class="m-portlet__nav-item">
                                        <button role="button" data-toggle="modal" data-target="#previewPopup" class="btn btn-outline-focus m-btn m-btn--icon m-btn--wide">
                                            <span>
                                                <i class="la la-download"></i>
                                                <span>{{ __('Preview report') }}</span>
                                            </span>
                                        </button>
                                    </li>
                                @endcan
                                <li class="m-portlet__nav-item"></li>
                                @can ('update claims')
                                    <li class="m-portlet__nav-item">
                                        <a href="{{ route('claims.edit', $claim) }}" class="btn btn-secondary m-btn m-btn--icon m-btn--wide">
                                            <span>
                                                <i class="la la-pencil-square"></i>
                                                <span>{{ __('Edit') }}</span>
                                            </span>
                                        </a>
                                    </li>
                                @endcan
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="m-portlet__body">
                    <div class="row">
                        <div class="col-6">
                            <div class="row">
                                <h5 class="col">{{ __('Claim information') }}</h5>
                            </div>

                            <div class="row">
                                <label class="col-4">{{ __('Identifier') }}</label>
                                <div class="col-8">{{ $claim->identifier }}</div>
                            </div>
                            <div class="row">
                                <label class="col-4">{{ __('Incident type') }}</label>
                                <div class="col-8">{{ ucfirst(__($claim->incident_type)) }}</div>
                            </div>
                            <div class="row">
                                <label class="col-4">{{ __('Occurence') }}</label>
                                <div class="col-8">{{ $claim->incident_datetime->toFormattedDateString() }} {{ $claim->incident_datetime->format('H:i') }}</div>
                            </div>
                            <div class="row">
                                <label class="col-4">{{ __('Loot') }}</label>
                                <div class="col-8">
                                    @if ($claim->loot)
                                        € {{ number_format($claim->loot, 0) }}
                                    @else
                                        <abbr title="{{ __('Warning: this is an estimate') }}">€ {{ \App\Helpers\ClaimHelper::getLootEstimate($claim) }}</abbr>
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <label class="col-4">{{ __('ATM identifier') }}</label>
                                <div class="col-8">{{ $claim->atm->identifier }}</div>
                            </div>
                        </div>

                        <div class="col-6">
                            <div class="row">
                                <h5 class="col">{{ __('Claim status') }}</h5>
                            </div>
                            <div class="row">
                                <label class="col-4">{{ __('Current status') }}</label>
                                <div class="col-8">
                                    <span class="m-badge m-badge--metal m-badge--wide">{{ __($claim->status->description) }}</span>
                                </div>
                            </div>
                            <div class="row">
                                <label class="col-4">{{ __('Last update') }}</label>
                                <div class="col-8">{{ $claim->updated_at->toFormattedDateString() }} {{ $claim->updated_at->format('H:i') }}</div>
                            </div>

                            <div class="row mt-5">
                                <h5 class="col">{{ __('Location') }}</h5>
                            </div>

                            <div class="row">
                                <label class="col-4">{{ __('Address') }}</label>
                                <div class="col-8">
                                    {{ $atm->address }}<br>
                                    {{ $atm->zipcode }} {{ $atm->city }}, {{ $atm->country }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="m-portlet m-portlet--responsive-mobile" id="main_portlet">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-wrapper">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon">
                                    <i class="la la-tachometer"></i>
                                </span>
                                <h3 class="m-portlet__head-text m--font-focus">{{ __('Progress') }}</h3>
                            </div>
                        </div>
                        <div class="m-portlet__head-tools">
                            <ul class="m-portlet__nav">
                                <li class="m-portlet__nav-item">
                                    <span class="m-badge m-badge--focus m-badge--wide">{{ __('Report in progress...') }}</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="m-portlet__body">
                    <div class="row">
                        <div class="col-12">
                            <div class="progress m-progress--lg">
                                <div class="progress-bar progress-bar-striped progress-bar-animated m--bg-focus"
                                     data-toggle="m-tooltip" data-placement="top" title="Report in progress" role="progressbar"
                                     style="width: 10%;" aria-valuenow="10" aria-valuemin="0" aria-valuemax="10"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="col-xl-4">
            <div class="m-portlet m-portlet--responsive-mobile" id="location_portlet" m-portlet="true">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-wrapper">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon">
                                    <i class="la la-map-marker"></i>
                                </span>
                                <h3 class="m-portlet__head-text m--font-focus">{{ __('Location') }}</h3>
                            </div>
                        </div>
                        <div class="m-portlet__head-tools">
                            <ul class="m-portlet__nav">
                                <li class="m-portlet__nav-item">
                                    <a href="javascript:reloadMap()" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-refresh"></i></a>
                                </li>
                                <li class="m-portlet__nav-item">
                                    <a href="#" m-portlet-tool="fullscreen" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-expand"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="m-portlet__body m-portlet__body--no-padding">
                    <div id="map" style="height: 363px"></div>
                </div>
            </div>

            <div class="m-portlet">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <span class="m-portlet__head-icon">
                                <i class="la la-history"></i>
                            </span>
                            <h3 class="m-portlet__head-text m--font-focus">{{ __('Recent history') }}</h3>
                        </div>
                    </div>
                </div>
                <div class="m-portlet__body">
                    <div class="m-scrollable" data-scrollable="true" data-height="150">
                        <div class="m-list-timeline m-list-timeline--skin-light">
                            <div class="m-list-timeline__items">
                                @foreach ($claim->history() as $history)
                                    <div class="m-list-timeline__item">
                                        <span class="m-list-timeline__badge m-list-timeline__badge--info"></span>
                                        <span class="m-list-timeline__text">
                                            @if ($history->event == 'created')
                                                {{ __('Initial report created') }}
                                            @elseif ($history->event == 'updated')
                                                {{ __('Changed to status') }} {{ __(strtolower(array_first($history->getTags()))) }}
                                            @else
                                                {{ __(ucfirst($history->event)) }}
                                            @endif
                                        </span>
                                        <span class="m-list-timeline__time">{{ $history->created_at->toFormattedDateString() }}</span>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push ('scripts')
    <script>
        var wrapper = $('.detail_wrapper');

        $(document).on('change', '[name="type"]', function () {
            var element = $(this);
            var container = $(this).closest('.detail_container');

            container.find('.detail_type').text(element.val());
            container.find('.detail_body_content').show();
        });

        $(document).on('click', '#add-subject', function () {
            var index = wrapper.find('.detail_container').length;
            var content = $('#detail').html();
            collapseAllContainers();

            wrapper.append(content.replace(/{index}/g, index));
        });

        $(document).on('click', '.toggle', function () {
            var container = $(this).closest('.detail_container');
            var content = container.find('.detail_content');

            toggleContainer(container, !content.is(':visible'));
        });

        $(document).on('click', '.detail_close', function () {
            toggleContainer($(this).closest('.detail_container'), false);
        });

        function collapseAllContainers() {
            wrapper.find('.detail_container').each(function (key, container) {
                toggleContainer($(container), false);
            });
        }

        function toggleContainer(container, show) {
            if (show === true) {
                container.find('.toggle i').removeClass('la-angle-up').addClass('la-angle-down');
                container.find('.detail_content').slideDown();
            } else {
                container.find('.toggle i').removeClass('la-angle-down').addClass('la-angle-up');
                container.find('.detail_content').slideUp();
            }
        }
    </script>
    <script type="text/html" id="detail">
        @include ('pages.claims.partials.details', ['index' => 0, 'detail' => null])
    </script>

    <script async defer src="https://maps.googleapis.com/maps/api/js?key={{ config('geocoder.key') }}&callback=initMap"></script>
    <script>
        var map, marker;
        var position = {
            lat: parseFloat('{{ $atm->lat }}'),
            lng: parseFloat('{{ $atm->lng }}'),
        };

        function initMap() {
            map = new google.maps.Map(document.getElementById('map'), {
                zoom: 13,
                center: position,
                disableDefaultUI: true
            });

            marker = new google.maps.Marker({
                position: position,
                map: map,
            });
        }

        function reloadMap() {
            map.setCenter(position);
        }
    </script>
@endpush

@push ('styles')
    <style>
        .m-portlet--fullscreen #map {
            height: 100%!important;
        }
        i.action-icon {
            font-size: 5rem;
        }
        .detail_type {
            text-transform: capitalize;
        }
    </style>
@endpush

@push ('modals')
    <div class="modal fade" id="previewPopup" tabindex="-1" role="dialog" aria-labelledby="previewPopupLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="previewPopupLabel">{{ __('Report preview') }}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <iframe src="{{ route('claims.preview', $claim) }}" frameborder="0" class="w-100" style="min-height: 70vh;"></iframe>
                </div>
            </div>
        </div>
    </div>
@endpush