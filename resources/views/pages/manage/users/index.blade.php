@extends ('template.default-2019')

@section ('title', __('Manage users'))

@section ('content')
    <div class="m-portlet">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text m--font-focus">{{ __('Manage users') }}</h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                <ul class="m-portlet__nav">
                    @can ('manage users')
                        <li class="m-portlet__nav-item">
                            <a href="{{ route('manage.users.create') }}" class="btn btn-focus m-btn m-btn--icon m-btn--wide">
                                <span>
                                    <i class="la la-plus"></i>
                                    <span>{{ __('Create user') }}</span>
                                </span>
                            </a>
                        </li>
                    @endcan
                </ul>
            </div>
        </div>
        <div class="m-portlet__body">
                <div class="m_datatable" id="ajax_data"></div>
            </div>
        </div>
    </div>
@endsection

@push ('scripts')
    <script type="application/javascript">
        var Datatable = {
            init: function() {
                var t;
                t = $(".m_datatable").mDatatable({
                    data: {
                        type: "remote",
                        source: {
                            read: {
                                url: '{{ route('api.manage.users.index') }}',
                                map: function(t) {
                                    var e = t;
                                    return void 0 !== t.data && (e = t.data),
                                        e
                                }
                            }
                        },
                        pageSize: 10,
                        serverPaging: !0,
                        serverFiltering: !0,
                        serverSorting: !0
                    },
                    layout: {
                        scroll: !1,
                        footer: !1
                    },
                    sortable: !0,
                    pagination: !0,
                    toolbar: {
                        items: {
                            pagination: {
                                pageSizeSelect: [10, 20, 30, 50, 100]
                            }
                        }
                    },
                    search: {
                        input: $("#generalSearch")
                    },
                    columns: [{
                        field: "full_name",
                        title: "{{ __('Name') }}",
                        filterable: true,
                    }, {
                        field: "email",
                        title: "{{ __('Email') }}",
                        filterable: true,
                        width: 200,
                    }, {
                        field: "role",
                        title: "{{ __('Role') }}",
                        filterable: true,
                        width: 100,
                    }, {
                        field: "active",
                        title: "{{ __('Status') }}",
                        width: 100,
                        template: function(t) {
                            if (t.active == true) {
                                return '<span class="m-badge m-badge--success m-badge--wide">{{ __('Active') }}</span>';
                            }

                            return '<span class="m-badge m-badge--metal m-badge--wide">{{ __('Inactive') }}</span>';
                        }
                    }, {
                        field: "url",
                        width: 100,
                        attr: {
                            nowrap: "nowrap"
                        },
                        title: "{{ __('Actions') }}",
                        sortable: !1,
                        textAlign: 'right',
                        template: function(t, e, a) {
                            return '<a role="button" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="{{ __('Reset password') }}" data-toggle="modal" data-target="#passwordReset" data-user="' + t.id + '" data-name="' + t.full_name + '"><i class="la la-lock"></i></a>' +
                                '<a href="' + t.url + '" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="{{ __('Show ATM') }}"><i class="la la-search"></i></a>';
                        }
                    }]
                }),
                    $("#m_form_status").on("change", function() {
                        t.search($(this).val(), "Status")
                    }),
                    $("#m_form_type").on("change", function() {
                        t.search($(this).val(), "Type")
                    }),
                    $("#m_form_status, #m_form_type").selectpicker()
            }
        };
        jQuery(document).ready(function() {
            Datatable.init();
        });

        $('#passwordReset').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget);
            var user = {
                id: button.data('user'),
                name: button.data('name'),
            };

            var modal = $(this);
            modal.find('.user_name').text(user.name);
            modal.find('.modal-body input[name="id"]').val(user.id);
        })
    </script>
@endpush

@push ('modals')
    <div class="modal fade" id="passwordReset" tabindex="-1" role="dialog" aria-labelledby="passwordResetLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form action="{{ route('manage.users.set-password') }}" method="post">
                    @csrf
                    <input type="hidden" name="id" value="">

                    <div class="modal-header">
                        <h5 class="modal-title" id="passwordResetLabel">{{ __('Reset password') }} {{ __('for') }} <span class="user_name"></span></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group m-form__group m-form__group--md row">
                            <div class="col-lg-6">
                                <label for="password" class="col-form-label">{{ __('New password') }}:</label>
                                <input type="password" class="form-control" name="password">
                            </div>
                            <div class="col-lg-6">
                                <label for="password_confirmation" class="col-form-label">{{ __('Confirm password') }}:</label>
                                <input type="password" class="form-control" name="password_confirmation">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-focus m-btn m-btn--icon m-btn--wide">
                            <i class="la la-check"></i>
                            {{ __('Save') }}
                        </button>
                        <span class="m--margin-left-10">{{ __('or') }} <a href="javascript:;" data-dismiss="modal" class="m-link m--font-bold">{{ __('cancel') }}</a></span>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endpush