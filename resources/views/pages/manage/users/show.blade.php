@extends ('template.default-2019')

@section ('title', __('Create user'))

@section ('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="m-portlet m-portlet--last m-portlet--head-lg m-portlet--responsive-mobile" id="main_portlet">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-wrapper">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                @isset ($user)
                                    <span class="m-portlet__head-icon">
                                        <i class="la la-pencil-square"></i>
                                    </span>
                                    <h3 class="m-portlet__head-text m--font-focus">
                                        {{ __('User') }}: <small class="text-muted">{{ $user->full_name}}</small>
                                    </h3>
                                @else
                                    <span class="m-portlet__head-icon">
                                        <i class="la la-plus"></i>
                                    </span>
                                    <h3 class="m-portlet__head-text m--font-focus">{{ __('Create user') }}</h3>
                                @endisset
                            </div>
                        </div>
                        <div class="m-portlet__head-tools">
                            <a href="{{ url()->previous() }}" class="btn btn-secondary m-btn m-btn--icon m-btn--wide m-btn--md">
                                <span>
                                    <i class="la la-arrow-left"></i>
                                    <span>{{ __('Back') }}</span>
                                </span>
                            </a>
                        </div>
                    </div>
                </div>
                <form class="m-form m-form--label-align-left- m-form--state-" action="{{ isset($user) ? route('manage.users.update', $user) : route('manage.users.store') }}" method="post">
                    @csrf
                    @isset ($user)
                        @method ('put')
                    @endisset

                    <div class="m-portlet__body">
                        <div class="row">
                            <div class="col-xl-8 offset-xl-2">
                                <div class="m-form__section m-form__section--first">
                                    <div class="m-form__heading">
                                        <h3 class="m-form__heading-title">{{ __('User data') }}</h3>
                                    </div>
                                    <div class="form-group m-form__group m-form__group--md row {{ $errors->has('first_name') ? 'has-danger' : '' }}">
                                        <label class="col-lg-3 col-form-label required">{{ __('Name') }}:</label>
                                        <div class="col-lg-3">
                                            <input type="text" name="first_name" class="form-control m-input"
                                                   value="{{ old('first_name', $user->first_name ?? '') }}" placeholder="{{ __('First name') }}">
                                            @if ($errors->has('first_name'))
                                                <div class="form-control-feedback">{{ $errors->first('first_name') }}</div>
                                            @endif
                                        </div>
                                        <div class="col-lg-3">
                                            <input type="text" name="name" class="form-control m-input"
                                                   value="{{ old('name', $user->name ?? '') }}" placeholder="{{ __('Name') }}">
                                            @if ($errors->has('name'))
                                                <div class="form-control-feedback">{{ $errors->first('name') }}</div>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group m-form__group--md row {{ $errors->has('email') ? 'has-danger' : '' }}">
                                        <label class="col-lg-3 col-form-label required">{{ __('Email') }}:</label>
                                        <div class="col-lg-6">
                                            <input type="text" name="email" class="form-control m-input"
                                                   value="{{ old('email', $user->email ?? '') }}" placeholder="{{ __('Email') }}">
                                            @if ($errors->has('email'))
                                                <div class="form-control-feedback">{{ $errors->first('email') }}</div>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group m-form__group--md row {{ $errors->has('phone') ? 'has-danger' : '' }}">
                                        <label class="col-lg-3 col-form-label required">{{ __('Phone') }}:</label>
                                        <div class="col-lg-6">
                                            <input type="text" name="phone" class="form-control m-input"
                                                   value="{{ old('phone', $user->phone ?? '') }}" placeholder="{{ __('Phone') }}">
                                            @if ($errors->has('phone'))
                                                <div class="form-control-feedback">{{ $errors->first('phone') }}</div>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group m-form__group--md row {{ $errors->has('region') ? 'has-danger' : '' }}">
                                        <label class="col-lg-3 col-form-label">{{ __('Region') }}:</label>
                                        <div class="col-lg-6">
                                            <input type="text" name="region" class="form-control m-input"
                                                   value="{{ old('region', $user->region ?? '') }}" placeholder="{{ __('Region') }}">
                                            @if ($errors->has('region'))
                                                <div class="form-control-feedback">{{ $errors->first('region') }}</div>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group m-form__group--md row {{ $errors->has('active') ? 'has-danger' : '' }}">
                                        <label class="col-lg-3 col-form-label">{{ __('Active') }}:</label>
                                        <div class="col-lg-6">
                                            <input type="hidden" name="active" value="0">
                                            <span class="m-switch">
                                                <label>
                                                    <input type="checkbox" name="active" value="1"
                                                            {{ old('active', $user->active ?? 0) == true ? 'checked' : '' }}>
                                                    <span></span>
                                                </label>
                                            </span>
                                            @if ($errors->has('active'))
                                                <div class="form-control-feedback">{{ $errors->first('active') }}</div>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="m-separator m-separator--dashed m-separator--lg"></div>

                                <div class="m-form__section">
                                    <div class="m-form__heading">
                                        <h3 class="m-form__heading-title">{{ __('Authorisation') }}</h3>
                                    </div>
                                    <div class="col-lg-9">
                                        <div class="m-checkbox-list">
                                            @foreach (\Spatie\Permission\Models\Role::all() as $role)
                                                <label class="m-radio">
                                                    <input type="radio" name="role" value="{{ $role->name }}"
                                                            {{ optional($user ?? null)->hasRole($role->name) ? 'checked' : '' }}>
                                                    {{ ucfirst($role->name) }}
                                                    <span></span>
                                                </label>
                                            @endforeach
                                        </div>
                                        @if ($errors->has('role'))
                                            <div class="form-control-feedback">{{ $errors->first('role') }}</div>
                                        @endif
                                    </div>
                                </div>

                                @if (!isset($user))
                                    <div class="m-separator m-separator--dashed m-separator--lg"></div>

                                    <div class="m-form__section">
                                        <div class="m-form__heading">
                                            <h3 class="m-form__heading-title">{{ __('Password') }}</h3>
                                        </div>

                                        <div class="form-group m-form__group m-form__group--md row {{ $errors->has('password') ? 'has-danger' : '' }}">
                                            <label class="col-lg-3 col-form-label required">{{ __('Choose a password') }}:</label>
                                            <div class="col-lg-6">
                                                <input type="password" name="password" class="form-control m-input">
                                            </div>
                                        </div>

                                        <div class="form-group m-form__group m-form__group--md row {{ $errors->has('password_confirmation') ? 'has-danger' : '' }}">
                                            <label class="col-lg-3 col-form-label required">{{ __('Confirm the password') }}:</label>
                                            <div class="col-lg-6">
                                                <input type="password" name="password_confirmation" class="form-control m-input">
                                                @if ($errors->has('password_confirmation'))
                                                    <div class="form-control-feedback">{{ $errors->first('password_confirmation') }}</div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                @endif

                                @isset ($user)
                                    <div class="m-separator m-separator--dashed m-separator--lg"></div>

                                    <div class="m-alert m-alert--icon m-alert--icon-solid m-alert--outline alert alert-{{ $errors->has('status_id') ? 'danger' : 'info' }} fade show" role="alert">
                                        <div class="m-alert__icon">
                                            <i class="flaticon-exclamation-1"></i>
                                            <span></span>
                                        </div>
                                        <div class="m-alert__text">
                                            {{ __('While editing an existing user, please select the proper reason for editing this entity:') }}<br>
                                            <div class="row mt-3">
                                                <div class="col-lg-6">
                                                    <input type="hidden" name="action" value="update">
                                                    <select name="status_id" class="form-control m-input">
                                                        <option value="" disabled selected>{{ __('Select a status') }}</option>
                                                        @foreach (\App\Status::user()->get() as $status)
                                                            <option {{ old('status_id') == $status->id ? 'selected' : '' }} value="{{ $status->id }}">{{ __($status->description) }}</option>
                                                        @endforeach
                                                    </select>
                                                    @if ($errors->has('status_id'))
                                                        <div class="form-control-feedback mt-2">{{ $errors->first('status_id') }}</div>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endisset
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__foot m--align-right">
                        <button type="submit" class="btn btn-focus m-btn m-btn--icon m-btn--wide">
                            <i class="la la-check"></i>
                            {{ __('Save') }}
                        </button>
                        <span class="m--margin-left-10">{{ __('or') }} <a href="{{ url()->previous() }}" class="m-link m--font-bold">{{ __('cancel') }}</a></span>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection