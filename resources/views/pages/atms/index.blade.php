@extends ('template.default-2019')

@section ('title', __('ATMs'))

@section ('content')
    <div class="m-portlet m-portlet--tabs">
        <div class="m-portlet__head">
            <div class="m-portlet__head-tools">
                <ul class="nav nav-tabs m-tabs-line m-tabs-line--focus" role="tablist">
                    <li class="nav-item m-tabs__item">
                        <a class="nav-link m-tabs__link active" data-toggle="tab" href="#overview" role="tab">
                            <i class="la la-list"></i> {{ __('Overview') }}
                        </a>
                    </li>
                    <li class="nav-item m-tabs__item">
                        <a class="nav-link m-tabs__link" data-toggle="tab" href="#map" role="tab" onclick="toggleMap();">
                            <i class="la la-map"></i> {{ __('Map') }}
                        </a>
                    </li>
                </ul>
            </div>
            <div class="m-portlet__head-tools">
                <ul class="m-portlet__nav">
                    @can ('create atms')
                        <li class="m-portlet__nav-item">
                            <a href="{{ route('atms.create') }}" class="btn btn-focus m-btn m-btn--icon m-btn--wide">
                                <span>
                                    <i class="la la-plus"></i>
                                    <span>{{ __('Create ATM') }}</span>
                                </span>
                            </a>
                        </li>
                    @endcan
                    <li class="m-portlet__nav-item"></li>
                    <li class="m-portlet__nav-item">
                        <div class="m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" m-dropdown-toggle="hover" aria-expanded="true">
                            <a href="#" class="m-portlet__nav-link btn btn-lg btn-secondary  m-btn m-btn--icon m-btn--icon-only m-btn--pill  m-dropdown__toggle">
                                <i class="la la-ellipsis-h m--font-brand"></i>
                            </a>
                            <div class="m-dropdown__wrapper">
                                <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
                                <div class="m-dropdown__inner">
                                    <div class="m-dropdown__body">
                                        <div class="m-dropdown__content">
                                            <ul class="m-nav">
                                                <li class="m-nav__section m-nav__section--first">
                                                    <span class="m-nav__section-text">Quick Actions</span>
                                                </li>
                                                <li class="m-nav__item">
                                                    <a href="" class="m-nav__link">
                                                        <i class="m-nav__link-icon flaticon-share"></i>
                                                        <span class="m-nav__link-text">Create Post</span>
                                                    </a>
                                                </li>
                                                <li class="m-nav__item">
                                                    <a href="" class="m-nav__link">
                                                        <i class="m-nav__link-icon flaticon-chat-1"></i>
                                                        <span class="m-nav__link-text">Send Messages</span>
                                                    </a>
                                                </li>
                                                <li class="m-nav__item">
                                                    <a href="" class="m-nav__link">
                                                        <i class="m-nav__link-icon flaticon-multimedia-2"></i>
                                                        <span class="m-nav__link-text">Upload File</span>
                                                    </a>
                                                </li>
                                                <li class="m-nav__section">
                                                    <span class="m-nav__section-text">Useful Links</span>
                                                </li>
                                                <li class="m-nav__item">
                                                    <a href="" class="m-nav__link">
                                                        <i class="m-nav__link-icon flaticon-info"></i>
                                                        <span class="m-nav__link-text">FAQ</span>
                                                    </a>
                                                </li>
                                                <li class="m-nav__item">
                                                    <a href="" class="m-nav__link">
                                                        <i class="m-nav__link-icon flaticon-lifebuoy"></i>
                                                        <span class="m-nav__link-text">Support</span>
                                                    </a>
                                                </li>
                                                <li class="m-nav__separator m-nav__separator--fit m--hide">
                                                </li>
                                                <li class="m-nav__item m--hide">
                                                    <a href="#" class="btn btn-outline-danger m-btn m-btn--pill m-btn--wide btn-sm">Submit</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        <div class="m-portlet__body">
            <div class="tab-content">
                <div class="tab-pane active" id="overview" role="tabpanel">
                    <div class="m_datatable" id="ajax_data"></div>
                </div>
                <div class="tab-pane" id="map" role="tabpanel">
                    <div id="map-container" style="height: 60vh;"></div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push ('scripts')
    <script type="application/javascript">
        var Datatable = {
            init: function() {
                var t;
                t = $(".m_datatable").mDatatable({
                    data: {
                        type: "remote",
                        source: {
                            read: {
                                url: '{{ route('api.atms.index') }}',
                                map: function(t) {
                                    var e = t;
                                    return void 0 !== t.data && (e = t.data),
                                        e
                                }
                            }
                        },
                        pageSize: 10,
                        serverPaging: !0,
                        serverFiltering: !0,
                        serverSorting: !0
                    },
                    layout: {
                        scroll: !1,
                        footer: !1
                    },
                    sortable: !0,
                    pagination: !0,
                    toolbar: {
                        items: {
                            pagination: {
                                pageSizeSelect: [10, 20, 30, 50, 100]
                            }
                        }
                    },
                    search: {
                        input: $("#generalSearch")
                    },
                    columns: [{
                        field: "identifier",
                        title: "#",
                        sortable: !1,
                        width: 100,
                        selector: !1,
                    }, {
                        field: "owner",
                        title: "{{ __('Owner') }}",
                        filterable: true,
                        width: 100,
                    }, {
                        field: "address",
                        title: "{{ __('Location') }}",
                        attr: {
                            nowrap: "nowrap"
                        },
                        template: function(t) {
                            return t.address + ", " + t.city
                        }
                    }, {
                        field: "active",
                        title: "{{ __('Status') }}",
                        width: 100,
                        template: function(t) {
                            if (t.active == true) {
                                return '<span class="m-badge m-badge--success m-badge--wide">{{ __('Active') }}</span>';
                            }

                            return '<span class="m-badge m-badge--metal m-badge--wide">{{ __('Inactive') }}</span>';
                        }
                    }, {
                        field: "url",
                        width: 50,
                        attr: {
                            nowrap: "nowrap"
                        },
                        title: "{{ __('Actions') }}",
                        sortable: !1,
                        overflow: "visible",
                        textAlign: 'right',
                        template: function(t, e, a) {
                            return '<a href="' + t.url + '" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="{{ __('Show ATM') }}"><i class="la la-search"></i></a>';
                        }
                    }]
                }),
                    $("#m_form_status").on("change", function() {
                        t.search($(this).val(), "Status")
                    }),
                    $("#m_form_type").on("change", function() {
                        t.search($(this).val(), "Type")
                    }),
                    $("#m_form_status, #m_form_type").selectpicker()
            }
        };
        jQuery(document).ready(function() {
            Datatable.init();
        });
    </script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key={{ config('geocoder.key') }}&callback=initMap"></script>
    <script>
        var map, origin, bounds, infowindow;
        var markers = [];

        function initMap() {
            origin = new google.maps.LatLng(51.9225, 4.47917);
            map = new google.maps.Map(document.getElementById('map-container'), {
                zoom: 4,
                center: origin
            });

            infowindow = new google.maps.InfoWindow();
            bounds = new google.maps.LatLngBounds();

            $.ajax({
                url: '{{ route('api.atms.locations') }}',
                method: 'POST',
                data: {},
                success: function (response) {
                    for (var i = 0; i < response.data.length; i++) {
                        var position = new google.maps.LatLng(parseFloat(response.data[i].lat), parseFloat(response.data[i].lng));
                        var marker = new google.maps.Marker({
                            position: position,
                            map: map,
                        });
                        google.maps.event.addListener(marker, 'click', (function (marker, i) {
                            return function () {
                                infowindow.setContent(response.data[i].popup);
                                infowindow.setOptions({maxWidth: 400});
                                infowindow.open(map, marker);
                            }
                        }) (marker, i));

                        markers.push(marker);
                        bounds.extend(marker.position);
                    }
                }
            }).done(function () {
                new MarkerClusterer(map, markers, {imagePath: '/assets/vendors/custom/markerclusterer/images/m'});
            });
        }

        function toggleMap() {
            setTimeout(
                function() {
                    map.fitBounds(bounds);
                },
                500);
        }
    </script>
@endpush