@extends ('template.default-2019')

@section ('title', __('ATM details'))

@section ('content')
    <div class="row">
        <div class="col-xl-8">
            <div class="m-portlet m-portlet--responsive-mobile" id="main_portlet">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-wrapper">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon">
                                    <i class="la la-file-text"></i>
                                </span>
                                <h3 class="m-portlet__head-text m--font-focus">{{ __('ATM details') }}</h3>
                            </div>
                        </div>
                        <div class="m-portlet__head-tools">
                            @can ('update atms')
                                <a href="{{ route('atms.edit', $atm) }}" class="btn btn-secondary m-btn m-btn--icon m-btn--wide">
                                    <span>
                                        <i class="la la-pencil-square"></i>
                                        <span>{{ __('Edit') }}</span>
                                    </span>
                                </a>
                            @endcan
                        </div>
                    </div>
                </div>
                <div class="m-portlet__body">
                    <div class="row">
                        <div class="col-6">
                            <div class="row">
                                <h5 class="col">{{ __('Location') }}</h5>
                            </div>

                            <div class="row">
                                <label class="col-4">{{ __('Address') }}</label>
                                <div class="col-8">
                                    {{ $atm->address }}<br>
                                    {{ $atm->zipcode }} {{ $atm->city }}, {{ $atm->country }}
                                </div>
                            </div>

                            <div class="row mt-5">
                                <h5 class="col">{{ __('Object information') }}</h5>
                            </div>

                            <div class="row">
                                <label class="col-4">{{ __('Identifier') }}</label>
                                <div class="col-8">{!! $atm->identifier ?? '<span class="text-muted">'. __('Unknown') .'</span>' !!}</div>
                            </div>
                            <div class="row">
                                <label class="col-4">{{ __('Serial number') }}</label>
                                <div class="col-8">{!! $atm->serial_number ?? '<span class="text-muted">'. __('Unknown') .'</span>' !!}</div>
                            </div>
                            <div class="row">
                                <label class="col-4">{{ __('Manufacturer') }}</label>
                                <div class="col-8">{!! $atm->manufacturer ?? '<span class="text-muted">'. __('Unknown') .'</span>' !!}</div>
                            </div>
                            <div class="row">
                                <label class="col-4">{{ __('Year') }}</label>
                                <div class="col-8">{!! $atm->year ?? '<span class="text-muted">'. __('Unknown') .'</span>' !!}</div>
                            </div>
                            <div class="row">
                                <label class="col-4">{{ __('Owner') }}</label>
                                <div class="col-8">{!! $atm->owner ?? '<span class="text-muted">'. __('Unknown') .'</span>' !!}</div>
                            </div>
                            <div class="row">
                                <label class="col-4">{{ __('Location') }}</label>
                                <div class="col-8">{!! $atm->location ?? '<span class="text-muted">'. __('Unknown') .'</span>' !!}</div>
                            </div>
                            <div class="row">
                                <label class="col-4">{{ __('Capacity') }}</label>
                                <div class="col-8">&euro; {!! $atm->capacity ? number_format($atm->capacity, 2) : '<span class="text-muted">'. __('Unknown') .'</span>' !!}</div>
                            </div>
                        </div>

                        <div class="col-6">
                            <div class="row">
                                <h5 class="col">{{ __('Status') }}</h5>
                            </div>
                            <div class="row">
                                <label class="col-4">{{ __('Last status') }}</label>
                                <div class="col-8">
                                    <span class="m-badge m-badge--metal m-badge--wide">{{ __($atm->status->description) }}</span>
                                </div>
                            </div>
                            <div class="row">
                                <label class="col-4">{{ __('Last update') }}</label>
                                <div class="col-8">{{ $atm->updated_at->toFormattedDateString() }} {{ $atm->updated_at->format('H:i') }}</div>
                            </div>

                            <div class="row mt-5">
                                <h5 class="col">{{ __('Usage') }}</h5>
                            </div>
                            <div class="row">
                                <label class="col-4">{{ __('Commissioned') }}</label>
                                <div class="col-8">
                                    @if ($atm->commissioned)
                                        <span class="m-badge m-badge--success m-badge--wide">{{ __('Yes') }}, {{ __('per') }} {{ $atm->commissioned->toFormattedDateString() }}</span>
                                    @else
                                        <span class="m-badge m-badge--metal m-badge--wide">{{ __('No') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <label class="col-4">{{ __('Decommissioned') }}</label>
                                <div class="col-8">
                                    @if ($atm->decommissioned)
                                        <span class="m-badge m-badge--success m-badge--wide">{{ __('Yes') }}, {{ __('per') }} {{ $atm->decommissioned->toFormattedDateString() }}</span>
                                    @else
                                        <span class="m-badge m-badge--metal m-badge--wide">{{ __('No') }}</span>
                                    @endif
                                </div>
                            </div>

                            <div class="row mt-5">
                                <h5 class="col">{{ __('Functionalities') }}</h5>
                            </div>
                            @forelse ($atm->functionalities as $functionality)
                                <div class="row">
                                    <div class="col-1">
                                        <i class="la la-check m--font-focus"></i>
                                    </div>
                                    <div class="col-11">{{ __($functionality->description) }}</div>
                                </div>
                            @empty
                                <div class="row">
                                    <div class="col-1">
                                        <i class="la la-times"></i>
                                    </div>
                                    <div class="col-11">{{ __('Unknown') }}</div>
                                </div>
                            @endforelse
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="col-xl-4">
            <div class="m-portlet m-portlet--responsive-mobile" id="location_portlet" m-portlet="true">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-wrapper">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon">
                                    <i class="la la-map-marker"></i>
                                </span>
                                <h3 class="m-portlet__head-text m--font-focus">{{ __('Location') }}</h3>
                            </div>
                        </div>
                        <div class="m-portlet__head-tools">
                            @can ('update atms')
                                <form action="{{ route('atms.setLocation', $atm) }}" method="post" class="save_action" style="display: none">
                                    @method('put')
                                    @csrf
                                    <input type="hidden" name="lat" value="{{ $atm->lat }}">
                                    <input type="hidden" name="lng" value="{{ $atm->lng }}">
                                    <input type="hidden" name="heading" value="{{ $atm->heading }}">
                                    <input type="hidden" name="pitch" value="{{ $atm->pitch }}">
                                    <button type="submit" class="btn btn-secondary m-btn m-btn--icon m-btn--wide">
                                        <span>
                                            <i class="la la-save"></i>
                                            <span>{{ __('Save') }}</span>
                                        </span>
                                    </button>
                                </form>
                            @endcan
                        </div>
                    </div>
                </div>
                <div class="m-portlet__body m-portlet__body--no-padding">
                    <div id="panorama" style="height: 363px"></div>
                    {{--<div id="map" style="height: 363px"></div>--}}
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xl-8">
            <div class="m-portlet m-portlet--responsive-mobile" id="main_portlet">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-wrapper">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon">
                                    <i class="la la-file"></i>
                                </span>
                                <h3 class="m-portlet__head-text m--font-focus">{{ __('Actions') }}</h3>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="m-portlet__body">
                    <div class="row m-row--col-separator-xl">
                        <div class="col-lg-4 text-center">
                            <i class="la la-warning m--font-info action-icon"></i>
                            <h4 class="mt-3 mb-3 m--font-info">{{ __('Claims') }}</h4>
                            <a href="{{ route('claims.new.atm', $atm) }}" class="btn btn-secondary m-btn m-btn--icon m-btn--wide">
                                <span>
                                    <span>{{ __('Report claim') }}</span>
                                    <i class="la la-arrow-right"></i>
                                </span>
                            </a>
                        </div>
                        <div class="col-lg-4 text-center">
                            <i class="la la-dashboard m--font-primary action-icon"></i>
                            <h4 class="mt-3 mb-3 m--font-primary">{{ __('Report') }}</h4>
                            <a href="#" class="btn btn-secondary m-btn m-btn--icon m-btn--wide">
                                <span>
                                    <span>{{ __('View ATM report') }}</span>
                                    <i class="la la-arrow-right"></i>
                                </span>
                            </a>
                        </div>
                        <div class="col-lg-4 text-center">
                            <i class="la la-history m--font-accent action-icon"></i>
                            <h4 class="mt-3 mb-3 m--font-accent">{{ __('History') }}</h4>
                            <a href="#" class="btn btn-secondary m-btn m-btn--icon m-btn--wide">
                                <span>
                                    <span>{{ __('View history') }}</span>
                                    <i class="la la-arrow-right"></i>
                                </span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-4">
            <div class="m-portlet m-portlet--full-height ">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <span class="m-portlet__head-icon">
                                <i class="la la-history"></i>
                            </span>
                            <h3 class="m-portlet__head-text m--font-focus">{{ __('Recent history') }}</h3>
                        </div>
                    </div>
                </div>
                <div class="m-portlet__body">
                    <div class="m-scrollable" data-scrollable="true" data-height="150">
                        <div class="m-list-timeline m-list-timeline--skin-light">
                            <div class="m-list-timeline__items">
                                @foreach ($atm->history() as $history)
                                    <div class="m-list-timeline__item">
                                        @if ($history instanceof \App\Models\Claim)
                                            <span class="m-list-timeline__badge m-list-timeline__badge--danger"></span>
                                            <span class="m-list-timeline__text">
                                                <a href="{{ route('claims.show', $history) }}">{{ ucfirst(__($history->incident_type)) }}</a>
                                            </span>
                                            <span class="m-list-timeline__time">{{ $history->incident_datetime->toFormattedDateString() }}</span>
                                        @else
                                            <span class="m-list-timeline__badge m-list-timeline__badge--info"></span>
                                            <span class="m-list-timeline__text">
                                                @if ($history->event == 'created')
                                                    {{ __('ATM registered') }}
                                                @elseif ($history->event == 'updated')
                                                    {{ __('Changed to status') }} {{ __(strtolower(array_first($history->getTags()))) }}
                                                @else
                                                    {{ __(ucfirst($history->event)) }}
                                                @endif
                                            </span>
                                            <span class="m-list-timeline__time">{{ $history->created_at->toFormattedDateString() }}</span>
                                        @endif
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push ('scripts')
    <script async defer src="https://maps.googleapis.com/maps/api/js?key={{ config('geocoder.key') }}&callback=initMap"></script>
    <script>
        var panorama;
        var posChanged, pitchChanged = false;

        function initMap() {
            panorama = new google.maps.StreetViewPanorama(document.getElementById('panorama'), {
                position: {
                    lat: {{ $atm->lat }},
                    lng: {{ $atm->lng }},
                },
                pov: {
                    heading: parseFloat('{{ $atm->heading }}'),
                    pitch: parseFloat('{{ $atm->pitch }}')
                },
                zoom: 1
            });

            panorama.addListener('position_changed', function() {
                if (posChanged) {
                    showPositionSaveAction();
                } else {
                    posChanged = true;
                }

                console.log(panorama.getPosition().lat(), panorama.getPosition().lng(), {{ $atm->lat }}, {{ $atm->lng }});

                $('[name="lat"]').val(panorama.getPosition().lat());
                $('[name="lng"]').val(panorama.getPosition().lng());
            });

            panorama.addListener('pov_changed', function() {
                if (pitchChanged) {
                    showPositionSaveAction();
                } else {
                    pitchChanged = true;
                }

                $('[name="heading"]').val(panorama.getPov().heading);
                $('[name="pitch"]').val(panorama.getPov().pitch);
            });
        }

        function showPositionSaveAction() {
            $('.save_action').show();
        }
    </script>
@endpush

@push ('styles')
    <style>
        ,.m-portlet--fullscreen #map, .m-portlet--fullscreen #panorama {
            height: 100%!important;
        }
        i.action-icon {
            font-size: 5rem;
        }
    </style>
@endpush