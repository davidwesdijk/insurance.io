@extends ('template.default-2019')

@section ('title', __('Create ATM'))

@section ('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="m-portlet m-portlet--last m-portlet--head-lg m-portlet--responsive-mobile" id="main_portlet">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-wrapper">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                @isset ($atm)
                                    <span class="m-portlet__head-icon">
                                        <i class="la la-pencil-square"></i>
                                    </span>
                                    <h3 class="m-portlet__head-text m--font-focus">
                                        {{ __('Edit ATM') }}: <small class="text-muted">{{ $atm->address }}, {{ $atm->city }}</small>
                                    </h3>
                                @else
                                    <span class="m-portlet__head-icon">
                                        <i class="la la-plus"></i>
                                    </span>
                                    <h3 class="m-portlet__head-text m--font-focus">{{ __('Create ATM') }}</h3>
                                @endisset
                            </div>
                        </div>
                        <div class="m-portlet__head-tools">
                            <a href="{{ url()->previous() }}" class="btn btn-secondary m-btn m-btn--icon m-btn--wide m-btn--md">
                                <span>
                                    <i class="la la-arrow-left"></i>
                                    <span>{{ __('Back') }}</span>
                                </span>
                            </a>
                        </div>
                    </div>
                </div>
                <form class="m-form m-form--label-align-left- m-form--state-" action="{{ isset($atm) ? route('atms.update', $atm) : route('atms.store') }}" method="post">
                    @csrf
                    @isset ($atm)
                        @method ('put')
                    @endisset

                    <div class="m-portlet__body">
                        <div class="row">
                            <div class="col-xl-8 offset-xl-2">
                                <div class="m-form__section m-form__section--first">
                                    <div class="m-form__heading">
                                        <h3 class="m-form__heading-title">{{ __('Location') }}</h3>
                                    </div>
                                    <div class="form-group m-form__group m-form__group--md row {{ $errors->has('address') ? 'has-danger' : '' }}">
                                        <label class="col-lg-3 col-form-label required">{{ __('Address') }}:</label>
                                        <div class="col-lg-9">
                                            <input type="text" name="address" class="form-control m-input"
                                                   value="{{ old('address', $atm->address ?? '') }}" placeholder="{{ __('Address') }}">
                                            @if ($errors->has('address'))
                                                <div class="form-control-feedback">{{ $errors->first('address') }}</div>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group m-form__group--md row {{ $errors->has('zipcode', 'city') ? 'has-danger' : '' }}">
                                        <div class="offset-lg-3 col-lg-3">
                                            <input type="text" name="zipcode" class="form-control m-input"
                                                   value="{{ old('zipcode', $atm->zipcode ?? '') }}" placeholder="{{ __('Zipcode') }}">
                                            @if ($errors->has('zipcode'))
                                                <div class="form-control-feedback">{{ $errors->first('zipcode') }}</div>
                                            @endif
                                        </div>
                                        <div class="col-lg-6">
                                            <input type="text" name="city" class="form-control m-input"
                                                   value="{{ old('city', $atm->city ?? '') }}" placeholder="{{ __('City') }}">
                                            @if ($errors->has('city'))
                                                <div class="form-control-feedback">{{ $errors->first('city') }}</div>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group m-form__group--md row {{ $errors->has('country') ? 'has-danger' : '' }}">
                                        <div class="offset-lg-3 col-lg-6">
                                            <select name="country" class="form-control m-input">
                                                <option value="" disabled selected>{{ __('Select a country') }}</option>
                                                <option value="NL" {{ old('country', $atm->country ?? '') == 'NL' ? 'selected' : '' }}>Netherlands</option>
                                                <option value="BE" {{ old('country', $atm->country ?? '') == 'BE' ? 'selected' : '' }}>Belgium</option>
                                            </select>
                                            @if ($errors->has('country'))
                                                <div class="form-control-feedback">{{ $errors->first('country') }}</div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="m-separator m-separator--dashed m-separator--lg"></div>
                                <div class="m-form__section">
                                    <div class="m-form__heading">
                                        <h3 class="m-form__heading-title">{{ __('ATM') }} {{ __('information') }}</h3>
                                    </div>
                                    <div class="form-group m-form__group m-form__group--md row {{ $errors->has('identifier') ? 'has-danger' : '' }}">
                                        <label class="col-lg-3 col-form-label required">{{ __('Identifier') }}:</label>
                                        <div class="col-lg-3">
                                            <input type="text" name="identifier" class="form-control m-input"
                                                   value="{{ old('identifier', $atm->identifier ?? '') }}" placeholder="{{ __('Identifier') }}">
                                            @if ($errors->has('identifier'))
                                                <div class="form-control-feedback">{{ $errors->first('identifier') }}</div>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group m-form__group--md row {{ $errors->has('serial_number') ? 'has-danger' : '' }}">
                                        <label class="col-lg-3 col-form-label required">{{ __('Serial number') }}:</label>
                                        <div class="col-lg-6">
                                            <input type="text" name="serial_number" class="form-control m-input"
                                                   value="{{ old('serial_number', $atm->serial_number ?? '') }}" placeholder="{{ __('Serial number') }}">
                                            @if ($errors->has('serial_number'))
                                                <div class="form-control-feedback">{{ $errors->first('serial_number') }}</div>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group m-form__group--md row {{ $errors->has('manufacturer') ? 'has-danger' : '' }}">
                                        <label class="col-lg-3 col-form-label required">{{ __('Manufacturer') }}:</label>
                                        <div class="col-lg-6">
                                            <input type="text" name="manufacturer" class="form-control m-input"
                                                   value="{{ old('manufacturer', $atm->manufacturer ?? '') }}" placeholder="{{ __('Manufacturer') }}">
                                            @if ($errors->has('manufacturer'))
                                                <div class="form-control-feedback">{{ $errors->first('manufacturer') }}</div>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group m-form__group--md row {{ $errors->has('year') ? 'has-danger' : '' }}">
                                        <label class="col-lg-3 col-form-label required">{{ __('Construction year') }}:</label>
                                        <div class="col-lg-3">
                                            <input type="number" name="year" class="form-control m-input"
                                                   value="{{ old('year', $atm->year ?? '') }}" placeholder="{{ __('Construction year') }}">
                                            @if ($errors->has('year'))
                                                <div class="form-control-feedback">{{ $errors->first('year') }}</div>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group m-form__group--md row {{ $errors->has('owner') ? 'has-danger' : '' }}">
                                        <label class="col-lg-3 col-form-label required">{{ __('Owner') }}:</label>
                                        <div class="col-lg-6">
                                            <select name="owner" class="form-control m-input">
                                                <option value="" disabled selected>{{ __('Select a bank') }}</option>
                                                <option {{ old('owner', $atm->owner ?? '') == 'ING' ? 'selected' : '' }}>ING</option>
                                                <option {{ old('owner', $atm->owner ?? '') == 'Rabobank' ? 'selected' : '' }}>Rabobank</option>
                                                <option {{ old('owner', $atm->owner ?? '') == 'ABN AMRO' ? 'selected' : '' }}>ABN AMRO</option>
                                            </select>
                                            @if ($errors->has('bank'))
                                                <div class="form-control-feedback">{{ $errors->first('owner') }}</div>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group m-form__group--md row {{ $errors->has('location') ? 'has-danger' : '' }}">
                                        <label class="col-lg-3 col-form-label required">{{ __('Location') }}:</label>
                                        <div class="col-lg-9">
                                            <div class="m-radio-inline">
                                                <label class="m-radio">
                                                    <input type="radio" name="location" value="indoor" {{ old('location', $atm->location ?? '') == 'indoor' ? 'checked' : '' }}> {{ __('Indoor') }}
                                                    <span></span>
                                                </label>
                                                <label class="m-radio">
                                                    <input type="radio" name="location" value="outdoor" {{ old('location', $atm->location ?? '') == 'outdoor' ? 'checked' : '' }}> {{ __('Outdoor') }}
                                                    <span></span>
                                                </label>
                                            </div>
                                            @if ($errors->has('state'))
                                                <div class="form-control-feedback">{{ $errors->first('state') }}</div>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group m-form__group--md row {{ $errors->has('capacity') ? 'has-danger' : '' }}">
                                        <label class="col-lg-3 col-form-label required">{{ __('Capacity') }}:</label>
                                        <div class="col-lg-4">
                                            <div class="input-group m-input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" id="basic-addon1">&euro;</span>
                                                </div>
                                                <input type="text" class="form-control m-input" name="capacity"
                                                       value="{{ old('capacity', $atm->capacity ?? '') }}" aria-describedby="basic-addon1"
                                                       data-mask="#.##0" data-mask-reverse="true">
                                                <div class="input-group-append">
                                                    <span class="input-group-text" id="basic-addon1">,00</span>
                                                </div>
                                            </div>
                                            @if ($errors->has('capacity'))
                                                <div class="form-control-feedback">{{ $errors->first('capacity') }}</div>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="m-separator m-separator--dashed m-separator--lg"></div>

                                <div class="m-form__section">
                                    <div class="m-form__heading">
                                        <h3 class="m-form__heading-title">{{ __('Functionalities') }}</h3>
                                    </div>
                                    <div class="form-group m-form__group m-form__group--md row">
                                        <label class="col-lg-3 col-form-label required">{{ __('Select applicable functionalities') }}:</label>
                                        <div class="col-lg-9">
                                            <div class="m-checkbox-list">
                                                @foreach (\App\Models\Functionality::orderBy('description')->get() as $functionality)
                                                    <label class="m-checkbox">
                                                        <input type="checkbox" name="functionalities[]" value="{{ $functionality->id }}"
                                                                @isset($atm)
                                                                    {{ $atm->functionalities->pluck('id')->search($functionality->id) !== false ? 'checked' : '' }}
                                                                @endisset
                                                                > {{ $functionality->description }}
                                                        <span></span>
                                                    </label>
                                                @endforeach
                                            </div>
                                            @if ($errors->has('functionalities.*'))
                                                <div class="form-control-feedback">{{ $errors->first('functionalities') }}</div>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="m-separator m-separator--dashed m-separator--lg"></div>

                                <div class="m-form__section">
                                    <div class="m-form__heading">
                                        <h3 class="m-form__heading-title">{{ __('Usage') }}</h3>
                                    </div>
                                    <div class="form-group m-form__group m-form__group--md row">
                                        <div class="col-lg-6 m-form__group-sub {{ $errors->has('commissioned') ? 'has-danger' : '' }}">
                                            <label class="form-control-label">{{ __('Commissioned per date (if applicable)') }}:</label>
                                            <input class="form-control m-input" name="commissioned" type="date" value="{{ old('commissioned', optional($atm->commissioned ?? null)->format('Y-m-d') ?? '') }}" id="commissioned-input">
                                            @if ($errors->has('commissioned'))
                                                <div class="form-control-feedback">{{ $errors->first('commissioned') }}</div>
                                            @endif
                                        </div>
                                        <div class="col-lg-6 m-form__group-sub {{ $errors->has('decommissioned') ? 'has-danger' : '' }}">
                                            <label class="form-control-label">{{ __('Decommissioned per date (if applicable)') }}:</label>
                                            <input class="form-control m-input" name="decommissioned" type="date" value="{{ old('decommissioned', optional($atm->decommissioned ?? null)->format('Y-m-d') ?? '') }}" id="decommissioned-input">
                                            @if ($errors->has('decommissioned'))
                                                <div class="form-control-feedback">{{ $errors->first('decommissioned') }}</div>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                @isset ($atm)
                                    <div class="m-separator m-separator--dashed m-separator--lg"></div>

                                    <div class="m-alert m-alert--icon m-alert--icon-solid m-alert--outline alert alert-{{ $errors->has('status_id') ? 'danger' : 'info' }} fade show" role="alert">
                                        <div class="m-alert__icon">
                                            <i class="flaticon-exclamation-1"></i>
                                            <span></span>
                                        </div>
                                        <div class="m-alert__text">
                                            {{ __('While editing an existing ATM, please select the proper reason for editing this entity:') }}<br>
                                            <div class="row mt-3">
                                                <div class="col-lg-6">
                                                    <input type="hidden" name="action" value="update">
                                                    <select name="status_id" class="form-control m-input">
                                                        <option value="" disabled selected>{{ __('Select a status') }}</option>
                                                        @foreach (\App\Status::atm()->get() as $status)
                                                            <option {{ old('status_id') == $status->id ? 'selected' : '' }} value="{{ $status->id }}">{{ __($status->description) }}</option>
                                                        @endforeach
                                                    </select>
                                                    @if ($errors->has('status_id'))
                                                        <div class="form-control-feedback mt-2">{{ $errors->first('status_id') }}</div>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endisset
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__foot m--align-right">
                        <button type="submit" class="btn btn-focus m-btn m-btn--icon m-btn--wide">
                            <i class="la la-check"></i>
                            {{ __('Save') }}
                        </button>
                        <span class="m--margin-left-10">{{ __('or') }} <a href="{{ url()->previous() }}" class="m-link m--font-bold">{{ __('cancel') }}</a></span>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection