<div class="m-wizard__form-step" id="m_wizard_form_step_5">
    <div class="m-form__section m-form__section--first">
        <div id="witness-repeater">
            <div class="m-form__heading">
                <div class="pull-right">
                    <div data-repeater-create="" class="btn btn btn-sm btn-brand m-btn m-btn--icon m-btn--pill m-btn--wide">
                        <span>
                            <i class="la la-plus"></i>
                            <span>{{ __('Add') }}</span>
                        </span>
                    </div>
                </div>
                <h3 class="m-form__heading-title">
                    {{ __('Witnesses') }}
                    <small class="text-muted">{{ __('Please fill in all available information') }}</small>
                </h3>
            </div>

            <div data-repeater-list="witnesses" class="mt-5 d-block">
                <div data-repeater-item>
                    <div class="media border border-secondary p-4 mb-3">
                        <i class="la fa-3x la-user mr-3"></i>
                        <div class="media-body">
                            <div class="form-group m-form__group m-form__group--md row">
                                <div class="col-lg-6 order-lg-last text-right">
                                    <div data-repeater-delete="" class="btn-sm btn btn-danger m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill">
                                        <i class="la la-trash-o"></i>
                                    </div>
                                </div>
                                <label class="col-lg-3 col-form-label required">{{ __('Gender') }}</label>
                                <div class="col-lg-3">
                                    <div class="m-radio-inline">
                                        <label class="m-radio">
                                            <input type="radio" name="gender" value="M"> {{ __('Male') }}
                                            <span></span>
                                        </label>
                                        <label class="m-radio">
                                            <input type="radio" name="gender" value="F"> {{ __('Female') }}
                                            <span></span>
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group m-form__group m-form__group--md row">
                                <label class="col-lg-3 col-form-label required">{{ __('Name') }}</label>
                                <div class="col-lg-9">
                                    <input type="text" name="name" class="form-control m-input" placeholder="{{ __('Name') }}">
                                </div>
                            </div>

                            <div class="form-group m-form__group m-form__group--md row">
                                <label class="col-lg-3 col-form-label required">{{ __('Phone') }}</label>
                                <div class="col-lg-9">
                                    <div class="m-input-icon m-input-icon--left">
                                        <input type="text" class="form-control m-input" name="phone" value="">
                                        <span class="m-input-icon__icon m-input-icon__icon--left">
                                            <span><i class="la la-phone"></i></span>
                                        </span>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group m-form__group m-form__group--md row">
                                <label class="col-lg-3 col-form-label">{{ __('Address') }}</label>
                                <div class="col-lg-9">
                                    <input type="text" name="address" class="form-control m-input" placeholder="{{ __('Address') }}">
                                </div>
                            </div>
                            <div class="form-group m-form__group m-form__group--md row">
                                <div class="offset-lg-3 col-lg-3">
                                    <input type="text" name="zipcode" class="form-control m-input" placeholder="{{ __('Zipcode') }}">
                                </div>
                                <div class="col-lg-6">
                                    <input type="text" name="city" class="form-control m-input" placeholder="{{ __('City') }}">
                                </div>
                            </div>

                            <div class="form-group m-form__group m-form__group--md row">
                                <label class="col-lg-3 col-form-label">{{ __('Point of time') }}</label>
                                <div class="col-lg-3">
                                    <div class="m-input-icon m-input-icon--left">
                                        <input type="time" class="form-control m-input" name="time" value="">
                                        <span class="m-input-icon__icon m-input-icon__icon--left">
                                            <span><i class="la la-clock-o"></i></span>
                                        </span>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@push ('scripts')
    <script>
        $(function () {
            var witnessrepeater = $("#witness-repeater").repeater({
                initEmpty: false,
                defaultValues: {
                    company: '',
                    name: '',
                    phone: '',
                },
                show: function() {
                    $(this).slideDown()
                },
                hide: function(e) {
                    $(this).slideUp(e)
                }
            });

            @if ($claim->witnesses ?? false)
            witnessrepeater.setList([
                @foreach ($claim->witnesses as $witness)
                {
                    gender: '{{ $witness->gender ?? '' }}',
                    name: '{{ $witness->name ?? '' }}',
                    phone: '{{ $witness->phone ?? '' }}',
                    address: '{{ $witness->address ?? '' }}',
                    zipcode: '{{ $witness->zipcode ?? '' }}',
                    city: '{{ $witness->city ?? '' }}',
                    time: '{{ $witness->time ?? '' }}',
                },
                @endforeach
            ]);
            @endif
        });
    </script>
@endpush