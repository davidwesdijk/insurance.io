<div class="m-wizard__form-step m-wizard__form-step--current" id="m_wizard_form_step_1">

    <div class="m-form__section m-form__section--first">
        <div class="m-form__heading">
            <h3 class="m-form__heading-title">General incident information</h3>
        </div>

        <div class="form-group m-form__group m-form__group--md row">
            <label class="col-lg-3 col-form-label">Case number</label>
            <div class="col-lg-6">
                <input type="text" class="form-control m-input" name="identifier" value="{{ $claim->identifier ?? $identifier }}" readonly>
            </div>
        </div>

        <div class="form-group m-form__group m-form__group--md row">
            <label class="col-lg-3 col-form-label required">{{ __('Type of incident') }}</label>
            <div class="col-lg-6">
                <select name="incident_type" class="form-control m-input bootstrap-select" title="{{ __('Select one of the following') }}...">
                    @foreach (\App\Base::type('IncidentType') as $key => $value)
                        <option value="{{ $key }}" {{ old('incident_type', $claim->incident_type ?? null) == $key ? 'selected' : '' }}>{{ \Illuminate\Support\Str::title($key) }}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="form-group m-form__group m-form__group--md row">
            <label class="col-lg-3 col-form-label required">{{ __('Actual attempt') }}</label>
            <div class="col-lg-6">
                <input type="hidden" name="attempt" value="0">
                <span class="m-switch m-switch--icon">
                    <label>
                        <input type="checkbox" name="attempt" value="1"
                                {{ old('attempt', $claim->attempt ?? null) == true ? 'checked' : '' }}>
                        <span></span>
                    </label>
                </span>
                @if ($errors->has('attempt'))
                    <div class="form-control-feedback">{{ $errors->first('active') }}</div>
                @endif
            </div>
        </div>

        <div class="form-group m-form__group m-form__group--md row">
            <label class="col-lg-3 col-form-label required">{{ __('Incident date & time') }}</label>
            <div class="col-lg-6">
                <div class="input-group">
                    <div class="input-group-prepend"><span class="input-group-text"><i class="la la-clock-o"></i></span></div>
                    <input class="form-control m-input" type="datetime-local" name="incident_datetime" value="{{ optional($claim->incident_datetime ?? now())->format('Y-m-d\TH:i') }}" id="example-datetime-local-input">
                </div>
            </div>
        </div>
    </div>

    <div class="m-separator m-separator--dashed m-separator--md"></div>

    <div class="m-form__section">
        <div class="m-form__heading">
            <h3 class="m-form__heading-title">
                {{ __('Reporting times') }}
                <i data-toggle="m-tooltip" data-width="auto" class="m-form__heading-help-icon flaticon-info" title="Some help text goes here"></i>
            </h3>
        </div>

        <div class="form-group m-form__group m-form__group--md row">
            <label class="col-lg-3 col-form-label required">{{ __('Emergency service') }}</label>
            <div class="col-lg-6">
                <div class="input-group">
                    <div class="input-group-prepend"><span class="input-group-text"><i class="la la-clock-o"></i></span></div>
                    <input class="form-control m-input" type="datetime-local" name="reported_emergency_services" value="{{ isset($claim) ? $claim->reported_emergency_services->format('Y-m-d\TH:i') : '' }}">
                </div>
            </div>
        </div>

        <div class="form-group m-form__group m-form__group--md row">
            <label class="col-lg-3 col-form-label required">{{ __('SIOC') }}</label>
            <div class="col-lg-6">
                <div class="input-group">
                    <div class="input-group-prepend"><span class="input-group-text"><i class="la la-clock-o"></i></span></div>
                    <input class="form-control m-input" type="datetime-local" name="reported_sioc" value="{{ isset($claim) ? $claim->reported_sioc->format('Y-m-d\TH:i') : '' }}">
                </div>
            </div>
        </div>

        <div class="form-group m-form__group m-form__group--md row">
            <label class="col-lg-3 col-form-label required">{{ __('Police') }}</label>
            <div class="col-lg-6">
                <div class="input-group">
                    <div class="input-group-prepend"><span class="input-group-text"><i class="la la-clock-o"></i></span></div>
                    <input class="form-control m-input" type="datetime-local" name="reported_police" value="{{ isset($claim) ? $claim->reported_police->format('Y-m-d\TH:i') : '' }}">
                </div>
            </div>
        </div>

        <div class="form-group m-form__group m-form__group--md row">
            <label class="col-lg-3 col-form-label required">{{ __('Arrival picket') }}</label>
            <div class="col-lg-6">
                <div class="input-group">
                    <div class="input-group-prepend"><span class="input-group-text"><i class="la la-clock-o"></i></span></div>
                    <input class="form-control m-input" type="datetime-local" name="arrival_picket" value="{{ isset($claim) ? $claim->arrival_picket->format('Y-m-d\TH:i') : '' }}">
                </div>
            </div>
        </div>

        <div class="form-group m-form__group m-form__group--md row">
            <label class="col-lg-3 col-form-label required">{{ __('Departure picket') }}</label>
            <div class="col-lg-6">
                <div class="input-group">
                    <div class="input-group-prepend"><span class="input-group-text"><i class="la la-clock-o"></i></span></div>
                    <input class="form-control m-input" type="datetime-local" name="departure_picket" value="{{ isset($claim) ? $claim->departure_picket->format('Y-m-d\TH:i') : '' }}">
                </div>
            </div>
        </div>
    </div>

    <div class="m-separator m-separator--dashed m-separator--md"></div>

    <div class="m-form__heading">
        <h3 class="m-form__heading-title">{{ __('Additional location information') }}</h3>
    </div>

    <div class="form-group m-form__group m-form__group--md row">
        <label class="col-lg-3 col-form-label">{{ __('Other ATMs on location?') }}</label>
        <div class="col-md-2">
            <div class="m-checkbox-inline mt-2">
                <label class="m-checkbox">
                    <input type="hidden" name="other_atms_on_location" value="0">
                    <input type="checkbox" name="other_atms_on_location" value="1" {{ ($claim->other_atms_on_location ?? null) == true ? 'checked' : '' }} data-show-if-toggled="other_atms_on_location"> {{ __('Yes') }}
                    <span></span>
                </label>
            </div>
        </div>
        <div class="col-lg-7 col-md-10 other_atms_on_location" style="display: none;">
            <input type="text" class="form-control m-input" name="other_atms_on_location_data" value="{{ $claim->other_atms_on_location_data ?? '' }}" placeholder="{{ __('Description') }}">
        </div>
    </div>

    <div class="form-group m-form__group m-form__group--md row">
        <label class="col-lg-3 col-form-label">{{ __('ATM SecurityBox present?') }}</label>
        <div class="col-md-2">
            <div class="m-checkbox-inline mt-2">
                <label class="m-checkbox">
                    <input type="hidden" name="securitybox_present" value="0">
                    <input type="checkbox" name="securitybox_present" value="1" {{ ($claim->securitybox_present ?? null) == true ? 'checked' : '' }} data-show-if-toggled="securitybox_present"> {{ __('Yes') }}
                    <span></span>
                </label>
            </div>
        </div>
        <div class="col-lg-3 col-md-4 securitybox_present" style="display: none;">
            <input type="text" class="form-control m-input" name="securitybox_present_data" value="{{ $claim->other_atms_on_location_data ?? '' }}" placeholder="{{ __('Number') }}">
        </div>
    </div>

    <div class="form-group m-form__group m-form__group--md row">
        <label class="col-lg-3 col-form-label">{{ __('Surveillance service present?') }}</label>
        <div class="col-md-2">
            <div class="m-checkbox-inline mt-2">
                <label class="m-checkbox">
                    <input type="hidden" name="surveillance_present" value="0">
                    <input type="checkbox" name="surveillance_present" value="1" {{ ($claim->surveillance_present ?? null) == true ? 'checked' : '' }} data-show-if-toggled="surveillance_present"> {{ __('Yes') }}
                    <span></span>
                </label>
            </div>
        </div>
    </div>

    <div class="form-group m-form__group m-form__group--md row surveillance_present" style="display: none;">
        <label class="col-lg-3 col-form-label required">{{ __('Which surveillance service?') }}</label>
        <div class="col-lg-6">
            <input type="text" class="form-control m-input" name="surveillance_present_service" value="{{ $claim->surveillance_present_service ?? '' }}" placeholder="{{ __('Company name of surveillance service') }}">
        </div>
    </div>

    <div class="form-group m-form__group m-form__group--md row surveillance_present" style="display: none;">
        <label class="col-lg-3 col-form-label required">{{ __('Employee name') }}</label>
        <div class="col-lg-6">
            <input type="text" class="form-control m-input" name="surveillance_present_employee" value="{{ $claim->surveillance_present_employee ?? '' }}" placeholder="{{ __('Name employee of surveillance service') }}">
        </div>
    </div>

    <div class="form-group m-form__group m-form__group--md row surveillance_present" style="display: none;">
        <label class="col-lg-3 col-form-label required">{{ __('ID number employee') }}</label>
        <div class="col-lg-6">
            <input type="text" class="form-control m-input" name="surveillance_present_number" value="{{ $claim->surveillance_present_number ?? '' }}" placeholder="{{ __('ID number of employee surveillance service') }}">
        </div>
    </div>

    <div class="m-separator m-separator--dashed m-separator--sm"></div>

    <div class="form-group m-form__group m-form__group--md row">
        <label class="col-lg-3 col-form-label">{{ __('Access door forced?') }}</label>
        <div class="col-md-2">
            <div class="m-checkbox-inline mt-2">
                <label class="m-checkbox">
                    <input type="hidden" name="access_door_forced" value="0">
                    <input type="checkbox" name="access_door_forced" value="1" {{ ($claim->access_door_forced ?? null) == true ? 'checked' : '' }}> {{ __('Yes') }}
                    <span></span>
                </label>
            </div>
        </div>
    </div>

    <div class="form-group m-form__group m-form__group--md row">
        <label class="col-lg-3 col-form-label required">{{ __('Type access door') }}</label>
        <div class="col-lg-6">
            <input type="text" class="form-control m-input" name="access_door_type" value="{{ $claim->access_door_type ?? '' }}">
        </div>
    </div>

    <div class="form-group m-form__group m-form__group--md row">
        <label class="col-lg-3 col-form-label">{{ __('Additional information access door') }}</label>
        <div class="col-lg-6">
            <textarea name="access_door_data" rows="3" class="form-control m-input">{!! isset($claim) ? nl2br($claim->access_door_data) : '' !!}</textarea>
        </div>
    </div>

    <div class="form-group m-form__group m-form__group--md row">
        <label class="col-lg-3 col-form-label">{{ __('NexGen cilinders secured?') }}</label>
        <div class="col-md-2">
            <div class="m-checkbox-inline mt-2">
                <label class="m-checkbox">
                    <input type="hidden" name="cilinders_secured" value="0">
                    <input type="checkbox" name="cilinders_secured" value="1" {{ ($claim->cilinders_secured ?? null) == true ? 'checked' : '' }} data-show-if-toggled="cilinders_secured"> {{ __('Yes') }}
                    <span></span>
                </label>
            </div>
        </div>
        <div class="col-lg-3 col-md-4 cilinders_secured" style="display: none;">
            <input type="text" class="form-control m-input" name="cilinders_secured_amount" value="{{ $claim->cilinders_secured_amount ?? '' }}" placeholder="{{ __('How many cilinders?') }}">
        </div>
    </div>

    <div class="m-separator m-separator--dashed m-separator--sm"></div>

    <div class="form-group m-form__group m-form__group--md row">
        <label class="col-lg-3 col-form-label">{{ __('State of vault') }}</label>
        <div class="col-lg-6">
            <select name="vault_state" class="form-control m-input bootstrap-select" title="{{ __('Select one of the following') }}...">
                @foreach (\App\Base::type('VaultState') as $key => $value)
                    <option value="{{ $key }}" {{ ($claim->vault_state ?? null) == $key ? 'selected' : '' }}>{{ \Illuminate\Support\Str::title($key) }}</option>
                @endforeach
            </select>
        </div>
    </div>

    <div class="form-group m-form__group m-form__group--md row">
        <label class="col-lg-3 col-form-label">{{ __('Valuables present?') }}</label>
        <div class="col-md-2">
            <div class="m-checkbox-inline mt-2">
                <label class="m-checkbox">
                    <input type="hidden" name="valuables_present" value="0">
                    <input type="checkbox" name="valuables_present" value="1" {{ ($claim->valuables_present ?? null) == true ? 'checked' : '' }} data-show-if-toggled="valuables_present"> {{ __('Yes') }}
                    <span></span>
                </label>
            </div>
        </div>
        <div class="col-lg-7 col-md-10 valuables_present" style="display: none;">
            <textarea class="form-control m-input" name="valuables_present_amount" placeholder="{{ __('Description of the amount of valuables at the location') }}">{!! isset($claim) ? nl2br($claim->valuables_present_amount) : '' !!}</textarea>
        </div>
    </div>

    <div class="form-group m-form__group m-form__group--md row">
        <label class="col-lg-3 col-form-label">{{ __('Amount valuables missing') }}</label>
        <div class="col-lg-6">
            <input type="text" class="form-control m-input" name="amount_missing" value="{{ $claim->amount_missing ?? '' }}">
        </div>
    </div>

    <div class="m-separator m-separator--dashed m-separator--sm"></div>

    <div class="form-group m-form__group m-form__group--md row">
        <label class="col-lg-3 col-form-label">{{ __('Attributes confiscated by police') }}</label>
        <div class="col-lg-6">
            <input type="text" class="form-control m-input" name="confiscated_police" value="{{ $claim->confiscated_police ?? '' }}">
        </div>
    </div>

    <div class="form-group m-form__group m-form__group--md row">
        <label class="col-lg-3 col-form-label">{{ __('Weather during incident') }}</label>
        <div class="col-lg-6">
            <select name="weather_circumstances" class="form-control m-input bootstrap-select" title="{{ __('Select one of the following') }}...">
                @foreach (\App\Base::type('WeatherCircumstances') as $key => $value)
                    <option value="{{ $key }}" {{ ($claim->weather_circumstances ?? null) == $key ? 'selected' : '' }}>{{ \Illuminate\Support\Str::title($key) }}</option>
                @endforeach
            </select>
        </div>
    </div>

    <div class="form-group m-form__group m-form__group--md row">
        <label class="col-lg-3 col-form-label">{{ __('Recorder present?') }}</label>
        <div class="col-md-2">
            <div class="m-checkbox-inline mt-2">
                <label class="m-checkbox">
                    <input type="hidden" name="recorder_present" value="0">
                    <input type="checkbox" name="recorder_present" value="1" {{ ($claim->recorder_present ?? null) == true ? 'checked' : '' }}> {{ __('Yes') }}
                    <span></span>
                </label>
            </div>
        </div>
    </div>

    <div class="form-group m-form__group m-form__group--md row">
        <label class="col-lg-3 col-form-label">{{ __('Recordings present?') }}</label>
        <div class="col-md-2">
            <div class="m-checkbox-inline mt-2">
                <label class="m-checkbox">
                    <input type="hidden" name="recordings_present" value="0">
                    <input type="checkbox" name="recordings_present" value="1" {{ ($claim->recordings_present ?? null) == true ? 'checked' : '' }}> {{ __('Yes') }}
                    <span></span>
                </label>
            </div>
        </div>
    </div>

    <div class="form-group m-form__group m-form__group--md row">
        <label class="col-lg-3 col-form-label">{{ __('Are there recorders nearby?') }}</label>
        <div class="col-md-2">
            <div class="m-checkbox-inline mt-2">
                <label class="m-checkbox">
                    <input type="hidden" name="nearby_recorders" value="0">
                    <input type="checkbox" name="nearby_recorders" value="1" {{ ($claim->nearby_recorders ?? null) == true ? 'checked' : '' }} data-show-if-toggled="nearby_recorders"> {{ __('Yes') }}
                    <span></span>
                </label>
            </div>
        </div>
        <div class="offset-lg-3 col-lg-7 col-md-10 nearby_recorders" style="display: none;">
            <textarea class="form-control m-input" name="nearby_recorders_description" placeholder="{{ __('Are there recorders or camera\'s visible in the surroundings of the location?') }}">{!! isset($claim) ? nl2br($claim->nearby_recorders_description) : '' !!}</textarea>
        </div>
    </div>

    <div class="m-separator m-separator--dashed m-separator--sm"></div>

    <div class="form-group m-form__group m-form__group--md row">
        <label class="col-lg-3 col-form-label">{{ __('Escape vehicle information') }}</label>
        <div class="col-lg-6">
            <input type="text" class="form-control m-input" name="escape_vehicle" value="{{ $claim->escape_vehicle ?? '' }}">
        </div>
    </div>

    <div class="form-group m-form__group m-form__group--md row">
        <label class="col-lg-3 col-form-label">{{ __('Amount of suspects') }}</label>
        <div class="col-lg-6">
            <input type="text" class="form-control m-input" name="suspects_amount" value="{{ $claim->suspects_amount ?? '' }}">
        </div>
    </div>

    <div class="form-group m-form__group m-form__group--md row">
        <label class="col-lg-3 col-form-label">{{ __('Signs of the suspect(s)') }}</label>
        <div class="col-lg-6">
            <textarea name="suspect_signs" rows="3" class="form-control m-input">{!! isset($claim) ? nl2br($claim->suspect_signs) : '' !!}</textarea>
        </div>
    </div>

    <div class="form-group m-form__group m-form__group--md row">
        <label class="col-lg-3 col-form-label">{{ __('Abandoned objects') }}</label>
        <div class="col-lg-6">
            <textarea name="abandoned_objects" rows="3" class="form-control m-input" placeholder="{{ __('Describe any found abandoned objects, e.g. gas hose or pizzaslides that has\'t been noticed by the police.') }}">{!! isset($claim) ? nl2br($claim->abandoned_objects) : '' !!}</textarea>
        </div>
    </div>

    <div class="form-group m-form__group m-form__group--md row">
        <label class="col-lg-3 col-form-label">{{ __('Objects secured by picket?') }}</label>
        <div class="col-lg-6">
            <input type="text" class="form-control m-input" name="abandoned_objects_secured" value="{{ $claim->abandoned_objects_secured ?? '' }}">
        </div>
    </div>

    <div class="form-group m-form__group m-form__group--md row">
        <label class="col-lg-3 col-form-label">{{ __('Other notable information') }}</label>
        <div class="col-lg-6">
            <textarea name="additional_information" rows="3" class="form-control m-input">{!! isset($claim) ? nl2br($claim->additional_information) : '' !!}</textarea>
        </div>
    </div>


</div>

@push ('scripts')
    <script>
        $(function () {
            var general = $('#m_wizard_form_step_1');

            autosize(general.find('textarea'));

            general.on('change', '[name="incident_datetime"]', function() {
                var inputs = ['reported_emergency_services', 'reported_sioc', 'reported_police', 'arrival_picket', 'departure_picket'];

                for (var index = 0; index < inputs.length; index++) {
                    $('[name="' + inputs[index] + '"]').val($(this).val());
                }
            });
        });
    </script>
@endpush