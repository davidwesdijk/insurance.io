<div class="m-wizard__form-step" id="m_wizard_form_step_6">
    <div class="m-form__section m-form__section--first">
        <div class="m-form__heading">
            <h3 class="m-form__heading-title">{{ __('Handling of values') }} <small class="text-muted">{{ __('after incident') }}</small></h3>
        </div>

        <div class="form-group m-form__group m-form__group--md row">
            <label class="col-lg-3 col-form-label required">{{ __('Last servicing') }}</label>
            <div class="col-lg-3">
                <div class="m-input-icon m-input-icon--left">
                    <input class="form-control m-input" type="date" name="last_servicing" value="{{ isset($claim) ? $claim->last_servicing->format('Y-m-d') : '' }}">
                    <span class="m-input-icon__icon m-input-icon__icon--left">
                        <span><i class="la la-calendar"></i></span>
                    </span>
                </div>
            </div>
        </div>

        <div class="form-group m-form__group m-form__group--md row">
            <label class="col-lg-3 col-form-label required">{{ __('Last known balance') }}</label>
            <div class="col-lg-3">
                <div class="m-input-icon m-input-icon--left">
                    <input class="form-control m-input" type="number" name="last_balance" value="{{ $claim->last_balance ?? '' }}">
                    <span class="m-input-icon__icon m-input-icon__icon--left">
                        <span><i class="la la-euro"></i></span>
                    </span>
                </div>
            </div>
        </div>

        <div class="form-group m-form__group m-form__group--md row">
            <label class="col-lg-3 col-form-label">{{ __('Loot') }}</label>
            <div class="col-lg-3">
                <div class="m-input-icon m-input-icon--left">
                    <input class="form-control m-input" type="number" name="loot" value="{{ $claim->loot ?? '' }}">
                    <span class="m-input-icon__icon m-input-icon__icon--left">
                        <span><i class="la la-euro"></i></span>
                    </span>
                </div>
            </div>
        </div>

        <div class="form-group m-form__group m-form__group--md row">
            <label class="col-lg-3 col-form-label">{{ __('Amount of valuables found') }}</label>
            <div class="col-lg-3">
                <div class="m-input-icon m-input-icon--left">
                    <input class="form-control m-input" type="number" name="valuables_found" value="{{ $claim->valuables_found ?? '' }}">
                    <span class="m-input-icon__icon m-input-icon__icon--left">
                        <span><i class="la la-euro"></i></span>
                    </span>
                </div>
            </div>
        </div>

        <div class="form-group m-form__group m-form__group--md row">
            <label class="col-lg-3 col-form-label">{{ __('Values transported by') }}</label>
            <div class="col-lg-9">
                <div class="m-radio-inline">
                    <label class="m-radio">
                        <input type="radio" name="transporter" value="G4S" {{ ($claim->transporter ?? null) == 'G4S' ? 'checked' : '' }}> {{ __('G4S') }}
                        <span></span>
                    </label>
                    <label class="m-radio">
                        <input type="radio" name="transporter" value="SecurCash" {{ ($claim->transporter ?? null) == 'SecurCash' ? 'checked' : '' }}> {{ __('SecurCash') }}
                        <span></span>
                    </label>
                </div>
            </div>
        </div>

        <div class="form-group m-form__group m-form__group--md row">
            <label class="col-lg-3 col-form-label">{{ __('Arrival money transporter') }}</label>
            <div class="col-lg-3">
                <div class="m-input-icon m-input-icon--left">
                    <input type="time" class="form-control m-input" name="transporter_arrival" value="{{ $claim->transporter_arrival ?? '' }}">
                    <span class="m-input-icon__icon m-input-icon__icon--left">
                        <span><i class="la la-clock-o"></i></span>
                    </span>
                </div>
            </div>
        </div>
    </div>

    <div class="m-separator m-separator--dashed m-separator--sm"></div>

    <div class="m-form__heading">
        <h3 class="m-form__heading-title">{{ __('Codes') }}</h3>
    </div>

    <div class="form-group m-form__group m-form__group--md row">
        <div class="col-lg-6 m-form__group-sub">
            <label class="form-control-label required">{{ __('Transportcode') }} <small class="text-muted">{{ __('Money transporter') }}</small></label>
            <input type="text" name="transporter_code" class="form-control m-input" value="{{ $claim->transporter_code ?? '' }}">
        </div>
        <div class="col-lg-6 m-form__group-sub">
            <label class="form-control-label required">{{ __('Location code') }} <small class="text-muted">{{ __('Count center') }}</small></label>
            <div class="m-input-icon m-input-icon--left">
                <input type="text" name="countcenter_code" class="form-control m-input" value="{{ $claim->countcenter_code ?? '' }}">
            </div>
        </div>
    </div>

    <div class="m-separator m-separator--dashed m-separator--sm"></div>

    <div class="m-form__section">
        <div class="m-form__heading">
            <h3 class="m-form__heading-title">{{ __('Secured values') }}</h3>
        </div>

        <div class="form-group m-form__group m-form__group--md row" id="sealbags-repeater">
            <label class="col-lg-3 col-form-label required">
                {{ __('Sealbagnumbers') }}
                <div data-repeater-create="" class="btn btn btn-sm btn-brand m-btn--icon-only m-btn m-btn--icon m-btn--pill m-btn--wide">
                    <i class="la la-plus"></i>
                </div>
            </label>
            <div class="col-lg-9 mt-2" data-repeater-list="sealbags">
                <div data-repeater-item="" class="form-group m-form__group m-form__group--md row">
                    <div class="col-md-4 col-sm-8">
                        <input type="text" class="form-control m-input" name="sealbag">
                        <div class="d-md-none m--margin-bottom-10"></div>
                    </div>
                    <div class="col-md-6 col-sm-4">
                        <div data-repeater-delete="" class="btn-sm btn btn-danger m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill mt-2">
                            <i class="la la-trash-o"></i>
                        </div>
                        <div class="m--margin-bottom-10"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="m-separator m-separator--dashed m-separator--sm"></div>

    <div class="m-form__section">
        <div id="transporter_employees-repeater">
            <div class="m-form__heading">
                <div class="pull-right">
                    <div data-repeater-create="" class="btn btn btn-sm btn-brand m-btn m-btn--icon m-btn--pill m-btn--wide">
                        <span>
                            <i class="la la-plus"></i>
                            <span>{{ __('Add') }}</span>
                        </span>
                    </div>
                </div>
                <h3 class="m-form__heading-title">
                    {{ __('Employees money transporters') }}
                    <small class="text-muted">{{ __('Who secured cards') }}</small>
                </h3>
            </div>

            <div class="form-group m-form__group row m-form__group--sm">
                <div class="col-md-4">
                    <label class="form-control-label required">{{ __('Name') }}</label>
                </div>
                <div class="col-md-3">
                    <label class="form-control-label required">{{ __('Number') }}</label>
                </div>
            </div>

            <div data-repeater-list="transporter_employees">
                <div data-repeater-item>
                    <div class="form-group m-form__group row m-form__group--sm">
                        <div class="col-md-4">
                            <input type="text" name="name" class="form-control m-input" value="">
                            <div class="m--margin-bottom-20"></div>
                        </div>
                        <div class="col-md-3">
                            <input type="text" class="form-control m-input" name="number" value="">
                            <div class="m--margin-bottom-20"></div>
                        </div>
                        <div class="col-sm-1 right">
                            <div data-repeater-delete="" class="btn-sm btn btn-danger m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill mt-2">
                                <i class="la la-trash-o"></i>
                            </div>
                            <div class="m--margin-bottom-20"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@push ('scripts')
    <script>
        $(function () {
            var sealbagsrepeater = $("#sealbags-repeater").repeater({
                initEmpty: false,
                show: function() {
                    $(this).slideDown()
                },
                hide: function(e) {
                    $(this).slideUp(e)
                }
            });

            @if ($claim->sealbags ?? false)
            sealbagsrepeater.setList([
                @foreach ($claim->sealbags as $sealbag)
                {
                    sealbag: '{{ $sealbag->sealbag ?? '' }}',
                },
                @endforeach
            ]);
            @endif

            var transporter_employees_repeater = $("#transporter_employees-repeater").repeater({
                    initEmpty: false,
                    defaultValues: {
                        company: '',
                        name: '',
                        phone: '',
                    },
                    show: function() {
                        $(this).slideDown()
                    },
                    hide: function(e) {
                        $(this).slideUp(e)
                    }
                });

            @if ($claim->transporter_employees ?? false)
            transporter_employees_repeater.setList([
                @foreach ($claim->transporter_employees as $employee)
                {
                    name: '{{ $employee->name ?? '' }}',
                    number: '{{ $employee->number ?? '' }}',
                },
                @endforeach
            ]);
            @endif
        });
    </script>
@endpush