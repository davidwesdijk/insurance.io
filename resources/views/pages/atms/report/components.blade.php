<div class="m-wizard__form-step" id="m_wizard_form_step_3">
    <div class="m-form__section m-form__section--first">
        <div class="m-form__heading">
            <h3 class="m-form__heading-title">{{ __('Secured components') }}</h3>
        </div>

        <div class="form-group m-form__group m-form__group--md row">
            <label class="col-lg-3 col-form-label required">{{ __('SecurityBox number') }}</label>
            <div class="col-lg-4">
                <input type="text" class="form-control m-input" name="securitybox_number" value="{{ $claim->securitybox_number ?? '' }}">
            </div>
        </div>

        <div class="form-group m-form__group m-form__group--md row">
            <label class="col-lg-3 col-form-label required">{{ __('SecurityBox location') }}</label>
            <div class="col-lg-9">
                <input type="text" class="form-control m-input" name="securitybox_location" value="{{ $claim->securitybox_location ?? '' }}" placeholder="{{ __('E.g. Removed and delivered at...') }}">
            </div>
        </div>

        <div class="m-separator m-separator--dashed m-separator--sm"></div>

        <div class="form-group m-form__group m-form__group--md row">
            <label class="col-lg-3 col-form-label required">{{ __('SIM card number') }}</label>
            <div class="col-lg-4">
                <input type="text" class="form-control m-input" name="sim_number" value="{{ $claim->sim_number ?? '' }}">
            </div>
        </div>

        <div class="form-group m-form__group m-form__group--md row">
            <label class="col-lg-3 col-form-label required">{{ __('SIM card location') }}</label>
            <div class="col-lg-9">
                <input type="text" class="form-control m-input" name="sim_location" value="{{ $claim->sim_location ?? '' }}" placeholder="{{ __('E.g. Removed and delivered at...') }}">
            </div>
        </div>

        <div class="m-separator m-separator--dashed m-separator--sm"></div>

        <div class="form-group m-form__group m-form__group--md row">
            <label class="col-lg-3 col-form-label required">{{ __('Recorder location') }}</label>
            <div class="col-lg-9">
                <input type="text" class="form-control m-input" name="recorder_location" value="{{ $claim->recorder_location ?? '' }}" placeholder="{{ __('E.g. Removed and delivered at...') }}">
            </div>
        </div>

        <div class="m-separator m-separator--dashed m-separator--sm"></div>

        <div class="form-group m-form__group m-form__group--md row">
            <label class="col-lg-3 col-form-label required">{{ __('Cilinder location') }}</label>
            <div class="col-lg-9">
                <input type="text" class="form-control m-input" name="cilinder_location" value="{{ $claim->cilinder_location ?? '' }}" placeholder="{{ __('E.g. Removed and delivered at...') }}">
            </div>
        </div>
    </div>
</div>