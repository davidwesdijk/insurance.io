<div class="m-wizard__form-step" id="m_wizard_form_step_4">
    <div class="m-form__section m-form__section--first">
        <div class="m-form__heading">
            <h3 class="m-form__heading-title">{{ __('Police report') }} <small class="text-muted">{{ __('Who will report the incident?') }}</small></h3>
        </div>

        <div class="form-group m-form__group row">
            <div class="col-lg-6 m-form__group-sub">
                <label class="form-control-label required">{{ __('Name Risk & Compliance Officer (RCO)') }}</label>
                <input type="text" name="rco_name" class="form-control m-input" value="{{ $claim->rco_name ?? '' }}">
            </div>
            <div class="col-lg-6 m-form__group-sub">
                <label class="form-control-label required">{{ __('Phone number') }}</label>
                <div class="m-input-icon m-input-icon--left">
                    <input type="text" class="form-control m-input" name="rco_phone" value="{{ $claim->rco_phone ?? '' }}">
                    <span class="m-input-icon__icon m-input-icon__icon--left">
                        <span><i class="la la-phone"></i></span>
                    </span>
                </div>
            </div>
        </div>
    </div>

    <div class="m-separator m-separator--dashed m-separator--sm"></div>

    <div class="m-form__section">
        <div class="m-form__heading">
            <h3 class="m-form__heading-title">{{ __('Owner of the location') }}</h3>
        </div>

        <div class="form-group m-form__group m-form__group--md row">
            <label class="col-lg-3 col-form-label required">{{ __('Company name') }}</label>
            <div class="col-lg-9">
                <input type="text" class="form-control m-input" name="owner_company" value="{{ $claim->owner_company ?? '' }}">
            </div>
        </div>
    </div>

    <div class="m-separator m-separator--dashed m-separator--sm"></div>

    <div class="m-form__section">
        <div id="contacts-repeater">
            <div class="m-form__heading">
                <div class="pull-right">
                    <div data-repeater-create="" class="btn btn btn-sm btn-brand m-btn m-btn--icon m-btn--pill m-btn--wide">
                        <span>
                            <i class="la la-plus"></i>
                            <span>{{ __('Add') }}</span>
                        </span>
                    </div>
                </div>
                <h3 class="m-form__heading-title">
                    {{ __('Contacts') }}
                    <small class="text-muted">{{ __('Persons picket has had contact with') }}</small>
                </h3>
            </div>

            <div class="form-group m-form__group m-form__group--sm row">
                <div class="col-md-4">
                    <label class="form-control-label required">{{ __('Company name') }}</label>
                </div>
                <div class="col-md-4">
                    <label class="form-control-label required">{{ __('Name') }}</label>
                </div>
                <div class="col-md-3">
                    <label class="form-control-label required">{{ __('Phone number') }}</label>
                </div>
            </div>

            <div data-repeater-list="contacts">
                <div data-repeater-item>
                    <div class="form-group m-form__group m-form__group--sm row">
                        <div class="col-md-4">
                            <input type="text" name="company" class="form-control m-input" value="">
                            <div class="m--margin-bottom-20"></div>
                        </div>
                        <div class="col-md-4">
                            <input type="text" name="name" class="form-control m-input" value="">
                            <div class="m--margin-bottom-20"></div>
                        </div>
                        <div class="col-md-3">
                            <div class="m-input-icon m-input-icon--left">
                                <input type="text" class="form-control m-input" name="phone" value="">
                                <span class="m-input-icon__icon m-input-icon__icon--left">
                                    <span><i class="la la-phone"></i></span>
                                </span>
                            </div>
                            <div class="m--margin-bottom-20"></div>
                        </div>
                        <div class="col-sm-1 right">
                            <div data-repeater-delete="" class="btn-sm btn btn-danger m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill mt-2">
                                <i class="la la-trash-o"></i>
                            </div>
                            <div class="m--margin-bottom-20"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@push ('scripts')
    <script>
        $(function () {
            var repeater = $("#contacts-repeater").repeater({
                initEmpty: false,
                defaultValues: {
                    company: '',
                    name: '',
                    phone: '',
                },
                show: function() {
                    $(this).slideDown()
                },
                hide: function(e) {
                    $(this).slideUp(e)
                }
            });

            @if ($claim->contacts ?? false)
                repeater.setList([
                    @foreach ($claim->contacts as $contact)
                    {
                        company: '{!! $contact->company ?? '' !!}',
                        name: '{{ $contact->name ?? '' }}',
                        phone: '{{ $contact->phone ?? '' }}',
                    },
                    @endforeach
                ]);
            @endif
        });
    </script>
@endpush