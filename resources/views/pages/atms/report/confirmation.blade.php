<div class="m-wizard__form-step" id="m_wizard_form_step_7">
    @isset ($claim)
        <div class="m-form__section m-form__section--first">
            <div class="m-form__heading">
                <h3 class="m-form__heading-title">{{ __('Reason for editing') }} <small class="text-muted">{{ __('of report') }} {{ $claim->identifier }}</small></h3>
            </div>

            <div class="m-alert m-alert--icon m-alert--icon-solid m-alert--outline alert alert-{{ $errors->has('status_id') ? 'danger' : 'info' }} fade show" role="alert">
                <div class="m-alert__icon">
                    <i class="flaticon-exclamation-1"></i>
                    <span></span>
                </div>
                <div class="m-alert__text">
                    {{ __('While editing an existing claim, please select the proper reason for editing this report:') }}<br>
                    <div class="row mt-3">
                        <div class="col-lg-6">
                            <input type="hidden" name="action" value="update">
                            <select name="status_id" class="form-control m-input">
                                <option value="" disabled selected>{{ __('Select a status') }}</option>
                                @foreach (\App\Status::claim()->get() as $status)
                                    <option {{ old('status_id') == $status->id ? 'selected' : '' }} value="{{ $status->id }}">{{ __($status->description) }}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('status_id'))
                                <div class="form-control-feedback mt-2">{{ $errors->first('status_id') }}</div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endisset
</div>