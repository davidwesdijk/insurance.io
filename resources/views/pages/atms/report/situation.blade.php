<div class="m-wizard__form-step" id="m_wizard_form_step_2">
    <div class="m-form__section m-form__section--first">
        <div class="m-form__heading">
            <h3 class="m-form__heading-title">{{ __('Description of the situation') }}</h3>
        </div>
        <div class="form-group m-form__group m-form__group--md row">
            <div class="col-lg-12">
                <div class="m-alert m-alert--icon m-alert--outline alert m-alert--default" role="alert">
                    <div class="m-alert__icon">
                        <i class="la la-question-circle"></i>
                    </div>
                    <div class="m-alert__text">
                        {{ __('Please give a description of the situation as complete as possible. For guidance click the help button.') }}
                    </div>
                    <div class="m-alert__actions" style="width: 200px;" id="m_quick_sidebar_toggle">
                        <button type="button" class="btn btn-brand btn-sm m-btn m-btn--pill m-btn--wide btn-focus">
                            {{ __('Show') }}
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group m-form__group m-form__group--md row">
            <div class="col-lg-12">
                <textarea class="situation" name="situation">{!! isset($claim) ? nl2br($claim->situation) : '' !!}</textarea>
            </div>
        </div>
    </div>
</div>

@push ('scripts')
    <script>
        $(function () {
            var situation = $('#m_wizard_form_step_2');

            situation.find('.situation').summernote({
                minHeight: 250,
                toolbar: [
                    ['style', ['bold', 'italic', 'underline']],
                    ['para', ['ul', 'ol']],
                ],
                callbacks: {
                    onPaste: function (e) {
                        var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');
                        e.preventDefault();
                        document.execCommand('insertText', false, bufferText);
                    }
                }
            });
        });
    </script>
@endpush

@section ('sidebar_title', 'Information')
@section ('sidebar')
    <div class="row">
        <div class="col-12">
            <div class="m-alert m-alert--icon m-alert--outline alert m-alert--default" role="alert">
                <div class="m-alert__icon">
                    <i class="la la-question-circle"></i>
                </div>
                <div class="m-alert__text">
                    {{ __('Please following the subjects below to provide a complete view of the incident and the location.') }}
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <h6>{{ __('Reporting') }}</h6>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
        </div>
    </div>

    <div class="m-separator m-separator--dashed m-separator--sm"></div>

    <div class="row">
        <div class="col-12">
            <h6>{{ __('Recordings') }}</h6>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
        </div>
    </div>

    <div class="m-separator m-separator--dashed m-separator--sm"></div>

    <div class="row">
        <div class="col-12">
            <h6>{{ __('Barriers') }}</h6>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
        </div>
    </div>

    <div class="m-separator m-separator--dashed m-separator--sm"></div>

    <div class="row">
        <div class="col-12">
            <h6>{{ __('Incident') }}</h6>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
        </div>
    </div>

    <div class="m-separator m-separator--dashed m-separator--sm"></div>

    <div class="row">
        <div class="col-12">
            <h6>{{ __('Claim') }}</h6>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
        </div>
    </div>
@endsection

@push ('modals')
    <div class="modal fade" id="situationModal" tabindex="-1" role="dialog" aria-labelledby="situationModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="situationModalLabel">{{ __('Additional information about the situation') }}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group m-form__group m-form__group--md row">
                        <div class="col-lg-6">
                            <label for="password" class="col-form-label">{{ __('New password') }}:</label>
                            <input type="password" class="form-control" name="password">
                        </div>
                        <div class="col-lg-6">
                            <label for="password_confirmation" class="col-form-label">{{ __('Confirm password') }}:</label>
                            <input type="password" class="form-control" name="password_confirmation">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <a href="javascript:;" data-dismiss="modal" class="m-link m--font-bold">{{ __('Close') }}</a></span>
                </div>
            </div>
        </div>
    </div>
@endpush