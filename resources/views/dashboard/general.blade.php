@extends ('template.default-2019')

@section ('content')
    <div class="row">
        <div class="col-lg-4">
            <div class="m-portlet m-portlet--full-height m--align-center">
                <div class="m-portlet__body">
                    <div class="row">
                        <div class="col-12">
                            <p class="display-3 m--font-boldest m--font-focus mb-0 mt-3">
                                {{ \App\Models\ATM::count() }}
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <p class="small text-muted mb-0">
                                {{ __('Total registered ATM\'s') }}
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-4">
            <div class="m-portlet m-portlet--full-height m--align-center">
                <div class="m-portlet__body">
                    <div class="row">
                        <div class="col-12">
                            <p class="display-3 m--font-boldest m--font-warning mb-0 mt-3">
                                {{ \App\Models\Claim::count() }}
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <p class="small text-muted mb-0">
                                {{ __('Number of incidents') }}
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-4">
            <div class="m-portlet m-portlet--full-height m--align-center">
                <div class="m-portlet__body">
                    <div class="row">
                        <div class="col-12">
                            <p class="display-3 m--font-boldest m--font-danger mb-0 mt-3">
                                &euro; 0,00
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <p class="small text-muted mb-0">
                                {{ __('Total claims value') }}
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-8">
            <div class="m-portlet m-portlet--responsive-mobile m-portlet--full-height" id="location_portlet">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-wrapper">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon">
                                    <i class="la la-map-marker"></i>
                                </span>
                                <h3 class="m-portlet__head-text m--font-focus">{{ __('Map') }}</h3>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="m-portlet__body m-portlet__body--no-padding">
                    <div id="map-container" style="height: 100%"></div>
                </div>
            </div>
        </div>

        <div class="col-lg-4">
            <div class="m-portlet m-portlet--head-overlay m-portlet--full-height   m-portlet--rounded-force">
                <div class="m-portlet__head m-portlet__head--fit">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text m--font-light">
                                Latest reports
                            </h3>
                        </div>
                    </div>
                </div>
                <div class="m-portlet__body">
                    <div class="m-widget28">
                        <div class="m-widget28__pic m-portlet-fit--sides"></div>
                        <div class="m-widget28__container" >
                            <!-- begin::Nav pills -->
                            <ul class="m-widget28__nav-items nav nav-pills nav-fill" role="tablist">
                                <li class="m-widget28__nav-item nav-item">
                                    <a class="nav-link active" data-toggle="pill" href="#menu11">
                                        <span><i class="fa flaticon-pie-chart"></i></span><span>Adjuster reports</span>
                                    </a>
                                </li>
                                <li class="m-widget28__nav-item nav-item">
                                    <a class="nav-link" data-toggle="pill" href="#menu21">
                                        <span><i class="fa flaticon-file-1"></i></span><span>General statistics</span>
                                    </a>
                                </li>
                                <li class="m-widget28__nav-item nav-item">
                                    <a class="nav-link" data-toggle="pill" href="#menu31">
                                        <span><i class="fa flaticon-clipboard"></i></span><span>Audit report</span>
                                    </a>
                                </li>
                            </ul>
                            <!-- end::Nav pills -->

                            <!-- begin::Tab Content -->
                            <div class="m-widget28__tab tab-content">
                                <div id="menu11" class="m-widget28__tab-container tab-pane active">
                                    <div class="m-widget28__tab-items">
                                        <div class="m-widget28__tab-item">
                                            <span>Company Name</span>
                                            <span>SLT Back-end Solutions</span>
                                        </div>
                                        <div class="m-widget28__tab-item">
                                            <span>INE Number</span>
                                            <span>D330-1234562546</span>
                                        </div>
                                        <div class="m-widget28__tab-item">
                                            <span>Total Charges</span>
                                            <span>USD 1,250.000</span>
                                        </div>
                                        <div class="m-widget28__tab-item">
                                            <span>Project Description</span>
                                            <span>Creating Back-end Components</span>
                                        </div>
                                    </div>
                                </div>
                                <div id="menu21" class="m-widget28__tab-container tab-pane fade">
                                    <div class="m-widget28__tab-items">
                                        <div class="m-widget28__tab-item">
                                            <span>Project Description</span>
                                            <span>Back-End Web Architecture</span>
                                        </div>
                                        <div class="m-widget28__tab-item">
                                            <span>Total Charges</span>
                                            <span>USD 2,170.000</span>
                                        </div>
                                        <div class="m-widget28__tab-item">
                                            <span>INE Number</span>
                                            <span>D110-1234562546</span>
                                        </div>
                                        <div class="m-widget28__tab-item">
                                            <span>Company Name</span>
                                            <span>SLT Back-end Solutions</span>
                                        </div>
                                    </div>
                                </div>
                                <div id="menu31" class="m-widget28__tab-container tab-pane fade">
                                    <div class="m-widget28__tab-items">
                                        <div class="m-widget28__tab-item">
                                            <span>Total Charges</span>
                                            <span>USD 3,450.000</span>
                                        </div>
                                        <div class="m-widget28__tab-item">
                                            <span>Project Description</span>
                                            <span>Creating Back-end Components</span>
                                        </div>
                                        <div class="m-widget28__tab-item">
                                            <span>Company Name</span>
                                            <span>SLT Back-end Solutions</span>
                                        </div>
                                        <div class="m-widget28__tab-item">
                                            <span>INE Number</span>
                                            <span>D510-7431562548</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end::Tab Content -->
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection

@push ('scripts')
    {{--<script src="{{ asset('assets/snippets/pages/dashboard/dashboard_new.js') }}" type="text/javascript"></script>--}}
    <script async defer src="https://maps.googleapis.com/maps/api/js?key={{ config('geocoder.key') }}&callback=initMap"></script>
    <script>
        var map, origin, bounds, infowindow;
        var markers = [];

        function initMap() {
            origin = new google.maps.LatLng(51.9225, 4.47917);
            map = new google.maps.Map(document.getElementById('map-container'), {
                zoom: 4,
                center: origin
            });

            infowindow = new google.maps.InfoWindow();
            bounds = new google.maps.LatLngBounds();

            $.ajax({
                url: '{{ route('api.atms.locations') }}',
                method: 'POST',
                data: {},
                success: function (response) {
                    for (var i = 0; i < response.data.length; i++) {
                        var position = new google.maps.LatLng(parseFloat(response.data[i].lat), parseFloat(response.data[i].lng));
                        var marker = new google.maps.Marker({
                            position: position,
                            map: map,
                        });
                        google.maps.event.addListener(marker, 'click', (function (marker, i) {
                            return function () {
                                infowindow.setContent(response.data[i].popup);
                                infowindow.setOptions({maxWidth: 400});
                                infowindow.open(map, marker);
                            }
                        }) (marker, i));

                        markers.push(marker);
                        bounds.extend(marker.position);
                    }
                }
            }).done(function () {
                new MarkerClusterer(map, markers, {imagePath: '/assets/vendors/custom/markerclusterer/images/m'});
                setTimeout(
                    function() {
                        map.fitBounds(bounds);
                    },
                    500);

            });
        }

        function toggleMap() {
            setTimeout(
                function() {
                    map.fitBounds(bounds);
                },
                500);
        }
    </script>
@endpush