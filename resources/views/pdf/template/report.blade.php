<!DOCTYPE html>
<html lang="en">
<head>
    <!-- META SECTION -->
    <title>
        @yield('title')
    </title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <style>
        .page-break {
            page-break-after: always;
        }

        body {
            font-family: Helvetica;
            font-size: 14px;
            margin-top: 3cm;
            margin-left: 2cm;
            margin-right: 2cm;
            margin-bottom: 2cm;
            z-index: 1;
        }

        @page {
            margin: 0px 0px;
        }

        a {
            color: black;
            text-decoration: none;
        }

        p {
            font-family: Helvetica;
            font-size: 14px;
        }

        table {
            font-family: Helvetica;
            font-size: 14px;
            width: 100%;
        }

        ol {
            font-family: Helvetica;
            font-size: 14px;
        }

        h1 {
            font-family: Helvetica;
            font-size: 28px;
            width: 100%;
            vertical-align: middle;
        }

        h2 {
            font-family: Helvetica;
            font-size: 22px;
            width: 100%;
            vertical-align: middle;
        }

        h3 {
            font-family: Helvetica;
            font-size: 14px;
        }

        td {

        }

        th {
            font-weight: bold;
            text-align: left;
        }

        .small-blank-row {
            font-size: 2px;
        }

        .max-width {
            width: 100%;
        }

        .page-break-before {
            page-break-before: always;
        }

        .page-break-after {
            page-break-after: always;
        }

        .no-page-break {
            page-break-inside: avoid;
        }

        .border-top {
            border-top: 1px solid;
        }

        .text-center {
            text-align: center;
        }

        header {
            position: fixed;
            top: 1cm;
            left: 2cm;
            right: 2cm;
            height: 1cm;
        }

        footer.footer {
            position: fixed;
            bottom: 0cm;
            left: 2cm;
            right: 2cm;
            height: 1cm;
            text-align: right;
        }

        footer.watermark {
            position: fixed;
            top: 750px;
            right: 0px;
            bottom: 0px;
            left: 100px;
            text-align: center;
            vertical-align: middle;
            font-size: 120px;
            font-weight: bold;
            z-index: -1;
            color: #cfcfcf;
        }

        footer.watermark .content {
            transform: rotate(-45deg);
        }

        table.bordered {
            border-collapse: collapse;
        }

        table.bordered td {
            padding: 8px 10px;
            vertical-align: top;
            border-top: 1px solid #b3b3b3;
        }

        table.col-2 tr td {
            width: 50%;
        }

        table.col-3 tr td {
            width: 33.3%;
        }

        .cell-33 {
            width: 33.3%;
        }

        .cell-66 {
            width: 66.6%;
        }

        .bold {
            font-weight: bold;
        }
    </style>
    @stack ('styles')
</head>
<body>
    @yield('content')
</body>
</html>