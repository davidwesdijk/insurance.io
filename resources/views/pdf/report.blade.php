@extends ('pdf.template.report')

@section ('title', $filename)

@section ('content')
    <footer class="watermark"><span class="content">PREVIEW</span></footer>
    <div class="cover">
        <table style="text-align: center; width: 100%; margin-top: -1cm;">
            <tr>
                <td style="padding-bottom: 150px;">
                    <img src="{{ public_path('img/insurance.jpg') }}" style="width: 500px;">
                </td>
            </tr>
            <tr>
                <td>
                    <h1 class="title">{{ __('Report') }}</h1>
                    <h1 class="title">{{ __($claim->incident_type) }}</h1>
                </td>
            </tr>
            <tr>
                <td>
                    <table class="summary" style="width: 100%">
                        <tr>
                            <td style="width: 50%">{{ __('Identifier') }} {{ __('SIOC') }}</td>
                            <td style="width: 50%">{{ $claim->identifier }}</td>
                        </tr>
                        <tr>
                            <td>{{ __('Date incident') }}</td>
                            <td>{{ $claim->incident_datetime->toFormattedDateString() }}</td>
                        </tr>
                        <tr>
                            <td>{{ __('Time incident') }}</td>
                            <td>{{ $claim->incident_datetime->format('H:i') }}</td>
                        </tr>
                        <tr>
                            <td>{{ __('ATM identifier') }}</td>
                            <td>{{ $claim->atm->identifier }}</td>
                        </tr>
                        <tr>
                            <td valign="top">{{ __('Location') }}</td>
                            <td>
                                {{ $claim->atm->address }}<br>
                                {{ $claim->atm->zipcode }} {{ $claim->atm->city }}<br>
                                {{ $claim->atm->country }}
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>

    <div class="page-break"></div>

    <header>
        <img src="{{ public_path('img/insurance.jpg') }}" style="width: 200px;">
    </header>
    <footer class="footer">
        {{ __('PREVIEW') }} -
        {{ now()->toDateTimeString() }}
    </footer>


    <h1 style="text-transform: uppercase; text-align: center;">{{ __('Report') }} {{ __($claim->incident_type) }}</h1>

    <table class="col-2 bordered" style="width: 100%;">
        <tr>
            <td>{{ __('Identifier') }} {{ __('SIOC') }}</td>
            <td class="bold">{{ $claim->identifier }}</td>
        </tr>
        <tr>
            <td>{{ __('Incident occurence') }}</td>
            <td class="bold">{{ $claim->incident_datetime->toFormattedDateString() }} {{ $claim->incident_datetime->format('H:i') }}</td>
        </tr>
        <tr>
            <td>{{ __('Address') }}</td>
            <td class="bold">{{ $claim->atm->address }}</td>
        </tr>
        <tr>
            <td>{{ __('Zipcode') }}</td>
            <td class="bold">{{ $claim->atm->zipcode }}</td>
        </tr>
        <tr>
            <td>{{ __('City') }}</td>
            <td class="bold">{{ $claim->atm->city }}, {{ $claim->atm->country }}</td>
        </tr>
        <tr>
            <td>{{ __('Location') }}</td>
            <td class="bold">{{ __($claim->atm->location) }}</td>
        </tr>
        <tr>
            <td>{{ __('Manufacturer') }}</td>
            <td class="bold">{{ $claim->atm->manufacturer }}</td>
        </tr>
        <tr>
            <td>{{ __('ATM identifier') }}</td>
            <td class="bold">{{ $claim->atm->identifier }}</td>
        </tr>
        <tr>
            <td>{{ __('Serial number') }}</td>
            <td class="bold">{{ $claim->atm->serial_number }}</td>
        </tr>
        <tr>
            <td>{{ __('Incident type') }}</td>
            <td class="bold">{{ __($claim->incident_type) }}</td>
        </tr>
        <tr>
            <td>{{ __('Attempt (yes / no)') }}</td>
            <td class="bold">{{ __($claim->attempt ? 'Yes' : 'No') }}</td>
        </tr>
        <tr>
            <td>{{ __('Reported to emergency services') }}</td>
            <td class="bold">{{ optional($claim->reported_emergency_services)->format('H:i') }}</td>
        </tr>
        <tr>
            <td>{{ __('Reported to SIOC') }}</td>
            <td class="bold">{{ optional($claim->reported_sioc)->format('H:i') }}</td>
        </tr>
        <tr>
            <td>{{ __('Reported to police') }}</td>
            <td class="bold">{{ optional($claim->reported_police)->format('H:i') }}</td>
        </tr>
        <tr>
            <td>{{ __('Arrival picket') }}</td>
            <td class="bold">{{ optional($claim->arrival_picket)->format('H:i') }}</td>
        </tr>
        <tr>
            <td>{{ __('Departure picket') }}</td>
            <td class="bold">{{ optional($claim->departure_picket)->format('H:i') }}</td>
        </tr>
        <tr>
            <td>{{ __('Name picket') }}</td>
            <td class="bold">{{ auth()->user()->full_name }}</td>
        </tr>
        <tr>
            <td>{{ __('Phone picket') }}</td>
            <td class="bold">{{ auth()->user()->phone ?? __('Unknown') }}</td>
        </tr>
        <tr>
            <td>{{ __('Region picket') }}</td>
            <td class="bold">{{ auth()->user()->region ?? __('Unknown') }}</td>
        </tr>
        <tr>
            <td>{{ __('Name police officer') }}</td>
            <td class="bold">{{ __('Unknown') }}</td>
        </tr>
        <tr>
            <td>{{ __('Phone police officer') }}</td>
            <td class="bold">{{ __('Unknown') }}</td>
        </tr>
        <tr>
            <td>{{ __('Region police officer') }}</td>
            <td class="bold">{{ __('Unknown') }}</td>
        </tr>
        <tr>
            <td>{{ __('BVA number') }}</td>
            <td class="bold">{{ __('Unknown') }}</td>
        </tr>
    </table>

    <div class="page-break"></div>

    <table class="col-2 bordered" style="width: 100%;">
        <tr>
            <td>{{ __('Other ATMs on location') }}</td>
            <td class="bold">
                @if ($claim->other_atms_on_location)
                    {{ __('Yes') }}, {{ $claim->other_atms_on_location_data ?? __('unknown') }}
                @else
                    {{ __('No') }}
                @endif
            </td>
        </tr>
        <tr>
            <td>{{ __('SecurityBox present?') }}</td>
            <td class="bold">
                @if ($claim->securitybox_present)
                    {{ __('Yes') }}, {{ __('number') }}: {{ $claim->securitybox_present_data ?? __('unknown') }}
                @else
                    {{ __('No') }}
                @endif
            </td>
        </tr>
        <tr>
            <td>{{ __('Which surveillance service is present?') }}</td>
            <td class="bold">{{ $claim->surveillance_present_service }}</td>
        </tr>
        <tr>
            <td>{{ __('Name employee surveillance service') }}</td>
            <td class="bold">{{ $claim->surveillance_present_employee }}</td>
        </tr>
        <tr>
            <td>{{ __('ID number surveillance service') }}</td>
            <td class="bold">{{ $claim->surveillance_present_number }}</td>
        </tr>
        <tr>
            <td>{{ __('Access door has been forced?') }}</td>
            <td class="bold">{{ __($claim->access_door_forced ? 'Yes' : 'No') }}</td>
        </tr>
        <tr>
            <td>{{ __('Type access door') }}</td>
            <td class="bold">{{ $claim->access_door_type }}</td>
        </tr>
        <tr>
            <td>{{ __('Access door particularities') }}</td>
            <td class="bold">{{ $claim->access_door_data }}</td>
        </tr>
        <tr>
            <td>{{ __('NexGen cilinders secured + how many') }}</td>
            <td class="bold">
                @if ($claim->cilinders_secured)
                    {{ __('Yes') }}, {{ $claim->cilinders_secured_amount ?? __('unknown') }}
                @else
                    {{ __('No') }}
                @endif
            </td>
        </tr>
        <tr>
            <td>{{ __('State of vault') }}</td>
            <td class="bold">{{ ucfirst($claim->vault_state) }}</td>
        </tr>
        <tr>
            <td>{{ __('Valuables present on location?') }}</td>
            <td class="bold">
                @if ($claim->valuables_present)
                    {{ __('Yes') }}, {{ $claim->valuables_present_amount ?? __('unknown') }}
                @else
                    {{ __('No') }}
                @endif
            </td>
        </tr>
        <tr>
            <td>{{ __('Valueables looted') }}</td>
            <td class="bold">{{ ucfirst($claim->amount_missing) }}</td>
        </tr>
        <tr>
            <td>{{ __('Attributes confiscated by police') }}</td>
            <td class="bold">{{ ucfirst($claim->confiscated_police) }}</td>
        </tr>
        <tr>
            <td>{{ __('Weather during incident') }}</td>
            <td class="bold">{{ ucfirst($claim->weather_circumstances) }}</td>
        </tr>
        <tr>
            <td>{{ __('Recorder present?') }}</td>
            <td class="bold">{{ __($claim->recorder_present ? 'Yes' : 'No') }}</td>
        </tr>
        <tr>
            <td>{{ __('Recordings present?') }}</td>
            <td class="bold">{{ __($claim->recordings_present ? 'Yes' : 'No') }}</td>
        </tr>

        <tr>
            <td>{{ __('Are there recorders nearby?') }}</td>
            <td class="bold">
                @if ($claim->nearby_recorders)
                    {{ __('Yes') }}, {{ $claim->nearby_recorders_description ?? __('unknown') }}
                @else
                    {{ __('No') }}
                @endif
            </td>
        </tr>
        <tr>
            <td>{{ __('Escape vehicle information') }}</td>
            <td class="bold">{{ ucfirst($claim->escape_vehicle) }}</td>
        </tr>
        <tr>
            <td>{{ __('Amount of suspects') }}</td>
            <td class="bold">{{ ucfirst($claim->suspects_amount) }}</td>
        </tr>
        <tr>
            <td>{{ __('Signs of the suspect(s)') }}</td>
            <td class="bold">{!! ucfirst(nl2br($claim->suspects_signs)) !!}</td>
        </tr>
        <tr>
            <td>{{ __('Abandoned objects') }}</td>
            <td class="bold">{!! ucfirst(nl2br($claim->abandoned_objects)) !!}</td>
        </tr>
        <tr>
            <td>{{ __('Objects secured by picket?') }}</td>
            <td class="bold">{{ ucfirst($claim->abandoned_objects_secured) }}</td>
        </tr>
        <tr>
            <td>{{ __('Other notable information') }}</td>
            <td class="bold">{!! ucfirst(nl2br($claim->additional_information)) !!}</td>
        </tr>
    </table>

    <div class="page-break"></div>

    <h1 style="text-transform: uppercase; text-align: center;">{{ __('Situation description') }}</h1>

    @if ($claim->situation)
        {!! $claim->situation !!}
    @else
        {{ __('Unknown') }}
    @endif

    <div class="page-break"></div>

    <h2 style="text-transform: uppercase; text-align: center;">{{ __('Secured components') }}</h2>

    <table class="bordered" style="width: 100%;">
        <tr>
            <td class="cell-33">{{ __('SecurityBox') }}</td>
            <td class="cell-66 bold">{!! !$claim->securitybox_number ? '' : $claim->securitybox_number . ', ' !!}{{ $claim->securitybox_location }}</td>
        </tr>
        <tr>
            <td class="cell-33">{{ __('SIM card') }}</td>
            <td class="cell-66 bold">{!! !$claim->sim_number ? '' : $claim->sim_number . ', ' !!}{{ $claim->sim_location }}</td>
        </tr>
        <tr>
            <td class="cell-33">{{ __('Recorder') }}</td>
            <td class="cell-66 bold">{{ $claim->recorder_location }}</td>
        </tr>
        <tr>
            <td class="cell-33">{{ __('Cilinder') }}</td>
            <td class="cell-66 bold">{{ $claim->cilinder_location }}</td>
        </tr>
    </table>

    <h2 style="text-transform: uppercase; text-align: center; margin-top:25px;">{{ __('Risk & compliance officer') }}</h2>

    <table class="bordered" style="width: 100%;">
        <tr>
            <td class="cell-33">{{ __('Name') }}</td>
            <td class="cell-66 bold">{{ $claim->rco_name }}</td>
        </tr>
        <tr>
            <td class="cell-33">{{ __('Phone') }}</td>
            <td class="cell-66 bold">{{ $claim->rco_phone }}</td>
        </tr>
    </table>

    <h2 style="text-transform: uppercase; text-align: center; margin-top:25px;">{{ __('Owner of the location') }}</h2>

    <table class="bordered" style="width: 100%;">
        <tr>
            <td class="cell-33">{{ __('Company name') }}</td>
            <td class="cell-66 bold">{{ $claim->owner_company }}</td>
        </tr>
    </table>

    <h2 style="text-transform: uppercase; text-align: center; margin-top:25px;">{{ __('Contacts') }}</h2>

    <table class="col-3 bordered" style="width: 100%;">
        @forelse ($claim->contacts as $contact)
            <tr>
                <td>{!! $contact->company !!}</td>
                <td>{!! $contact->name !!}</td>
                <td>{!! $contact->phone !!}</td>
            </tr>
        @empty
            <tr>
                <td>&nbsp;</td>
                <td style="text-align: center;"><em>{{ __('Unknown') }}</em></td>
                <td>&nbsp;</td>
            </tr>
        @endforelse
    </table>

    @if ($claim->witnesses)
        <h2 style="text-transform: uppercase; text-align: center; margin-top:25px;">{{ __('Witnesses') }}</h2>

        <table class="col-5 bordered" style="width: 100%;">
            @forelse ($claim->witnesses as $witness)
                <tr>
                    <td>{!! $witness->gender !!}</td>
                    <td>{!! $witness->name !!}</td>
                    <td>
                        {!! $witness->adress !!}<br>
                        {!! $witness->zipcode !!} {!! $witness->city !!}
                    </td>
                    <td>{!! $witness->phone !!}</td>
                    <td>{!! $witness->time !!}</td>
                </tr>
            @empty
                <tr>
                    <td style="text-align: center;"><em>{{ __('Unknown') }}</em></td>
                </tr>
            @endforelse
        </table>
    @endif

    <div class="page-break"></div>

    <h1 style="text-transform: uppercase; text-align: center;">{{ __('Handling of values') }}</h1>

    <table class="col-2 bordered" style="width: 100%;">
        <tr>
            <td>{{ __('Identifier') }} {{ __('report') }}</td>
            <td class="bold">{{ $claim->identifier }}</td>
        </tr>
        <tr>
            <td>{{ __('Incident occurence') }}</td>
            <td class="bold">{{ $claim->incident_datetime->toFormattedDateString() }} {{ $claim->incident_datetime->format('H:i') }}</td>
        </tr>
        <tr>
            <td>{{ __('Address') }}</td>
            <td class="bold">{{ $claim->atm->address }}</td>
        </tr>
        <tr>
            <td>{{ __('Zipcode') }}</td>
            <td class="bold">{{ $claim->atm->zipcode }}</td>
        </tr>
        <tr>
            <td>{{ __('City') }}</td>
            <td class="bold">{{ $claim->atm->city }}, {{ $claim->atm->country }}</td>
        </tr>
        <tr>
            <td>{{ __('Location') }}</td>
            <td class="bold">{{ __($claim->atm->location) }}</td>
        </tr>
        <tr>
            <td>{{ __('Manufacturer') }}</td>
            <td class="bold">{{ $claim->atm->manufacturer }}</td>
        </tr>
        <tr>
            <td>{{ __('ATM identifier') }}</td>
            <td class="bold">{{ $claim->atm->identifier }}</td>
        </tr>
        <tr>
            <td>{{ __('Serial number') }}</td>
            <td class="bold">{{ $claim->atm->serial_number }}</td>
        </tr>
        <tr>
            <td>{{ __('Incident type') }}</td>
            <td class="bold">{{ __($claim->incident_type) }}</td>
        </tr>
        <tr>
            <td>{{ __('Last servicing') }}</td>
            <td class="bold">{{ $claim->last_servicing->toFormattedDateString() }}</td>
        </tr>
        <tr>
            <td>{{ __('Last known balance') }}</td>
            <td class="bold">
                @if ($claim->last_balance)
                    € {{ number_format($claim->last_balance, 0) }},-
                @else
                    {{ __('Unknown') }}
                @endif
            </td>
        </tr>
        <tr>
            <td>{{ __('Loot') }}</td>
            <td class="bold">
                @if ($claim->loot)
                    € {{ number_format($claim->loot, 0) }},-
                @else
                    {{ __('Unknown') }}
                @endif
            </td>
        </tr>
        <tr>
            <td>{{ __('Valuables found') }}</td>
            <td class="bold">
                @if ($claim->valuables_found)
                    € {{ number_format($claim->valuables_found, 0) }},-
                @else
                    {{ __('Unknown') }}
                @endif
            </td>
        </tr>
        <tr>
            <td>{{ __('Values transported by') }}</td>
            <td class="bold">{{ $claim->transporter }}</td>
        </tr>
        <tr>
            <td>{{ __('Arrival money transporter') }}</td>
            <td class="bold">{{ $claim->transporter_arrival }}</td>
        </tr>
        <tr>
            <td>{{ __('Transportcode') }} - {{ __('Money transporter') }}</td>
            <td class="bold">{{ $claim->transporter_code }}</td>
        </tr>
        <tr>
            <td>{{ __('Location code') }} - {{ __('Count center') }}</td>
            <td class="bold">{{ $claim->countcenter_code }}</td>
        </tr>
    </table>

    <div class="page-break"></div>

    <h1 style="text-transform: uppercase; text-align: center;">{{ __('Secured sealbags') }}</h1>

    <table class=" bordered" style="width: 100%;">
        @forelse ($claim->sealbags as $sealbag)
            <tr>
                <td class="cell-33">{{ __('Sealbagnumber') }}</td>
                <td class="cell-66 bold">{{ $sealbag->sealbag }}</td>
            </tr>
        @empty
            <tr>
                <td style="text-align: center;"><em>{{ __('Unknown') }}</em></td>
            </tr>
        @endforelse
    </table>

    <h1 style="text-transform: uppercase; text-align: center;">{{ __('Employees money transporters') }}</h1>

    <table class="col-2 bordered" style="width: 100%;">
        <tr>
            <td class="bold">{{ __('Name') }}</td>
            <td class="bold">{{ __('Number') }}</td>
        </tr>
        @forelse ($claim->transporter_employees as $employee)
            <tr>
                <td class="bold">{{ $employee->name }}</td>
                <td>{{ $employee->number }}</td>
            </tr>
        @empty
            <tr>
                <td style="text-align: center;"><em>{{ __('Unknown') }}</em></td>
            </tr>
        @endforelse
    </table>


    <div class="page-break"></div>

    <h1 style="text-transform: uppercase; text-align: center;">{{ __('Location') }}</h1>

    <div style="width: 100%; text-align: center; margin-bottom: 20px;">
        <img src="{!! \App\Helpers\ReportHelper::getImageForPanorama($claim->atm) !!}" style="width: 500px;">
    </div>

    <div style="width: 100%; text-align: center;">
        <img src="{!! \App\Helpers\ReportHelper::getImageForMap($claim->atm) !!}" style="width: 500px;">
    </div>
@endsection

@push ('styles')
    <style>
        .cover {
            border: 1px solid #b3b3b3;
            padding: 150px 75px 0px 75px;
            text-align: center;
            z-index: 3;
        }

        .cover h1 {
            font-size: 40px;
            font-weight: bold;
            text-transform: uppercase;
            line-height: 30px;
        }

        .cover .summary {
            margin-top: 30px;
            margin-bottom: 80px;
            font-size: 20px;
            font-weight: bold;
        }

        .cover .summary tr td {
            padding-bottom: 10px;
        }
    </style>
@endpush