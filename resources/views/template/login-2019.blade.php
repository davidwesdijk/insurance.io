<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8" />
    <title>
        {{ config('app.name') }}
        @hasSection('title')
            | @yield('title')
        @endif
    </title>

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <script src="{{ asset('assets/vendors/webfontloader/webfontloader.js') }}"></script>
    <script>
        WebFont.load({
            google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
            active: function() {
                sessionStorage.fonts = true;
            }
        });
    </script>

    <link href="{{ asset('assets/vendors/base/vendors.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/snippets/pages/user/style.bundle.css') }}" rel="stylesheet" type="text/css" />

    <link rel="shortcut icon" href="{{ asset('favicon.png') }}" />
</head>

<body  class="m--skin- m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default">

<div class="m-grid m-grid--hor m-grid--root m-page">
    <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-grid--tablet-and-mobile m-grid--hor-tablet-and-mobile m-login m-login--1 m-login--signin" id="m_login">
        <div class="m-grid__item m-grid__item--order-tablet-and-mobile-2 m-login__aside">
            <div class="m-stack m-stack--hor m-stack--desktop">
                <div class="m-stack__item m-stack__item--fluid">

                    @yield ('content')

                </div>
            </div>
        </div>
        <div class="m-grid__item m-grid__item--fluid m-grid m-grid--center m-grid--hor m-grid__item--order-tablet-and-mobile-1 m-login__content m-grid-item--center"
             style="background-image: url('{{ asset('assets/app/media/img/bg/bg-5.jpg') }}')">
            <div class="m-grid__item">
                <h3 class="m-login__welcome">{{ __('Welcome to INsurance') }}</h3>
                <p class="m-login__msg">
                    {{ __('The #1 insurance management platform with real time statistics') }}
                </p>
            </div>
        </div>
    </div>

</div>

<script src="{{ asset('assets/vendors/base/vendors.bundle.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/snippets/pages/user/scripts.bundle.js') }}" type="text/javascript"></script>

</body>
</html>