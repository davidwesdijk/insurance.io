<ul class="m-topbar__nav m-nav m-nav--inline">
    {{--<li class="m-nav__item m-topbar__notifications m-topbar__notifications--img m-dropdown m-dropdown--large m-dropdown--header-bg-fill m-dropdown--arrow m-dropdown--align-center 	m-dropdown--mobile-full-width" data-dropdown-toggle="click" data-dropdown-persistent="true">--}}
        {{--<a href="#" class="m-nav__link m-dropdown__toggle" id="m_topbar_notification_icon">--}}
            {{--<span class="m-nav__link-badge m-badge m-badge--dot m-badge--dot-small m-badge--danger"></span>--}}
            {{--<span class="m-nav__link-icon">--}}
                {{--<span class="m-nav__link-icon-wrapper">--}}
                    {{--<i class="flaticon-music-2"></i>--}}
                {{--</span>--}}
            {{--</span>--}}
        {{--</a>--}}
        {{--<div class="m-dropdown__wrapper">--}}
            {{--<span class="m-dropdown__arrow m-dropdown__arrow--center"></span>--}}
            {{--<div class="m-dropdown__inner">--}}
                {{--<div class="m-dropdown__header m--align-center" style="background: url(assets/app/media/img/misc/notification_bg.jpg); background-size: cover;">--}}
                    {{--<span class="m-dropdown__header-title">--}}
                        {{--9 New--}}
                    {{--</span>--}}
                    {{--<span class="m-dropdown__header-subtitle">--}}
                        {{--User Notifications--}}
                    {{--</span>--}}
                {{--</div>--}}
                {{--<div class="m-dropdown__body">--}}
                    {{--<div class="m-dropdown__content">--}}
                        {{--<ul class="nav nav-tabs m-tabs m-tabs-line m-tabs-line--brand" role="tablist">--}}
                            {{--<li class="nav-item m-tabs__item">--}}
                                {{--<a class="nav-link m-tabs__link active" data-toggle="tab" href="#topbar_notifications_notifications" role="tab">--}}
                                    {{--Alerts--}}
                                {{--</a>--}}
                            {{--</li>--}}
                            {{--<li class="nav-item m-tabs__item">--}}
                                {{--<a class="nav-link m-tabs__link" data-toggle="tab" href="#topbar_notifications_events" role="tab">--}}
                                    {{--Events--}}
                                {{--</a>--}}
                            {{--</li>--}}
                            {{--<li class="nav-item m-tabs__item">--}}
                                {{--<a class="nav-link m-tabs__link" data-toggle="tab" href="#topbar_notifications_logs" role="tab">--}}
                                    {{--Logs--}}
                                {{--</a>--}}
                            {{--</li>--}}
                        {{--</ul>--}}
                        {{--<div class="tab-content">--}}
                            {{--<div class="tab-pane active" id="topbar_notifications_notifications" role="tabpanel">--}}
                                {{--<div class="m-scrollable" data-scrollable="true" data-max-height="250" data-mobile-max-height="200">--}}
                                    {{--<div class="m-list-timeline m-list-timeline--skin-light">--}}
                                        {{--<div class="m-list-timeline__items">--}}
                                            {{--<div class="m-list-timeline__item">--}}
                                                {{--<span class="m-list-timeline__badge -m-list-timeline__badge--state-success"></span>--}}
                                                {{--<span class="m-list-timeline__text">--}}
                                                    {{--12 new users registered--}}
                                                {{--</span>--}}
                                                {{--<span class="m-list-timeline__time">--}}
                                                    {{--Just now--}}
                                                {{--</span>--}}
                                            {{--</div>--}}
                                            {{--<div class="m-list-timeline__item">--}}
                                                {{--<span class="m-list-timeline__badge"></span>--}}
                                                {{--<span class="m-list-timeline__text">--}}
                                                    {{--System shutdown--}}
                                                    {{--<span class="m-badge m-badge--success m-badge--wide">--}}
                                                        {{--pending--}}
                                                    {{--</span>--}}
                                                {{--</span>--}}
                                                {{--<span class="m-list-timeline__time">--}}
                                                    {{--14 mins--}}
                                                {{--</span>--}}
                                            {{--</div>--}}
                                            {{--<div class="m-list-timeline__item">--}}
                                                {{--<span class="m-list-timeline__badge"></span>--}}
                                                {{--<span class="m-list-timeline__text">--}}
                                                    {{--New invoice received--}}
                                                {{--</span>--}}
                                                {{--<span class="m-list-timeline__time">--}}
                                                    {{--20 mins--}}
                                                {{--</span>--}}
                                            {{--</div>--}}
                                            {{--<div class="m-list-timeline__item">--}}
                                                {{--<span class="m-list-timeline__badge"></span>--}}
                                                {{--<span class="m-list-timeline__text">--}}
                                                    {{--DB overloaded 80%--}}
                                                    {{--<span class="m-badge m-badge--info m-badge--wide">--}}
                                                        {{--settled--}}
                                                    {{--</span>--}}
                                                {{--</span>--}}
                                                {{--<span class="m-list-timeline__time">--}}
                                                    {{--1 hr--}}
                                                {{--</span>--}}
                                            {{--</div>--}}
                                            {{--<div class="m-list-timeline__item">--}}
                                                {{--<span class="m-list-timeline__badge"></span>--}}
                                                {{--<span class="m-list-timeline__text">--}}
                                                    {{--System error ---}}
                                                    {{--<a href="#" class="m-link">--}}
                                                        {{--Check--}}
                                                    {{--</a>--}}
                                                {{--</span>--}}
                                                {{--<span class="m-list-timeline__time">--}}
                                                    {{--2 hrs--}}
                                                {{--</span>--}}
                                            {{--</div>--}}
                                            {{--<div class="m-list-timeline__item m-list-timeline__item--read">--}}
                                                {{--<span class="m-list-timeline__badge"></span>--}}
                                                {{--<span href="" class="m-list-timeline__text">--}}
                                                    {{--New order received--}}
                                                    {{--<span class="m-badge m-badge--danger m-badge--wide">--}}
                                                        {{--urgent--}}
                                                    {{--</span>--}}
                                                {{--</span>--}}
                                                {{--<span class="m-list-timeline__time">--}}
                                                    {{--7 hrs--}}
                                                {{--</span>--}}
                                            {{--</div>--}}
                                            {{--<div class="m-list-timeline__item m-list-timeline__item--read">--}}
                                                {{--<span class="m-list-timeline__badge"></span>--}}
                                                {{--<span class="m-list-timeline__text">--}}
                                                    {{--Production server down--}}
                                                {{--</span>--}}
                                                {{--<span class="m-list-timeline__time">--}}
                                                    {{--3 hrs--}}
                                                {{--</span>--}}
                                            {{--</div>--}}
                                            {{--<div class="m-list-timeline__item">--}}
                                                {{--<span class="m-list-timeline__badge"></span>--}}
                                                {{--<span class="m-list-timeline__text">--}}
                                                    {{--Production server up--}}
                                                {{--</span>--}}
                                                {{--<span class="m-list-timeline__time">--}}
                                                    {{--5 hrs--}}
                                                {{--</span>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="tab-pane" id="topbar_notifications_events" role="tabpanel">--}}
                                {{--<div class="m-scrollable" m-scrollabledata-scrollable="true" data-max-height="250" data-mobile-max-height="200">--}}
                                    {{--<div class="m-list-timeline m-list-timeline--skin-light">--}}
                                        {{--<div class="m-list-timeline__items">--}}
                                            {{--<div class="m-list-timeline__item">--}}
                                                {{--<span class="m-list-timeline__badge m-list-timeline__badge--state1-success"></span>--}}
                                                {{--<a href="" class="m-list-timeline__text">--}}
                                                    {{--New order received--}}
                                                {{--</a>--}}
                                                {{--<span class="m-list-timeline__time">--}}
                                                    {{--Just now--}}
                                                {{--</span>--}}
                                            {{--</div>--}}
                                            {{--<div class="m-list-timeline__item">--}}
                                                {{--<span class="m-list-timeline__badge m-list-timeline__badge--state1-danger"></span>--}}
                                                {{--<a href="" class="m-list-timeline__text">--}}
                                                    {{--New invoice received--}}
                                                {{--</a>--}}
                                                {{--<span class="m-list-timeline__time">--}}
                                                    {{--20 mins--}}
                                                {{--</span>--}}
                                            {{--</div>--}}
                                            {{--<div class="m-list-timeline__item">--}}
                                                {{--<span class="m-list-timeline__badge m-list-timeline__badge--state1-success"></span>--}}
                                                {{--<a href="" class="m-list-timeline__text">--}}
                                                    {{--Production server up--}}
                                                {{--</a>--}}
                                                {{--<span class="m-list-timeline__time">--}}
                                                    {{--5 hrs--}}
                                                {{--</span>--}}
                                            {{--</div>--}}
                                            {{--<div class="m-list-timeline__item">--}}
                                                {{--<span class="m-list-timeline__badge m-list-timeline__badge--state1-info"></span>--}}
                                                {{--<a href="" class="m-list-timeline__text">--}}
                                                    {{--New order received--}}
                                                {{--</a>--}}
                                                {{--<span class="m-list-timeline__time">--}}
                                                    {{--7 hrs--}}
                                                {{--</span>--}}
                                            {{--</div>--}}
                                            {{--<div class="m-list-timeline__item">--}}
                                                {{--<span class="m-list-timeline__badge m-list-timeline__badge--state1-info"></span>--}}
                                                {{--<a href="" class="m-list-timeline__text">--}}
                                                    {{--System shutdown--}}
                                                {{--</a>--}}
                                                {{--<span class="m-list-timeline__time">--}}
                                                    {{--11 mins--}}
                                                {{--</span>--}}
                                            {{--</div>--}}
                                            {{--<div class="m-list-timeline__item">--}}
                                                {{--<span class="m-list-timeline__badge m-list-timeline__badge--state1-info"></span>--}}
                                                {{--<a href="" class="m-list-timeline__text">--}}
                                                    {{--Production server down--}}
                                                {{--</a>--}}
                                                {{--<span class="m-list-timeline__time">--}}
                                                    {{--3 hrs--}}
                                                {{--</span>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="tab-pane" id="topbar_notifications_logs" role="tabpanel">--}}
                                {{--<div class="m-stack m-stack--ver m-stack--general" style="min-height: 180px;">--}}
                                    {{--<div class="m-stack__item m-stack__item--center m-stack__item--middle">--}}
                                        {{--<span class="">--}}
                                            {{--All caught up!--}}
                                            {{--<br>--}}
                                            {{--No new logs.--}}
                                        {{--</span>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</li>--}}
    {{--<li class="m-nav__item m-topbar__quick-actions m-topbar__quick-actions--img m-dropdown m-dropdown--large m-dropdown--header-bg-fill m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push m-dropdown--mobile-full-width m-dropdown--skin-light"  data-dropdown-toggle="click">--}}
        {{--<a href="#" class="m-nav__link m-dropdown__toggle">--}}
            {{--<span class="m-nav__link-badge m-badge m-badge--dot m-badge--info m--hide"></span>--}}
            {{--<span class="m-nav__link-icon">--}}
                {{--<span class="m-nav__link-icon-wrapper">--}}
                    {{--<i class="flaticon-share"></i>--}}
                {{--</span>--}}
            {{--</span>--}}
        {{--</a>--}}
        {{--<div class="m-dropdown__wrapper">--}}
            {{--<span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>--}}
            {{--<div class="m-dropdown__inner">--}}
                {{--<div class="m-dropdown__header m--align-center" style="background: url(assets/app/media/img/misc/quick_actions_bg.jpg); background-size: cover;">--}}
                    {{--<span class="m-dropdown__header-title">--}}
                        {{--Quick Actions--}}
                    {{--</span>--}}
                    {{--<span class="m-dropdown__header-subtitle">--}}
                        {{--Shortcuts--}}
                    {{--</span>--}}
                {{--</div>--}}
                {{--<div class="m-dropdown__body m-dropdown__body--paddingless">--}}
                    {{--<div class="m-dropdown__content">--}}
                        {{--<div class="m-scrollable" data-scrollable="false" data-max-height="380" data-mobile-max-height="200">--}}
                            {{--<div class="m-nav-grid m-nav-grid--skin-light">--}}
                                {{--<div class="m-nav-grid__row">--}}
                                    {{--<a href="#" class="m-nav-grid__item">--}}
                                        {{--<i class="m-nav-grid__icon flaticon-file"></i>--}}
                                        {{--<span class="m-nav-grid__text">--}}
                                            {{--Generate Report--}}
                                        {{--</span>--}}
                                    {{--</a>--}}
                                    {{--<a href="#" class="m-nav-grid__item">--}}
                                        {{--<i class="m-nav-grid__icon flaticon-time"></i>--}}
                                        {{--<span class="m-nav-grid__text">--}}
                                            {{--Add New Event--}}
                                        {{--</span>--}}
                                    {{--</a>--}}
                                {{--</div>--}}
                                {{--<div class="m-nav-grid__row">--}}
                                    {{--<a href="#" class="m-nav-grid__item">--}}
                                        {{--<i class="m-nav-grid__icon flaticon-folder"></i>--}}
                                        {{--<span class="m-nav-grid__text">--}}
                                            {{--Create New Task--}}
                                        {{--</span>--}}
                                    {{--</a>--}}
                                    {{--<a href="#" class="m-nav-grid__item">--}}
                                        {{--<i class="m-nav-grid__icon flaticon-clipboard"></i>--}}
                                        {{--<span class="m-nav-grid__text">--}}
                                            {{--Completed Tasks--}}
                                        {{--</span>--}}
                                    {{--</a>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</li>--}}
    <li class="m-nav__item m-topbar__user-profile m-topbar__user-profile--img  m-dropdown m-dropdown--medium m-dropdown--arrow m-dropdown--header-bg-fill m-dropdown--align-right m-dropdown--mobile-full-width m-dropdown--skin-light" m-dropdown-toggle="click">
        <a href="#" class="m-nav__link m-dropdown__toggle">
            <span class="m-topbar__welcome">
                {{ __('Hello') }},&nbsp;
            </span>
            <span class="m-topbar__username">
                {{ auth()->user()->first_name }}
            </span>
            <span class="m-topbar__userpic">
                <img src="{{ asset('assets/app/media/img/users/user4.jpg') }}" class="m--img-rounded m--marginless m--img-centered" alt=""/>
            </span>
        </a>
        <div class="m-dropdown__wrapper">
            <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
            <div class="m-dropdown__inner">
                <div class="m-dropdown__header m--align-center" style="background: url('{{ asset('assets/app/media/img/misc/user_profile_bg.jpg') }}'); background-size: cover;">
                    <div class="m-card-user m-card-user--skin-dark">
                        <div class="m-card-user__pic">
                            <img src="{{ asset('assets/app/media/img/users/user4.jpg') }}" class="m--img-rounded m--marginless" alt=""/>
                        </div>
                        <div class="m-card-user__details">
                            <span class="m-card-user__name m--font-weight-500">
                                {{ auth()->user()->full_name }}
                            </span>
                            <a href="" class="m-card-user__email m--font-weight-300 m-link">
                                {{ auth()->user()->email }}
                            </a>
                        </div>
                    </div>
                </div>
                <div class="m-dropdown__body">
                    <div class="m-dropdown__content">
                        <ul class="m-nav m-nav--skin-light">
                            <li class="m-nav__item">
                                <a href="{{ route('dashboard') }}" class="m-nav__link">
                                    <i class="m-nav__link-icon flaticon-profile-1"></i>
                                    <span class="m-nav__link-text">
                                        My profile
                                    </span>
                                </a>
                            </li>
                            <li class="m-nav__separator m-nav__separator--fit"></li>
                            <li class="m-nav__item">
                                <a href="{{ route('logout') }}"
                                   onclick="event.preventDefault(); document.getElementById('logout-form').submit();"
                                   class="btn m-btn--pill btn-secondary m-btn m-btn--custom m-btn--label-brand m-btn--bolder">
                                    {{ __('Logout') }}
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </li>
    <li class="m-nav__item m-topbar__languages m-dropdown m-dropdown--small m-dropdown--header-bg-fill m-dropdown--arrow m-dropdown--align-right m-dropdown--mobile-full-width" m-dropdown-toggle="click">
        <a href="#" class="m-nav__link m-dropdown__toggle">
            <span class="m-nav__link-text">
                <img class="m-topbar__language-selected-img" src="{{ asset('img/locales/' . app()->getLocale() . '.svg') }}">
            </span>
        </a>
        <div class="m-dropdown__wrapper">
            <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
            <div class="m-dropdown__inner">
                <div class="m-dropdown__header m--align-center" style="background: url('{{ asset('assets/app/media/img/misc/quick_actions_bg.jpg') }}'); background-size: cover;">
                    <span class="m-dropdown__header-subtitle">{{ __('Select your language') }}</span>
                </div>
                <div class="m-dropdown__body">
                    <div class="m-dropdown__content">
                        <ul class="m-nav m-nav--skin-light">
                            @foreach (config('app.locales') as $locale => $description)
                                @if (app()->getLocale() === $locale)
                                    <li class="m-nav__item m-nav__item--active">
                                        <span class="m-nav__link m-nav__link--active">
                                            <span class="m-nav__link-icon"><img class="m-topbar__language-img" src="{{ asset('img/locales/' . $locale . '.svg') }}"></span>
                                            <span class="m-nav__link-title m-topbar__language-text m-nav__link-text">{{ __($description) }}</span>
                                        </span>
                                    </li>
                                @else
                                    <li class="m-nav__item">
                                        <a href="{{ route('language', $locale) }}" class="m-nav__link">
                                            <span class="m-nav__link-icon"><img class="m-topbar__language-img" src="{{ asset('img/locales/' . $locale . '.svg') }}"></span>
                                            <span class="m-nav__link-title m-topbar__language-text m-nav__link-text">{{ __($description) }}</span>
                                        </a>
                                    </li>
                                @endif
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </li>
</ul>