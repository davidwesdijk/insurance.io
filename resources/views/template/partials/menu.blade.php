@isset ($navigation)
    <ul class="m-menu__nav m-menu__nav--submenu-arrow">
        @foreach ($navigation as $menu)
            @continue (auth()->user()->cannot($menu->permission))

            @if ($menu->submenu->isEmpty())
                <li class="m-menu__item {{ (config('view.active') ?? Route::currentRouteName()) == $menu->route ? 'm-menu__item--active' : '' }}" aria-haspopup="true">
                    <a href="{{ $menu->route ? route($menu->route) : '#' }}" class="m-menu__link">
                        <span class="m-menu__item-here"></span>
                        <span class="m-menu__link-text">{{ __($menu->label) }}</span>
                    </a>
                </li>
            @else
                <li class="m-menu__item m-menu__item--submenu m-menu__item--rel {{ ((config('view.active') && config('view.active') === $menu->route) || $menu->hasActiveSubmenu()) ? 'm-menu__item--active' : '' }}" m-menu-submenu-toggle="click" aria-haspopup="true">
                    <a href="{{ $menu->route ? route($menu->route) : '#' }}" class="m-menu__link m-menu__toggle">
                        <span class="m-menu__item-here"></span>
                        <span class="m-menu__link-text">{{ __($menu->label) }}</span>
                        <i class="m-menu__hor-arrow la la-angle-down"></i>
                        <i class="m-menu__ver-arrow la la-angle-right"></i>
                    </a>
                    <div class="m-menu__submenu m-menu__submenu--classic m-menu__submenu--left">
                        <span class="m-menu__arrow m-menu__arrow--adjust"></span>
                        <ul class="m-menu__subnav">
                            @foreach ($menu->submenu as $submenu)
                                @continue (auth()->user()->cannot($submenu->permission))

                                <li class="m-menu__item" data-redirect="true" aria-haspopup="true">
                                    <a href="{{ $submenu->route ? route($submenu->route) : '#' }}" class="m-menu__link">
                                        @if ($submenu->icon)
                                            <i class="m-menu__link-icon {{ $submenu->icon }}"></i>
                                        @endif
                                        <span class="m-menu__link-text">{{ __($submenu->label) }}</span>
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </li>
            @endif
        @endforeach
    </ul>
@endisset