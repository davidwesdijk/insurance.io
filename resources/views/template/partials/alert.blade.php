@if (session()->has('message'))
    <script>
        $(function () {
            $.notify({
                message: '{{ session()->get('message.message') }}',
                title: '{{ session()->get('message.title') }}',
            }, {
                type: '{{ session()->get('message.type') }}'
            })
        });
    </script>
@endif