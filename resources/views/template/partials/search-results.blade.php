<div class="m-list-search__results">
    @if ($results->isEmpty())
        <span class="m-list-search__result-message">
            No record found
        </span>
    @else

        @can ('read atms')
            <span class="m-list-search__result-category m-list-search__result-category--first">
                {{ __('ATM\'s') }}
            </span>
            @forelse ($results->get('atms')->get('results') as $atm)
                <a href="{{ route('atms.show', $atm) }}" class="m-list-search__result-item">
                    <span class="m-list-search__result-item-icon"><i class="flaticon-interface-3 m--font-warning"></i></span>
                    <span class="m-list-search__result-item-text">
                        {{ $atm->address }}, {{ $atm->city }}
                    </span>
                </a>
            @empty
                <span class="m-list-search__result-message mt-1">
                    {{ __('No results') }}
                </span>
            @endforelse
        @endcan
    @endif
</div>