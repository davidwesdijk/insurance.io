<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8" />
    <title>
        {{ config('app.name') }}
        @hasSection('title')
            | @yield('title')
        @endif
    </title>

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <script src="{{ asset('assets/vendors/webfontloader/webfontloader.js') }}"></script>
    <script>
        WebFont.load({
            google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
            active: function() {
                sessionStorage.fonts = true;
            }
        });
    </script>

    <link href="{{ asset('assets/vendors/custom/fullcalendar/fullcalendar.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/vendors/base/vendors_new.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/app/style_new.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet" type="text/css" />

    @stack ('styles')

    <link rel="shortcut icon" href="{{ asset('favicon.png') }}" />
</head>

<body class="m-page--wide m-header--fixed m-header--fixed-mobile m-footer--push m-aside--offcanvas-default">
<div class="m-grid m-grid--hor m-grid--root m-page">
    <header id="m_header" class="m-grid__item m-header" m-minimize="minimize" m-minimize-offset="200" m-minimize-mobile-offset="200" >
        <div class="m-header__top">
            <div class="m-container m-container--responsive m-container--xxl m-container--full-height m-page__container">
                <div class="m-stack m-stack--ver m-stack--desktop">
                    <div class="m-stack__item m-brand">
                        <div class="m-stack m-stack--ver m-stack--general m-stack--inline">
                            <div class="m-stack__item m-stack__item--middle m-brand__logo">
                                <a href="{{ route('dashboard') }}" class="m-brand__logo-wrapper">
                                    <img alt="{{ config('app.name') }}" class="img-fluid" src="{{ asset('img/insurance.png') }}"/>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="m-stack__item m-stack__item--fluid m-header-head" id="m_header_nav">
                        <div id="m_header_topbar" class="m-topbar  m-stack m-stack--ver m-stack--general">
                            <div class="m-stack__item m-topbar__nav-wrapper">
                                @include ('template.partials.header-actions')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="m-header__bottom">
            <div class="m-container m-container--responsive m-container--xxl m-container--full-height m-page__container">
                <div class="m-stack m-stack--ver m-stack--desktop">
                    <div class="m-stack__item m-stack__item--middle m-stack__item--fluid">
                        <button class="m-aside-header-menu-mobile-close m-aside-header-menu-mobile-close--skin-light" id="m_aside_header_menu_mobile_close_btn">
                            <i class="la la-close"></i>
                        </button>
                        <div id="m_header_menu" class="m-header-menu m-aside-header-menu-mobile m-aside-header-menu-mobile--offcanvas m-header-menu--skin-dark m-header-menu--submenu-skin-light m-aside-header-menu-mobile--skin-light m-aside-header-menu-mobile--submenu-skin-light">
                            @include ('template.partials.menu')
                        </div>
                    </div>
                    @include ('template.partials.search')
                </div>
            </div>
        </div>
    </header>


    <div class="m-grid__item m-grid__item--fluid m-grid m-grid--hor-desktop m-grid--desktop m-body">
        <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver m-container m-container--responsive m-container--xxl m-page__container">
            <div class="m-grid__item m-grid__item--fluid m-wrapper">
                @hasSection ('header')
                    @include ('components.subheader')
                @endif
                <div class="m-content">
                    @yield ('content')
                </div>
            </div>
        </div>
    </div>
    @include ('template.partials.footer')
    @hasSection ('sidebar')
        @include ('components.sidebar')
    @endif
</div>

<div class="m-scroll-top m-scroll-top--skin-top" m-toggle="m-scroll-top" m-scroll-offset="500" m-scroll-speed="300">
    <i class="la la-arrow-up"></i>
</div>

<script src="{{ asset('assets/vendors/base/vendors_new.bundle.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/app/js/scripts_layout.bundle.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/vendors/custom/fullcalendar/fullcalendar.bundle.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/vendors/jquery-mask/jquery.mask.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/vendors/custom/markerclusterer/markerclusterer.js') }}" type="text/javascript"></script>

@include ('template.partials.alert')
@stack ('modals')
@stack ('scripts')

</body>
</html>
