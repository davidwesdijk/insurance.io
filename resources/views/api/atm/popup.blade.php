<div class="info_content">
    <h4>{{ $atm->address }}, {{ $atm->city }}</h4>
    <p>
        <strong>{{ __('Owner') }}</strong>: {{ $atm->owner }}<br>
        <strong>{{ __('Identifier') }}</strong>: {{ $atm->identifier }}<br>
    </p>
    <p>
        <a href="{{ route('atms.show', $atm) }}" class="btn btn-focus m-btn m-btn--icon m-btn--wide">
            <i class="la la-search"></i> {{ __('View details') }}
        </a>
    </p>
</div>