@extends('template.login-2019')

@section('title', 'Verify your account')

@section('content')
    <div class="m-login__wrapper">

        <div class="m-login__logo">
            <a href="/">
                <img src="{{ asset('img/insurance.png') }}" style="max-width: 100%;">
            </a>
        </div>

        <div class="m-login__signin">
            <div class="m-login__head">
                <h3 class="m-login__title">{{ __('Please verify') }}</h3>
                <p>{{ __('We\'ve send you a personal token. This code is only valid for 10 minutes.') }}</p>

            </div>

            <form class="m-login__form m-form" action="{{ route('verify-mfa') }}" method="post">
                {{ csrf_field() }}

                <div class="form-group m-form__group">
                    <input class="form-control m-input {{ $errors->has('token') ? 'is-invalid' : '' }}" type="text"
                           placeholder="{{ __('Enter your personal token...') }}" name="token" autocomplete="off"
                           required autofocus>
                    @if ($errors->has('token'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('token') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="row m-login__form-sub">
                    <div class="col m--align-left">
                        <label class="m-checkbox m-checkbox--focus">
                            <input type="checkbox" name="extend_verification"> {{ __('Trust this device') }}
                            <span></span>
                        </label>
                    </div>
                </div>
                <div class="m-login__form-action">
                    <button type="submit" id="m_login_signin_submit" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air">{{ __('Sign in') }}</button>
                </div>
            </form>
        </div>
    </div>
@endsection
