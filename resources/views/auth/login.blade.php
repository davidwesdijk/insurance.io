@extends('template.login-2019')

@section('title', 'Login')

@section('content')
    <div class="m-login__wrapper">

        <div class="m-login__logo">
            <a href="/">
                <img src="{{ asset('img/insurance.png') }}" style="max-width: 100%;">
            </a>
        </div>

        <div class="m-login__signin">
            <div class="m-login__head">
                <h3 class="m-login__title">{{ __('Sign in to INsurance') }}</h3>
            </div>

            <form class="m-login__form m-form" action="{{ route('login') }}" method="post">
                {{ csrf_field() }}

                <div class="form-group m-form__group">
                    <input id="email" type="email" class="form-control m-input {{ $errors->has('email') ? 'is-invalid' : '' }}"
                           name="email" value="{{ old('email') }}" placeholder="{{ __('Email') }}" required autofocus>

                    @if ($errors->has('email'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group m-form__group">
                    <input class="form-control m-input m-login__form-input--last {{ $errors->has('password') ? 'is-invalid' : '' }}"
                           type="password" placeholder="{{ __('Password') }}" name="password" required>

                    @if ($errors->has('password'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="row m-login__form-sub">
                    <div class="col m--align-left">
                        <label class="m-checkbox m-checkbox--focus">
                            <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> {{ __('Remember me') }}
                            <span></span>
                        </label>
                    </div>
                    <div class="col m--align-right">
                        <a href="javascript:;" id="m_login_forget_password" class="m-link">{{ __('Forgot password?') }}</a>
                    </div>
                </div>
                <div class="m-login__form-action">
                    <button type="submit" id="m_login_signin_submit" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air">{{ __('Sign in') }}</button>
                </div>

                <div class="row m-login__form-sub">
                    <div class="col m--align-center">
                        @foreach (config('app.locales') as $locale => $description)
                            @continue (app()->getLocale() === $locale)
                            <small><a href="{{ route('language', $locale) }}" id="m_login_forget_password" class="m-link text-muted">{{ __($description) }}</a></small>
                        @endforeach
                    </div>
                </div>
            </form>
        </div>

        <div class="m-login__forget-password">
            <div class="m-login__head">
                <h3 class="m-login__title">{{ __('Forgot password?') }}</h3>
                <div class="m-login__desc">{{ __('Enter your email to reset your password') }}:</div>
            </div>
            <form class="m-login__form m-form" action="">
                <div class="form-group m-form__group">
                    <input class="form-control m-input" type="text" placeholder="{{ __('Email') }}" name="email" id="m_email" autocomplete="off">
                </div>
                <div class="m-login__form-action">
                    <button id="m_login_forget_password_submit" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air">{{ __('Request') }}</button>
                    <button id="m_login_forget_password_cancel" class="btn btn-outline-focus m-btn m-btn--pill m-btn--custom">{{ __('Cancel') }}</button>
                </div>
            </form>
        </div>
    </div>
@endsection
